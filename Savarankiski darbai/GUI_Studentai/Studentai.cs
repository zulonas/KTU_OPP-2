﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Studentai
{
    class Studentai
    {
        const int Cn = 500;
        private Studentas[] Stud;
        public int Kiek { get; set; }

        /// <summary>
        /// Klasės konstruktorius: suteikia kintamiesiems reikšmes
        /// </summary>
        public Studentai()
        {
            Kiek = 0;
            Stud = new Studentas[Cn];
        }

        /// <summary>
        /// Grąžina nurodyto indekso studento objektą.
        /// </summary>
        /// <param name="i"> indeksas </param>
        /// <returns> grąžina studento objektą </returns>
        public Studentas ImtiStudenta(int i) { return Stud[i]; }

        /// <summary>
        /// Įrašo į studentų objektų masyvą naują studentą.
        /// </summary>
        /// <param name="stud"> studentas </param>
        public void DetiStudenta(Studentas stud) { Stud[Kiek++] = stud;}
    }
}
