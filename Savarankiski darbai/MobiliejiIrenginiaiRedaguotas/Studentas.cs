﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobiliejiIrenginiaiRedaguotas
{
    /// <summary>
    /// Mobiliųjų įrenginių vienkryptis sąrašas
    /// </summary>
    public sealed class Studentas
    {
        private Mazgas Pradzia;
        private Mazgas Darbinis;

        public string VardasPavarde { get; set; }

        /// <summary>
        /// Konstruktorius be parametrų
        /// </summary>
        public Studentas()
        {
            Pradzia = new Mazgas(new Mobilus(), null);
            Darbinis = null;
        }

        /// <summary>
        /// Sąsajos metodai Sąsajai priskiriama sąrašo pradžia
        /// </summary>
        public void Pradžia()
        {
            Darbinis = Pradzia.Kitas;
        }

        /// <summary>
        /// Sąsajai priskiriamas sąrašo sekantis elementas
        /// </summary>
        public void Kitas()
        {
            Darbinis = Darbinis.Kitas;
        }

        /// <summary>
        /// Grąžina true, jeigu sąsaja netuščia; false - priešingu atveju
        /// </summary>
        /// <returns></returns>
        public bool Yra()
        {
            return Darbinis != null;
        }

        /// <summary>
        /// Grąžina pagalbinės rodyklės rodomo elemento reikšmę
        /// </summary>
        /// <returns></returns>
        public Mobilus ImtiDuomenis() { return Darbinis.Duomenys; }

        /// <summary>
        /// Sukuriamas sąrašo elementas ir prijungiamas prie sąrašo PRADŽIOS
        /// </summary>
        /// <param name="inf"> naujo elemento reikšmė (duomenys)</param>
        public void DėtiDuomenis(Mobilus inf )
        {
            Darbinis = Pradzia;
            while (Darbinis != null)
            {
                if (Darbinis.Kitas == null)
                {
                    Darbinis.Kitas = new Mazgas(inf, null);
                    return;
                }
                Darbinis = Darbinis.Kitas;
            }
        }

        /// <summary>
        /// Sunaikinamas sąrašas
        /// </summary>
        public void Naikinti()
        {
            //eina per elementus ir juos visus naikina 
            while (Pradzia != null)
            {
                Darbinis = Pradzia;
                Pradzia = Pradzia.Kitas;
                Darbinis.Duomenys = null;
                Darbinis.Kitas = null;
            }
            Pradzia = Darbinis = null;
        }

        /// <summary>
        /// Rikiavimas burbuliuku
        /// </summary>
        public void Rikiuoti()
        {
            bool keista = true;
            while (keista)
            {
                keista = false;
                Mazgas laik = Pradzia.Kitas;
                while(laik.Kitas != null)
                {
                    if(laik.Duomenys > laik.Kitas.Duomenys)
                    {
                        Mobilus keiciamas = laik.Kitas.Duomenys;
                        laik.Kitas.Duomenys = laik.Duomenys;
                        laik.Duomenys = keiciamas;
                        keista = true;
                    }
                    laik = laik.Kitas;
                }
            }
        }

        /// <summary>
        ///  Suranda ir grąžina ilgiausiai veikiančio įrenginio duomenis 
        /// </summary>
        /// <returns>Mobiliojo įrenginio objektas</returns>
        public Mobilus MaxTrukmė()
        {
            Mobilus max;
            max = Pradzia.Kitas.Duomenys;
            for (Mazgas d1 = Pradzia.Kitas; d1.Kitas != null; d1 = d1.Kitas)
                if (d1.Duomenys > max)
                    max = d1.Duomenys;
            return max;
        }

        /// <summary>
        /// Ieškoma naujo elemento įterpimo vieta. Vieta objektui duom 
        /// ieškoma, naudojant rikiavimui sukurtu operatoriumi
        /// </summary>
        /// <param name="duom">objektas</param>
        /// <returns></returns>
        private Mazgas Vieta(Mobilus duom)
        {
            Mazgas dd = Pradzia;
            if (dd == null) return null;
            while (dd.Kitas !=null && duom >= dd.Kitas.Duomenys)
                dd = dd.Kitas;
            return dd;
        }


        public void DetiDuomenisRikiuotame(Mobilus duom)
        {
            if(Vieta(duom) != null){
                Mazgas turimas = Vieta(duom);
                turimas.Kitas = new Mazgas(duom, turimas.Kitas);
            }
            else
            {
                DėtiDuomenis(duom);
            }
        }
    }
}
