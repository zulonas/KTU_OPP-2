﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _3_klausimas
{
    public class Knyga
    {
        public string Pavadinimas { get; set; }
        public int Metai { get; set; }
        public int PuslapiuSk { get; set; }
        public Knyga(string pavad = "", int metai = 0, int puslSk = 0)
        {
            this.Pavadinimas = pavad;
            this.Metai = metai;
            this.PuslapiuSk = puslSk;
        }
        public override string ToString()
        {
            string eilute;
            eilute = string.Format("{0, -20} {1, 4:d} {2, 4:d}",
            Pavadinimas, Metai, PuslapiuSk);
            return eilute;
        }
    }
    // Klasė sąrašo vienam elementui saugoti
    public sealed class Mazgas
    {
        public Knyga Duom { get; set; } // duomenys
        public Mazgas Kitas { get; set; } // nuoroda į kitą elementą
        public Mazgas(Knyga duom, Mazgas adresas)
        {
            this.Duom = duom;
            this.Kitas = adresas;
        }

    }
    // Klasė knygų duomenims saugoti
    public sealed class Sąrašas
    {
        private Mazgas pr; // sąrašo pradžia
        private Mazgas pb; // sąrašo pabaiga
        private Mazgas ss; // sąrašo sąsaja
                           // Pradinės sąrašo nuorodų reikšmės
        public Sąrašas()
        {
            this.pr = null;
            this.pb = null;
            this.ss = null;
        }
        // Grąžina sąrašo sąsajos elemento reikšmę (duomenis)
        public Knyga ImtiDuomenis() { return ss.Duom; }
        // Sukuriamas sąrašo elementas ir prijungiamas prie sąrašo pabaigos
        // nauja – naujo elemento reikšmė (duomenys)
        public void DėtiDuomenisT(Knyga nauja)
        {
            // ATLIKITE: padėkite naują elementą sąrašo pabaigoje
            if (pb == null)
            {
                pr = new Mazgas(nauja, null);
                pb = pr;
            }
            else
            {
                pb.Kitas = new Mazgas(nauja, null);
                pb = pb.Kitas;
            } 
        }
        // Sąsajai priskiriama sąrašo pradžia
        public void Pradžia() { ss = pr; }
        // Sąsajai priskiriamas sąrašo sekantis elementas
        public void Kitas() { ss = ss.Kitas; }
        // Grąžina true, jeigu sąsaja netuščia
        public bool Yra() { return ss != null; }
        // Šalina sąsajos rodomą elementą
        public void Salinti() 
        /* Šalinimo metodas kuris salina dabartini (ss) elementą 
         * iš susieto sąrašo. Deje bet yra problema, kad iš kart po to
         * negalima eiti per sąrašo elementus(naudoti loop'ą. Bet tai
         * beveik nereikšminga
         */
        {
            Mazgas laikinas;
            if(pr == ss) //jeigu pradžioje
            {
                laikinas = pr;
                pr = pr.Kitas;
                laikinas.Duom = null;
                laikinas = null;
                ss = new Mazgas(null, pr);
            }
            else //jeigu ne pradžioje
            {
                laikinas = pr;
                while(laikinas.Kitas != ss)
                {
                    if (laikinas.Kitas == null) return; //jeigu neranda
                    laikinas = laikinas.Kitas;
                }
                //jeigu randamas
                if(laikinas.Kitas == pb) //pabaigoje
                {
                    pb = laikinas;
                    laikinas = laikinas.Kitas;
                    laikinas.Duom = null;
                    laikinas = null;
                    pb.Kitas = null;
                    return;
                }
                //viduryje
                ss = laikinas;
                laikinas = laikinas.Kitas;
                ss.Kitas = laikinas.Kitas;
                laikinas.Duom = null;
                laikinas = null;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            const string CFd = @"..\..\Knygos.txt";
            const string CFr = @"..\..\Rezultatai.txt";
            if (File.Exists(CFr))
                File.Delete(CFr);
            // ATLIKITE: skaitykite duomenis iš failo į tiesioginį sąrašą Knygos,
            // spausdinkite duomenis, pašalinkite knygas,
            // kurių pavadinime yra daugiau nei nurodytas knygų skaičius,
            // spausdinkite sąrašą.
            Sąrašas Knygos = ĮvestiTiesiog(CFd);
            Spausdinti(CFr, Knygos, "Pradiniu knygų sąrašas");
            Console.Write("Įveskite reikiamą žodžių skaičių: ");
            int zodziuSkaicius = int.Parse(Console.ReadLine());
            Išmesti(Knygos, zodziuSkaicius);
            Spausdinti(CFr, Knygos, "Knygų sąrašas po šalinimo ");

            Console.WriteLine("Programa darbą baigė.");
            Console.ReadKey(true);
        }
        // Skaitomos objektų reikšmės iš failo ir sudedamos į sąrašą tiesiogine tvarka
        // fv – duomenų failo vardas
        // Grąžina - sąrašo objekto nuorodą
        static Sąrašas ĮvestiTiesiog(string fv)
        {
            Sąrašas A = new Sąrašas();
            using (var failas = new StreamReader(fv))
            {
                string pavad;
                int metai;
                int pslSk;
                string eilute;
                while ((eilute = failas.ReadLine()) != null)
                {
                    var eilDalis = eilute.Split(';');
                    pavad = eilDalis[0];
                    metai = Convert.ToInt32(eilDalis[1]);
                    pslSk = Convert.ToInt32(eilDalis[2]);
                    var Knyga = new Knyga(pavad, metai, pslSk);
                    A.DėtiDuomenisT(Knyga);
                }
            }
            return A;
        }
        // Sąrašo A duomenys spausdinami lentele faile fv
        // fv – duomenų failo vardas
        // A - sąrašo objekto adresas
        // antraste - lentelės pavadinimas
        static void Spausdinti(string fv, Sąrašas A, string antraste)
        {
            using (var failas = new StreamWriter(fv, true))
            {
                failas.WriteLine(antraste);
                failas.WriteLine("-------------------------------------");
                failas.WriteLine("Pavadinimas Metai Puslapiai ");
                failas.WriteLine("-------------------------------------");
                // Sąrašo peržiūra, panaudojant sąsajos metodus
                for (A.Pradžia(); A.Yra(); A.Kitas())
                {
                    failas.WriteLine(A.ImtiDuomenis());
                }
                failas.WriteLine("-------------------------------------\n");
            }
        }
        // Iš sąrašo išmeta knygas, kurių pavadinime yra didesnis nei nurodytas žodžių skaičius
        // A - sąrašo objekto adresas
        // zodSkaicius - žodžių skaičius knygos pavadinime
        static void Išmesti(Sąrašas A, int zodSkaicius)
        {
            for(A.Pradžia(); A.Yra(); A.Kitas())
            {
                if(A.ImtiDuomenis().Pavadinimas.Length > zodSkaicius){
                    A.Salinti();
                }
            }
        }
    }
}
