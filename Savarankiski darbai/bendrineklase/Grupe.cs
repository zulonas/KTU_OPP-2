﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bendrineklase
{
    class Grupe<T> where T : Studentas // galima imti tik tas klases 
        //kurių bazinė yra studentas 
    {

        public string Pavadinimas { get; private set; } //uždraudžiame pavadinimo keitimą
        public int Kursas { get; private set; }
        public string Specializacija { get; set; }
        public string Kuratorius { get; set; }
        public List<T> StudentuSarasas { get; private set; }

        public Grupe(string pavadinimas, int kursas, string specializacija, string kuratorius)
        {
            Pavadinimas = pavadinimas;
            Kursas = kursas;
            Specializacija = specializacija;
            Kuratorius = kuratorius;
            StudentuSarasas = new List<T>();
        }

        public List<T> RastiGeriausia()
        {
            double max = StudentuSarasas.Max(a => a.Vidurkis);
            return StudentuSarasas.Where(a => a.Vidurkis == max).ToList();
        }

    }
}
