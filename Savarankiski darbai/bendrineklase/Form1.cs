﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace bendrineklase
{
     public partial class Form1 : Form
    {

        const string CFGrupes = @"../../Grupes.txt";
        const string CFBakalaurai = @"../../Bakalaurai.txt";
        const string CFMagistrantai = @"../../Magistrantai.txt";

        private Dictionary<string, Grupe<Bakalauras>> bakalauruGrupes =
            new Dictionary<string, Grupe<Bakalauras>>();
        private Dictionary<string, Grupe<Magistrantas>> magistrantuGrupes =            new  Dictionary<string, Grupe<Magistrantas>>();

        public Form1()
        {
            InitializeComponent();
            išsaugotiToolStripMenuItem.Enabled = false;
        }


        static void SkaitytiGrupes(string failas, Dictionary<string, Grupe<Bakalauras>>
         bakalauruGrupes, Dictionary<string, Grupe<Magistrantas>> magistrantuGrupes)
        {
            using (StreamReader reader = new StreamReader(failas,
           Encoding.GetEncoding("utf-8")))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split(';');
                    string pozymis = parts[0].Trim();
                    int kursas = int.Parse(parts[1].Trim());
                    string grupe = parts[2].Trim();
                    string specializacija = parts[3].Trim();
                    string kuratorius = parts[4].Trim();
                    switch (pozymis)
                    {
                        case "B":
                            Grupe<Bakalauras> bg = new Grupe<Bakalauras>(grupe, kursas,
                           specializacija, kuratorius);
                            bakalauruGrupes.Add(grupe, bg);
                            break;
                            ;
                        case "M":
                            Grupe<Magistrantas> mg = new Grupe<Magistrantas>(grupe, kursas,
                           specializacija, kuratorius);
                            magistrantuGrupes.Add(grupe, mg);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Dinamiškas puslapiu sukūrimas kiekvienai grupei
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="grupe"></param>
        private void SukurtiGrupesPuslapi<T>(Grupe<T> grupe) where T : Studentas
        {
            TabPage puslapis = new TabPage(grupe.Pavadinimas);
            tabControl1.TabPages.Add(puslapis);

            FlowLayoutPanel skydelis = new FlowLayoutPanel();
            skydelis.Dock = DockStyle.Fill;
            skydelis.FlowDirection = FlowDirection.TopDown;
            puslapis.Controls.Add(skydelis);
            Label kursas = new Label();
            kursas.Text = "Kursas: " + grupe.Kursas;
            kursas.AutoSize = true;
            skydelis.Controls.Add(kursas);
            Label laipsnis = new Label();
            laipsnis.Text = "Laipsnis: " + ((typeof(T) == typeof(Bakalauras))? "Bakalauras" : "Magistras");
            laipsnis.AutoSize = true;
            skydelis.Controls.Add(laipsnis);
            Label specializacija = new Label();
            specializacija.Text = "Specializacija: " + grupe.Specializacija;
            specializacija.AutoSize = true;
            skydelis.Controls.Add(specializacija);
            Label kuratorius = new Label();
            kuratorius.Text = "Kuratorius: " + grupe.Kuratorius;
            kuratorius.AutoSize = true;
            skydelis.Controls.Add(kuratorius);

            DataGridView tinklelis = new DataGridView();
            tinklelis.AutoGenerateColumns = false;
            tinklelis.RowHeadersVisible = false;
            tinklelis.AllowUserToAddRows = false;
            tinklelis.AllowUserToDeleteRows = false;
            tinklelis.AutoSize = true;
            tinklelis.BackgroundColor = Color.White;
            BindingSource duomenuModelis = new BindingSource();
            foreach (Studentas s in grupe.StudentuSarasas)
            {
                duomenuModelis.Add(s);
            }
            //kiekvienas studento objektas atitiks vieną eilutę tinklelyje
            tinklelis.DataSource = duomenuModelis;
            //apsirašom tinklelio stulpelius
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "PazymejimoNr"; //nurodome Studento objekto sąvybės pavadinimą
            column.Name = "Paž. nr."; //nurodome stulpelio pavadinimą
            column.ReadOnly = true;
            tinklelis.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Vardas";
            column.Name = "Vardas";
            column.ReadOnly = true;
            tinklelis.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Pavarde";
            column.Name = "Pavarde";
            column.ReadOnly = true;
            tinklelis.Columns.Add(column);
            DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn();
            combo.DataSource = Enum.GetValues(typeof(Statusas));
            combo.DataPropertyName = "Statusas";
            combo.Name = "Statusas";
            tinklelis.Columns.Add(combo);
            //tikriname T tipą, ir esant reikalui pridedame papildomą stulpelį
            if (typeof(T) == typeof(Magistrantas))
            {
                column = new DataGridViewTextBoxColumn();
                column.DataPropertyName = "Tema";
                column.Name = "Darbo tema";
                column.ReadOnly = true;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                tinklelis.Columns.Add(column);
            }
            skydelis.Controls.Add(tinklelis);

            //Geriausių studentų paieška
            Label gerStudentaspav = new Label();
            gerStudentaspav.Text = $"Geriausi studentai: ";
            gerStudentaspav.AutoSize = true;
            skydelis.Controls.Add(gerStudentaspav);
            foreach (Studentas stud in grupe.RastiGeriausia())
            {
                Label gerStudentas = new Label();
                gerStudentas.Text = $"Studentas: {stud.ToString()}\n";
                gerStudentas.Text += $"Paz. Nr - {stud.PazymejimoNr}\n";
                gerStudentas.Text += $"Vidurkis - {stud.Vidurkis}\n";
                gerStudentas.Text += $"Statusas - {stud.Statusas}";
                gerStudentas.AutoSize = true;
                skydelis.Controls.Add(gerStudentas);
            }

            //TODO vėliau studentų sąrašo atvaizdavimą pridėsime čia
            puslapis.Refresh();
        }

        private void SukurtiPagalLaipsni<T>(Dictionary<string,
            Grupe<T>> grupe, string pavadinimas) where T : Studentas
        {
            TabPage puslapis = new TabPage(pavadinimas);
            tabControl1.TabPages.Add(puslapis);

            FlowLayoutPanel skydelis = new FlowLayoutPanel();
            skydelis.Dock = DockStyle.Fill;
            skydelis.FlowDirection = FlowDirection.TopDown;
            puslapis.Controls.Add(skydelis);

            DataGridView tinklelis = new DataGridView();
            tinklelis.AutoGenerateColumns = false;
            tinklelis.RowHeadersVisible = false;
            tinklelis.AllowUserToAddRows = false;
            tinklelis.AllowUserToDeleteRows = false;
            tinklelis.AutoSize = true;
            tinklelis.BackgroundColor = Color.White;
            BindingSource duomenuModelis = new BindingSource();
            List<Studentas> Visi = new List<Studentas>();

            foreach (KeyValuePair<string, Grupe<T>> irasas in grupe)
            {
                foreach (Studentas s in irasas.Value.StudentuSarasas)
                {
                    Visi.Add(s);
                }
            }
            Visi.Sort();

            foreach(Studentas stud in Visi)
            {
                duomenuModelis.Add(stud);
            }
            //kiekvienas studento objektas atitiks vieną eilutę tinklelyje
            tinklelis.DataSource = duomenuModelis;
            //apsirašom tinklelio stulpelius
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "PazymejimoNr"; //nurodome Studento objekto sąvybės pavadinimą
            column.Name = "Paž. nr."; //nurodome stulpelio pavadinimą
            column.ReadOnly = true;
            tinklelis.Columns.Add(column);
            DataGridViewColumn vardas = new DataGridViewTextBoxColumn();
            vardas = new DataGridViewTextBoxColumn();
            vardas.DataPropertyName = "Vardas";
            vardas.Name = "Vardas";
            vardas.ReadOnly = true;
            tinklelis.Columns.Add(vardas);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Pavarde";
            column.Name = "Pavarde";
            column.ReadOnly = true;
            tinklelis.Columns.Add(column);
            DataGridViewComboBoxColumn combo = new DataGridViewComboBoxColumn();
            combo.DataSource = Enum.GetValues(typeof(Statusas));
            combo.DataPropertyName = "Statusas";
            combo.Name = "Statusas";
            tinklelis.Columns.Add(combo);
            //tikriname T tipą, ir esant reikalui pridedame papildomą stulpelį
            if (typeof(T) == typeof(Magistrantas))
            {
                column = new DataGridViewTextBoxColumn();
                column.DataPropertyName = "Tema";
                column.Name = "Darbo tema";
                column.ReadOnly = true;
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                tinklelis.Columns.Add(column);
            }
            skydelis.Controls.Add(tinklelis);

            //TODO vėliau studentų sąrašo atvaizdavimą pridėsime čia
            puslapis.Refresh();
        }

        private void BaigtiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ĮvestiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SkaitytiGrupes(CFGrupes, bakalauruGrupes, magistrantuGrupes);
            SkaitytiBakalaurus(CFBakalaurai, bakalauruGrupes);
            SkaitytiMagistrantus(CFMagistrantai, magistrantuGrupes);
            foreach (KeyValuePair<string, Grupe<Bakalauras>> irasas in bakalauruGrupes)
            {
                SukurtiGrupesPuslapi(irasas.Value);
            }
            foreach (KeyValuePair<string, Grupe<Magistrantas>> irasas in magistrantuGrupes)
            {
                SukurtiGrupesPuslapi(irasas.Value);
            }
            įvestiToolStripMenuItem.Enabled = false; 
        }

        static void SkaitytiBakalaurus(string failas, Dictionary<string,
        Grupe<Bakalauras>> bakalauruGrupes)
        {
            using (StreamReader reader = new StreamReader(failas,
            Encoding.GetEncoding("utf-8")))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split(';');
                    string vardas = parts[0].Trim();
                    string pavarde = parts[1].Trim();
                    string pazymejimoNr = parts[2].Trim();
                    string grupe = parts[3].Trim();
                    double vidurkis = GetDouble(parts[4].Trim());
                    Statusas statusas = (Statusas)Enum.Parse(typeof(Statusas),
                        parts[5].Trim());
                    Bakalauras bakalauras = new Bakalauras(vardas, pavarde, pazymejimoNr,
                        vidurkis, statusas);
                    bakalauruGrupes[grupe].StudentuSarasas.Add(bakalauras);
                }
            }
        }

        /// <summary>
        /// Metodas skirtas paimti double reiksme bet kokiu formatu
        /// </summary>
        /// <param name="value">bandoma parse'inti double reikšmė</param>
        /// <returns>reikšmė kuria kompiuteris normaliai supranta</returns>
        static double GetDouble(string reiksme)
        {
            double rezutatas;

            reiksme = reiksme.Replace(',', '.');
            if (!double.TryParse(reiksme, System.Globalization.NumberStyles.Any,
                System.Globalization.CultureInfo.GetCultureInfo("en-US"),
                out rezutatas))
            {
                rezutatas = -1;
                MessageBox.Show("Nepavyko nuskaityti double reikšmių");
            }

            return rezutatas;
        }


        static void SkaitytiMagistrantus(string failas, Dictionary<string,
        Grupe<Magistrantas>> magistrantuGrupes)
        {
            using (StreamReader reader = new StreamReader(failas,
           Encoding.GetEncoding("utf-8")))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split(';');
                    string vardas = parts[0].Trim();
                    string pavarde = parts[1].Trim();
                    string pazymejimoNr = parts[2].Trim();
                    string grupe = parts[3].Trim();
                    double vidurkis = GetDouble(parts[4].Trim());
                    Statusas statusas = (Statusas)Enum.Parse(typeof(Statusas),
                   parts[5].Trim());
                    string tema = parts[6].Trim();
                    Magistrantas magistrantas = new Magistrantas(vardas, pavarde,
                   pazymejimoNr, vidurkis, statusas, tema);
                    magistrantuGrupes[grupe].StudentuSarasas.Add(magistrantas);
                }
            }
        }

        private void IšsaugotiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Rezultaitai nespausdinami");
        }

        private void ParodytiSąrašusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SukurtiPagalLaipsni(bakalauruGrupes, "Visi bakalaurai");
            SukurtiPagalLaipsni(magistrantuGrupes, "Visi magistrantai");
            parodytiSąrašusToolStripMenuItem.Enabled = false;

        }
    }
}
