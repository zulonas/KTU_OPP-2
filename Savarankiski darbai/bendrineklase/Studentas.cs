﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bendrineklase
{

    enum Statusas
    {
        VF, //valstybės finansuojama vieta
        VNF //valstybės nefinansuojama vieta
    }

    abstract class Studentas: IComparable<Studentas>
    {
        public string Vardas { get; set; }
        public string Pavarde { get; set; }
        public string PazymejimoNr { get; set; }
        public double Vidurkis { get; set; }
        public Statusas Statusas { get; set; }

        public Studentas() { }

        public Studentas(string vardas, string pavarde, string pazymejimoNr,
        double vidurkis, Statusas statusas)
        {
            Vardas = vardas;
            Pavarde = pavarde;
            PazymejimoNr = pazymejimoNr;
            Vidurkis = vidurkis;
            Statusas = statusas;
        }

        public override string ToString()
        {
            return Vardas + " " + Pavarde;
        }

        public int CompareTo(Studentas other)
        {
            int vardas = string.Compare(this.Vardas, other.Vardas,
                StringComparison.CurrentCulture);
            int pavarde = string.Compare(this.Pavarde, other.Pavarde,
                StringComparison.CurrentCulture);

            if (vardas == 0)
                return pavarde;
            return vardas;
        }
    }
}
