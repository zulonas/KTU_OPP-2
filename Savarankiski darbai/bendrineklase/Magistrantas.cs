﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bendrineklase
{
    class Magistrantas : Studentas
    {
        public string Tema { get; set; }

        public Magistrantas()
        {
        }

        public Magistrantas(string vardas, string pavarde, string pazymejimoNr, double vidurkis, Statusas statusas, string tema)
        : base(vardas, pavarde, pazymejimoNr, vidurkis, statusas)
        {
            Tema = tema;
        }
    }
}
