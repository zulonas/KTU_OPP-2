﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bendrineklase
{
    class Bakalauras : Studentas
    {
        public Bakalauras()
        {
        }

        public Bakalauras(string vardas, string pavarde, string pazymejimoNr, double vidurkis, Statusas statusas)
            : base(vardas, pavarde, pazymejimoNr, vidurkis, statusas)
        {
        }
    }
}
