﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace StudentaiListMenu
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// KONSTANTOS
        /// </summary>
        const string CFd = "..\\..\\Studentai.txt"; // duomenų failo varda
        const string CFr = "..\\..\\Rezultatai.txt"; // rezultatų failo vardas
        const string CFvs = "..\\..\\VertinimoSistema.txt"; // vertinimo sistema
        const string CFu = "..\\..\\Uzduotis.txt"; // užduoties failo vardas
        const string CFn = "..\\..\\Nurodymai.txt"; // nurodymų failo vardas
        ///----------------------------------------------------
        /// <summary>
        /// KINTAMIEJI (OBJEKTAI, OBJEKTŲ MASYVAI)
        /// </summary>
        private List<Studentas> StudentuTestas; // studentų objektų masyvas
        private List<Pazymys> Pazymiai; // pažymių objektų masyvas
        //------------------------------------------------------
        public Form1()
        {
            InitializeComponent();
            // Nurodyti meniu punktai padaromi pasyviais
            spausdintiToolStripMenuItem.Enabled = false;
            studentųSkaičiusToolStripMenuItem.Enabled = false;
            studentoĮvertinimasToolStripMenuItem.Enabled = false;
            vaikinųPažymiųVidurkisToolStripMenuItem.Enabled = false;
            //--------------------------------------------------
        }

        /// <summary>
        /// Meniu punkto "Įvesti" atliekami veiksmai
        /// Duomenų failo vardas išrenkamas naudojant openFileDialog komponentą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void įvestiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pazymiai = SkaitytiVertinimoSistemaList(CFvs);
            // Komponento vertinimai užpildymas pažymiais
            foreach (Pazymys paz in Pazymiai)
                vertinimai.Items.Add(paz.ToString());
            vertinimai.SelectedIndex = 0; // parenkama 1-oji reikšmė
            // OpenFileDialog komponento savybių nustatymas
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.Title = "Pasirinkite duomenų failą";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string fv = openFileDialog1.FileName;
                duomenys.LoadFile(fv, RichTextBoxStreamType.PlainText);
                StudentuTestas = SkaitytiStudList(fv);
            }
            // Meniu punktų nustatymai
            įvestiToolStripMenuItem.Enabled = false;
            spausdintiToolStripMenuItem.Enabled = true;
            studentųSkaičiusToolStripMenuItem.Enabled = true;
            studentoĮvertinimasToolStripMenuItem.Enabled = true;
            vaikinųPažymiųVidurkisToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// Meniu punkto "Spausdinti" atliekami veiksmai
        /// Rezultatų failo vardas išrenkamas naudojant saveFileDialog komponentą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void spausdintiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // SaveFileDialog komponento savybių nustatymas
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.Title = "Pasirinkite rezultatų failą";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
            string fv = saveFileDialog1.FileName;
                // Jeigu reikia rezultatų failas išvalomas
                if (File.Exists(fv))
                    File.Delete(fv);
                SpausdintiStudList(fv, StudentuTestas,
                "Studentų sąrašas (testo rezultatai)");
                //----------------------------------------------------
                // Komponento dataGridView1 užpildymas duomenimis
                rezultatai.ColumnCount = 4;
                rezultatai.Columns[0].Name = "Nr.";
                rezultatai.Columns[0].Width = 40;
                rezultatai.Columns[1].Name = "Pavardė ir vardas";
                rezultatai.Columns[1].Width = 280;
                rezultatai.Columns[2].Name = "Pažymys";
                rezultatai.Columns[2].Width = 80;
                rezultatai.Columns[3].Name = "Lytis";
                rezultatai.Columns[3].Width = 150;
                for (int i = 0; i < StudentuTestas.Count; i++)
                {
                    Studentas studentas = StudentuTestas[i];
                    rezultatai.Rows.Add(i + 1, studentas.PavVrd, 
                        studentas.Pazym, ((studentas.ArVyras)
                        ?"Vyras":"Moteris"));
                }
                //----------------------------------------------------
            }
        }

        /// <summary>
        /// Meniu punkto "Studentų skaičius" atliekami veiksmai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void studentųSkaičiusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ivertis = vertinimai.SelectedItem.ToString().TrimStart();
            string[] eilDalis = ivertis.Split(' ');
            int pazymys = Int32.Parse(eilDalis[0]);
            int kiekis = Kiekis(StudentuTestas, pazymys);
            if (kiekis > 0)
                rezultatas.Text = "Studentų skaičius: " + kiekis.ToString();
            else
                rezultatas.Text = "Tokių studentų nėra.";
        }

        /// <summary>
        /// Meniu punkto "Studento įvertinimas" atliekami veiksmai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void studentoĮvertinimasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pavVrd = pavardeVrd.Text;
            int index = StudentoIndeksas(StudentuTestas, pavVrd);
            if (index > -1)
            {
                Studentas stud = StudentuTestas[index];
                int pazymys = stud.Pazym;
                pavardeVrd.Text = pavardeVrd.Text + " -> pažymys: " + pazymys.ToString();
            }
            else
                pavardeVrd.Text = pavardeVrd.Text + " -> tokio studento (-ės) nėra.";
        }

        /// <summary>
        /// Meniu punkto "Baigti" atliekami veiksmai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void baigtiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Meniu punkto "Užduotis" atliekami veiksmai:
        /// parodomas užduoties failo turinys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void užduotisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            duomenys.LoadFile(CFu, RichTextBoxStreamType.PlainText);
        }

        /// <summary>
        /// Meniu punkto "Nurodymai vartotojui" atliekami veiksmai
        /// parodomas nurodymų vartotojui failo turinys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nurodymaiVartotojuiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            duomenys.LoadFile(CFn, RichTextBoxStreamType.PlainText);
        }


        //------------------------------------------------------
        /// <summary>
        /// Skaito duomenis iš failo į dinaminį masyvą.
        /// </summary>
        /// <param name="fv"> duomenų failo vardas </param>
        /// <returns> grąžina dinaminio masyvo nuorodą </returns>
        static List<Studentas> SkaitytiStudList(string fv)
        {
            List<Studentas> StudTestas = new List<Studentas>(); // studentų objektų masyvas
            using (StreamReader srautas = new StreamReader(fv, Encoding.GetEncoding(1257)))
            {
                string eilute; // visa duomenų failo eilutė
                while ((eilute = srautas.ReadLine()) != null)
                {
                    string[] eilDalis = eilute.Split(';');
                    string pavVrd = eilDalis[0];
                    int pazym = int.Parse(eilDalis[1]);
                    bool arVyras = (char.Parse(eilDalis[2].Trim()) == 'V') ? true : false;
                    Studentas studentas = new Studentas(pavVrd, pazym, arVyras);
                    StudTestas.Add(studentas);
                }
            }
            return StudTestas;
        }
        //--------------------------------------------------

        //--------------------------------------------------
        /// <summary>
        /// Spausdina dinaminio masyvo duomenis faile lentele.
        /// </summary>
        /// <param name="fv"> rezultatų failo vardas </param>
        /// <param name="StudTestas"> dinaminis masyvas studentų duomenims saugoti </param>
        /// <param name="antraste"> užrašas virš lentelės </param>
        static void SpausdintiStudList(string fv, List<Studentas> StudTestas,
         string antraste)
        {
            const string virsus =
            "-----------------------------------\r\n"
            + " Nr. Pavardė ir vardas Pažymys Lytis \r\n"
            + "-----------------------------------";
            using (var fr = new StreamWriter(File.Open(fv, FileMode.Append),
            Encoding.GetEncoding(1257)))
            {
                fr.WriteLine("\n " + antraste);
                fr.WriteLine(virsus);
                for (int i = 0; i < StudTestas.Count; i++)
                {
                    Studentas stud = StudTestas[i];
                    fr.WriteLine("{0, 3} {1}", i + 1, stud);
                }
                fr.WriteLine("-----------------------------------\n");
            }
        }
        //-------------------------------------------------


        //------------------------------------------------------
        /// <summary>
        /// Skaito vertinimo sistemą iš failo į dinaminį masyvą.
        /// </summary>
        /// <param name="fv"> duomenų failo vardas </param>
        /// <returns> grąžina dinaminio masyvo nuorodą </returns>
        static List<Pazymys> SkaitytiVertinimoSistemaList(string fv)
        {
            List<Pazymys> VertSistema = new List<Pazymys>(); // pažymių objektų masyvas
            using (StreamReader srautas = new StreamReader(fv, Encoding.GetEncoding(1257)))
            {
            string eilute; // visa duomenų failo eilutė
                while ((eilute = srautas.ReadLine()) != null)
                {
                    string[] eilDalis = eilute.Split(';');
                    int pazym = int.Parse(eilDalis[0]);
                    string pazReiksme = eilDalis[1];
                    Pazymys pazymys = new Pazymys(pazym, pazReiksme);
                    VertSistema.Add(pazymys);
                }
            }
            return VertSistema;
        }
        //------------------------------------------------------

        //------------------------------------------------------------------------
        /// <summary>
        /// Suskaičiuoja studentų, kurių pažymiai lygūs nurodytam pažymiui, skaičių.
        /// </summary>
        /// <param name="StudTestas"> List'as studentų duomenims saugoti </param>
        /// <param name="pazymys"> nurodytas pažymys </param>
        /// <returns> grąžina suskaičiuotą studentų skaičių </returns>
        static int Kiekis(List<Studentas> StudTestas, int pazymys)
        {
            int kiek = 0;
            for (int i = 0; i < StudTestas.Count; i++)
            {
                Studentas stud = StudTestas[i];
                if (stud.Pazym == pazymys)
                    kiek++;
            }
            return kiek;
        }
        //-------------------------------------------------


        /// <summary>
        /// Ieško nurodytos pavardės ir vardo studento.
        /// </summary>
        /// <param name="StudTestas"> List'as studentų duomenims saugoti </param>
        /// <param name="pavVrd"> studento pavardė ir vardas </param>
        /// <returns> grąžina studento indeksą arba -1, jeigu ieškomo studento nėra
        ///</returns>
        static int StudentoIndeksas(List<Studentas> StudTestas, string pavVrd)
        {
            for (int i = 0; i < StudTestas.Count; i++)
            {
                if (StudTestas[i].PavVrd == pavVrd)
                    return i;
            }
            return -1;
        }

        /// <summary>
        /// Meniu punkto "Vaikinų pažymių vidurkis" atliekami veiksmai:
        /// parodomas pažymių vidurkis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void vaikinųPažymiųVidurkisToolStripMenuItem_Click(object 
            sender, EventArgs e)
        {
            if (VaikinuPažymiųVidurkis() == -1)
            {
                MessageBox.Show("Vaikinų nėra", "Vaikinų " + "pažymių vidurkis");
            }
            else
            {
                MessageBox.Show(VaikinuPažymiųVidurkis().ToString(), "Vaikinų " +
                "pažymių vidurkis");
            }
        }


        /// <summary>
        /// Metodas apskaičiuojantis vyrų pažymių kiekį
        /// </summary>
        /// <returns></returns>
        private double VaikinuPažymiųVidurkis()
        {
            int kiekis = 0;
            int pazymiai = 0;
            for (int i = 0; i < StudentuTestas.Count; i++)
            {
                if (StudentuTestas[i].ArVyras)
                {
                    kiekis++;
                    pazymiai += StudentuTestas[i].Pazym;
                }
            }
            if (kiekis == 0) return -1;
            return (double)pazymiai / kiekis;
        }
    }
}
