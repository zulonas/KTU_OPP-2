﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pratybos
{
    class Prekes : Preke
    {
        public int Kiekis { get; set; }

        public Prekes()
        {

        }

        public Prekes(string pav = "", string tipas = "", double kaina = 0.0,
            int kiek = 0) : base(pav, tipas, kaina)
        {
            Kiekis = kiek;
        }

        public override string ToString()
        {
            string eilute = "";
            eilute += string.Format("{0} {1,7:d}", base.ToString(), Kiekis);
            return eilute;
        }

        /// <summary>
        /// prekės sumos skaičiavimas
        /// </summary>
        /// <returns></returns>
        public override double Suma()
        {
            return Kiekis * Kaina;
        }

        public override void Padidintikaina(int procentai)
        {
            Kaina *= ((double)(100 + procentai) / 100);
        }
    }
}
