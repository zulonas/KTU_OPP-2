﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pratybos
{
    abstract class Preke : IComparable<Preke>
    {
        public string Pavadinimas { get; set; } 
        public string Tipas { get; set; }
        public double Kaina { get; set; }

        public Preke()
        {

        }

        public Preke(string pavadinimas = "", string tipas = "", double kaina = 0.0)
        {
            Pavadinimas = pavadinimas;
            Tipas = tipas;
            Kaina = kaina;
        }

        public override string ToString()
        {
            string eilute = "";
            eilute += string.Format(" {0, -16} {1, -20} {2, 7:f}",
                Pavadinimas, Tipas, Kaina);
            return eilute;
        }

        public int CompareTo(Preke kita)
        {
            int poz = String.Compare(this.Pavadinimas, kita.Pavadinimas,
                StringComparison.CurrentCulture);

            return ((Kaina < kita.Kaina) 
                || ((Kaina == kita.Kaina) && (poz > 0))) ? 1 : -1;
        }

        /// <summary>
        /// abstraktus metodas. Bus realizuotas išvestinėje klasėje
        /// </summary>
        /// <returns></returns>
        public abstract double Suma();

        public abstract void Padidintikaina(int procentai);
    }
}
