﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Pratybos
{
    class Program
    {
        const string CFd2 = @"../../Prekes2.txt";
        const string CFd1 = @"../../Prekes.txt";
        const string CFr = @"../../Rezultatai.txt";

        static void Main(string[] args)
        {
            if (File.Exists(CFr))
                File.Delete(CFr);
            //Pradiniai duomenys
            List<Prekes> PrekiuList = new List<Prekes>();
            Skaityti(CFd1, PrekiuList);
            Spausdinti(CFr, PrekiuList, "Pradinis sąrašas");

            //Papildomi duomenys
            List<Prekes> PapildomosPrekes = new List<Prekes>();
            Skaityti(CFd2, PapildomosPrekes);
            Spausdinti(CFr, PapildomosPrekes, "Papildomų prekių pradinis sąrašas");

            List<Prekes> NaujasPrekiuList = new List<Prekes>();
            Perrašyti(PrekiuList, NaujasPrekiuList);
            Console.WriteLine("Įveskite prekės tipą");
            string prekėsTipas = Console.ReadLine();
            NaujasPrekiuList.RemoveAll(item => item.Tipas == prekėsTipas);
            PapildomosPrekes.RemoveAll(item => item.Tipas == prekėsTipas);
            if (NaujasPrekiuList.Count() > 0)
            {
                Spausdinti(CFr, NaujasPrekiuList, "Suformuotas sąrašas");
                NaujasPrekiuList.Sort();
                Spausdinti(CFr, NaujasPrekiuList, "Rikiuotas suformuotas sąrašas");
                using (var fr = File.AppendText(CFr))
                {
                    double vid = NaujasPrekiuList.Average(item => item.Kiekis);
                    fr.WriteLine("Kiekio vidurkis = {0,5:f}", vid);
                    double prekiuSuma = Sum(NaujasPrekiuList);
                    fr.WriteLine("Naujo sąrašo prekių visa suma = {0,5:f}\n", prekiuSuma);
                }
            }
            else if (NaujasPrekiuList.Count() == 0)
            {
                using (var fr = File.AppendText(CFr))
                {
                    fr.WriteLine("Naujas sąrašas tuščias\n\n");
                }
            }

            //iterpimas į rikiuotą 
            IterpiIrikiuota(NaujasPrekiuList, PapildomosPrekes);
            Spausdinti(CFr, NaujasPrekiuList, "Iterptos naujos prekės");


            //duomenų padidinimas
            PadidintiKainas(10, PrekiuList, PapildomosPrekes, NaujasPrekiuList);
            Spausdinti(CFr, PrekiuList, "Pradinis padidintas");
            Spausdinti(CFr, PapildomosPrekes, "Papildomas padidintas");
            Spausdinti(CFr, NaujasPrekiuList, "Suformuotas padidintas");
        }

        /// <summary>
        /// Skaitomi duomenys iš failo
        /// </summary>
        /// <param name="fv">duomenų failo vardas</param>
        /// <param name="PrekiuList">objektų rinkinys prekių duomenims saugoti</param>
        static void Skaityti(string fv, List<Prekes> PrekiuList)
        {
            using (StreamReader reader = new StreamReader(fv, Encoding.GetEncoding("utf-8")))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split(';');
                    string pav = parts[0].Trim();
                    string tema = parts[1].Trim();
                    double kaina = GetDouble(parts[2]);
                    int kiek = int.Parse(parts[3]);
                    Prekes pr = new Prekes(pav, tema, kaina, kiek);
                    PrekiuList.Add(pr);
                }
            }
        }

        /// <summary>
        /// Spausdinami duomenys
        /// </summary>
        /// <param name="fv">rezultatu failo vardas</param>
        /// <param name="PrekiuList">objektų rinkinys prekių duomenims saugoti</param>
        /// <param name="info">lentelės pavadinimas</param>
        static void Spausdinti(string fv, List<Prekes> PrekiuList, string info)
        {
            const string virsus =
            "----------------------------------------------------------\r\n"
            + " Pavadinimas      Tipas                Kaina     Kiekis \r\n"
            + "----------------------------------------------------------";
            using (var fr = File.AppendText(fv))
            {
                fr.WriteLine(info);
                fr.WriteLine(virsus);
                foreach(Preke preke in PrekiuList)
                {
                    fr.WriteLine("{0}", preke);
                }
                if (PrekiuList.Count == 0) fr.WriteLine("Prekių nėra"); 
                fr.WriteLine("--------------------------------------------------" +
                "--------\r\n");
            }
        }

        /// <summary>
        /// Perrašo vieno dinaminio masyvo elementus į kitą masyvą
        /// 
        /// </summary>
        /// <param name="PrekiuList">turimas objektų rinkinys prekių duomenims /// saugoti</param>
        /// <param name="NaujasPrekiuList">>formuojamas objektų rinkinys</param>
        static void Perrašyti(List<Prekes> PrekiuList,
            List<Prekes> NaujasPrekiuList)
        {
            for (int i = 0; i < PrekiuList.Count; i++)
            {
                Prekes p = new Prekes(
                    PrekiuList[i].Pavadinimas,
                    PrekiuList[i].Tipas,
                    PrekiuList[i].Kaina,
                    PrekiuList[i].Kiekis);
                NaujasPrekiuList.Add(p);
            }
        }

        /// <summary>
        /// Skaičiuojama visa suma
        /// </summary>
        /// <param name="PrekiuList">– objektų rinkinys prekių duomenims
        /// saugoti</param>
        /// <returns></returns>
        static double Sum(List<Prekes> PrekiuList)
        {
            return PrekiuList.Sum(a => a.Suma());
        }

        /// <summary>
        /// Metodas skirtas surasti vieta įterpti naujai prekei
        /// </summary>
        /// <param name="PrekiuList">tikrinamas sąrašas</param>
        /// <param name="preke">lyginama prekė</param>
        /// <returns>rastas indeksas arba -1 jeigu sarašas surikiuotas
        /// teisingai</returns>
        static int Paieska(List<Prekes> PrekiuList, Prekes preke)
        {
            for(int x=0; x< PrekiuList.Count; x++)
            {
                if (PrekiuList[x].CompareTo(preke) == 1)
                {
                    return x;
                }
            }
            return -1;
        }

        /// <summary>
        /// Įterpia naują prekių objektą į rikuuotą Sąrašą
        /// </summary>
        /// <param name="PrekiuList">Rikiuotas sąrašas</param>
        /// <param name="preke">Įterpiamas objektas</param>
        static void IterptiIrikiuota(List<Prekes> PrekiuList, Prekes preke)
        {
            int rezultatas = Paieska(PrekiuList, preke);
            if (rezultatas > -1)
                PrekiuList.Insert(rezultatas, preke);
            else
                PrekiuList.Add(preke);
        }

        /// <summary>
        /// Įterpia į rikiuotą sąrašą elementus iš nerikiuoto sąrašo
        /// </summary>
        /// <param name="Pagrindinis">Surikiuotas į kurį įterpiama</param>
        /// <param name="Iterpiamas">Nesurikiuotas</param>
        static void IterpiIrikiuota(List<Prekes> Pagrindinis, List<Prekes> Iterpiamas)
        {
            foreach(Prekes preke in Iterpiamas)
            {
                Prekes naujas = new Prekes();
                Prekes pr = new Prekes(preke.Pavadinimas,
                                        preke.Tipas,
                                        preke.Kaina,
                                        preke.Kiekis);
                IterptiIrikiuota(Pagrindinis, pr);
            }
        }

        /// <summary>
        /// Metodas skirtas paimti double reiksme bet kokiu formatu
        /// </summary>
        /// <param name="value">bandoma parse'inti double reikšmė</param>
        /// <returns>double reikšmė</returns>
        private static double GetDouble(string reiksme)
        {
            double rezutatas;

            reiksme = reiksme.Replace(',', '.');
            if (!double.TryParse(reiksme, System.Globalization.NumberStyles.Any,
                System.Globalization.CultureInfo.GetCultureInfo("en-US"),
              out rezutatas))
            {
                rezutatas = -1;
            }

            return rezutatas;
        }

        /// <summary>
        /// Padidina pasirinkto sąrašo reikšme 
        /// </summary>
        /// <param name="procentai">kiek padidinti</param>
        /// <param name="sarasas">pasirinktas sąrašas</param>
        static void PadidintiKaina(int procentai, List<Prekes> sarasas)
        {
            foreach(Prekes preke in sarasas)
            {
                preke.Padidintikaina(procentai);
            }
        }
        
        /// <summary>
        /// Metodas skirtas padidinti kainas įvestiems sąrašams
        /// </summary>
        /// <param name="procentai">kiek procentų padidinti kainas</param>
        /// <param name="Pradinis"></param>
        /// <param name="Papildytas"></param>
        /// <param name="Naujas"></param>
        static void PadidintiKainas(int procentai, List<Prekes> Pradinis,
            List<Prekes> Papildytas, List<Prekes> Naujas)
        {
            PadidintiKaina(procentai, Pradinis);
            PadidintiKaina(procentai, Papildytas);
            PadidintiKaina(procentai, Naujas);
        }
    }
}
