﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_klausimas
{
    abstract class Kandidatas : Object
    {
        protected const double BazinisDydis = 800.00; // Bazinis atlyginimo dydis
        protected string PavVrd { get; set; } // Pavardė ir vardas
        protected int Amzius { get; set; } // Amžius
        protected double Stažas { get; set; } // Darbo stažas (metais)
                                              // Klasės konstruktorius
        public Kandidatas(string PavVrd = "", int Amzius = 0, double Stažas = 0.0)
        {
            this.PavVrd = PavVrd;
            this.Amzius = Amzius;
            this.Stažas = Stažas;
        }
        // Abstraktus metodas
        public abstract double Atlyginimas();
        public override string ToString()
        {
            // ATLIKITE: Užklokite kandidato spausdinimo eilute metodą
            return PavVrd;
        }
    }
    class Programuotojas : Kandidatas
    {
        // ATLIKITE: Aprašykite klasės savybes ir konstruktorių
        double komandDarbPatirtis;
        int nusiskundimai;

        public Programuotojas(string PavVrd, int Amzius, double Stažas,
            double komand=0, int nusiskund = 0): base(PavVrd, Amzius, Stažas)
        {
            komandDarbPatirtis = komand;
            nusiskundimai = nusiskund;
        }

        // ATLIKITE: Užklokite programuotojo atlyginimo skaičiavimo metodą
        public override double Atlyginimas()
        {
            return BazinisDydis + ((BazinisDydis * 0.2) * 
                (1.1 * komandDarbPatirtis)) + ((0.1 * BazinisDydis) 
                * (-1 * nusiskundimai));
        }

        // ATLIKITE: Užklokite programuotojo spausdinimo eilute metodą
        public override string ToString()
        {
            return base.ToString() + " " + Atlyginimas();
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            // Programuotojų objektų masyvas P(n)
            int n = 3;
            Programuotojas[] P = new Programuotojas[n];
            // P(n) objektų užpildymas reikšmėmis
            P[0] = new Programuotojas("Programuotojas1", 29, 1.1, 1.5, 0);
            P[1] = new Programuotojas("Programuotojas2", 39, 11.5, 2.2, 3);
            P[2] = new Programuotojas("Programuotojas3", 30, 3.0, 3.6, 0);
            // ATLIKITE: Papildykite Main metodą reikiamais veiksmais

            Spausdinti(P, n);
            Console.ReadKey(true);
        }
        public static void Spausdinti(Kandidatas[] K, int kn)
        {
            for(int x=0; x<kn; x++)
            {
                Console.WriteLine(K[x]);
            }
        }
    }
}
