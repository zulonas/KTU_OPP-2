﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilieji_irenginiai
{
    /// <summary>
    /// Mobiliojo įrenginio duomenų saugojimo klasė
    /// </summary>
    public class Mobilus
    {
        public string Modelis { get; set; } // modelio pavadinimas
        public string Tipas { get; set; } // įrenginio tipas
        public int Baterija { get; set; } // baterijos veikimo trukmė

        /// <summary>
        /// konstruktorius
        /// </summary>
        /// <param name="modelis"></param>
        /// <param name="tipas"></param>
        /// <param name="baterija"></param>
        public Mobilus(string modelis = "", string tipas = "", int baterija = 0)
        {
            this.Modelis = modelis;
            this.Tipas = tipas;
            this.Baterija = baterija;
        }

        /// <summary>
        /// objekto naujos reikšmės
        /// </summary>
        /// <param name="a">modelio pavadinimas</param>
        /// <param name="b">įrenginio tipas</param>
        /// <param name="c">baterijos veikimo trukmė</param>
        public void Dėti(string a, string b, int c)
        {
            Modelis = a;
            Tipas = b;
            Baterija = c;
        }

        public override string ToString()
        {
            string eilute;
            eilute = string.Format("|{0, -30}| {1, -20} | {2, 8:f} |",
            Modelis, Tipas, Baterija);
            return eilute;
        }

        public override bool Equals(object objektas)
        {
            Mobilus telef = objektas as Mobilus;
            return telef.Tipas == Tipas && telef.Modelis == Modelis && telef.Baterija ==
            Baterija;
        }

        /// <summary>
        /// Užklotas metodas GetHashCode()
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Užklotas operatorius >= (dviejų įrenginių palyginimui pagal baterijos
        /// veikimo trukmę ir modelio pavadinimą)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator >=(Mobilus pirmas, Mobilus antras)
        {
            int poz = String.Compare(pirmas.Modelis, antras.Modelis,
            StringComparison.CurrentCulture);
            return pirmas.Baterija > antras.Baterija || pirmas.Baterija == antras.Baterija &&
            poz > 0;
        }

        /// <summary>
        /// Užklotas operatorius <= (dviejų įrenginių palyginimui pagal baterijos veikimo trukmę ir modelio pavadinimą)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator <=(Mobilus pirmas, Mobilus antras)
        {
            int poz = String.Compare(pirmas.Modelis, antras.Modelis,
            StringComparison.CurrentCulture);
            return pirmas.Baterija < antras.Baterija || pirmas.Baterija == antras.Baterija &&
            poz < 0;
        }

        /// <summary>
        /// Užklotas operatorius == (įrenginių tipui palyginti)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator ==(Mobilus pirmas, Mobilus antras)
        {
            return pirmas.Tipas == antras.Tipas;
        }

        /// <summary>
        /// Užklotas operatorius != (įrenginių tipui palyginti)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator !=(Mobilus pirmas, Mobilus antras)
        {
            return pirmas.Tipas != antras.Tipas;
        }

        /// <summary>
        /// Užklotas operatorius > (dviejų įrenginių palyginimui pagal baterijos veikimo trukmę)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator >(Mobilus pirmas, Mobilus antras)
        {
            return pirmas.Baterija > antras.Baterija;
        }

        /// <summary>
        /// Užklotas operatorius < (dviejų įrenginių palyginimui pagal baterijos veikimo trukmę)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator <(Mobilus pirmas, Mobilus antras)
        {
            return pirmas.Baterija < antras.Baterija;
        }
    }
}