﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mobilieji_irenginiai
{
    /// <summary>
    /// Mobiliųjų įrenginių vienkryptis sąrašas
    /// </summary>
    public sealed class Studentas
    {
        Mazgas pr; // sąrašo pradžios nuoroda
        Mazgas d; // sąsajos nuoroda
        public string VardasPavarde { get; set; }

        /// <summary>
        /// Konstruktorius be parametrų
        /// </summary>
        public Studentas()
        {
            this.pr = null;
            this.d = null;
        }

        /// <summary>
        /// Sąsajos metodai Sąsajai priskiriama sąrašo pradžia
        /// </summary>
        public void Pradžia()
        {
            d = pr;
        }

        /// <summary>
        /// Sąsajai priskiriamas sąrašo sekantis elementas
        /// </summary>
        public void Kitas()
        {
            d = d.Kitas;
        }

        /// <summary>
        /// Grąžina true, jeigu sąsaja netuščia; false - priešingu atveju
        /// </summary>
        /// <returns></returns>
        public bool Yra()
        {
            return d != null;
        }

        /// <summary>
        /// Grąžina pagalbinės rodyklės rodomo elemento reikšmę
        /// </summary>
        /// <returns></returns>
        public Mobilus ImtiDuomenis() { return d.Duomenys; }

        /// <summary>
        /// Sukuriamas sąrašo elementas ir prijungiamas prie sąrašo PRADŽIOS
        /// </summary>
        /// <param name="inf"> naujo elemento reikšmė (duomenys)</param>
        public void DėtiDuomenis(Mobilus duom)
        {
            Mazgas d1 = new Mazgas();
            d1.Duomenys = duom;
            d1.Kitas = pr;
            pr = d1;
        }

        public void Įterpti(Mobilus duom)
        {
            Mazgas d = new Mazgas();
            d.Duomenys = duom;
            d.Kitas = null;

            if (pr == null)
            {
                pr = d; // jei sąrašas tuščias
            }
            else
            {
                if (pr.Duomenys >= duom)
                { // jeigu elementą reikia sukurti sąrašo pradžioje
                    d.Kitas = pr;
                    pr = d;
                }
                else
                { // jeigu elementą reikia įterpti sąraše
                  // randama įterpimo vieta – elementas, už kurio reikia įterpti
                    Mazgas dd = Vieta(duom);
                    d.Kitas = dd.Kitas; // naujas elementas įterpiamas už surasto elemento
                    dd.Kitas = d;
                }
            }
        }

        /// <summary>
        /// Sunaikinamas sąrašas
        /// </summary>
        public void Naikinti()
        {
            //eina per elementus ir juos visus naikina 
            while (pr != null)
            {
                d = pr;
                pr.Duomenys = null;
                pr = pr.Kitas;
                d = null;
            }
            d = pr = null;
        }

        /// <summary>
        /// Rikiavimas išrinkimo būdu. Nuo head'p iki tail'o
        /// </summary>
        public void Rikiuoti()
        {
            for (Mazgas d1 = pr; d1.Kitas != null; d1 = d1.Kitas)
            {
                Mazgas maxv = d1;

                //suranda didziausia likusią reiksme 
                for (Mazgas d2 = d1; d2 != null; d2 = d2.Kitas)
                {
                    if (d2.Duomenys <= maxv.Duomenys)
                    {
                        maxv = d2;
                    }

                }
                //apkeičia vietomis
                Mobilus St = d1.Duomenys;
                d1.Duomenys = maxv.Duomenys;
                maxv.Duomenys = St;
            }
        }

        /// <summary>
        ///  Suranda ir grąžina ilgiausiai veikiančio įrenginio duomenis 
        /// </summary>
        /// <returns>Mobiliojo įrenginio objektas</returns>
        public Mobilus MaxTrukmė()
        {
            Mobilus max;
            max = pr.Duomenys;
            for (Mazgas d1 = pr; d1 != null; d1 = d1.Kitas)
                if (d1.Duomenys > max)
                    max = d1.Duomenys;
            return max;
        }

        /// <summary>
        /// Ieškoma naujo elemento įterpimo vieta. Vieta objektui duom 
        /// ieškoma, naudojant rikiavimui sukurtu operatoriumi
        /// </summary>
        /// <param name="duom">objektas</param>
        /// <returns></returns>
        private Mazgas Vieta(Mobilus duom)
        {
            Mazgas dd = pr;
            while (dd != null && dd.Kitas != null && duom >= dd.Kitas.Duomenys)
                dd = dd.Kitas;
            return dd;
        }
    }
}
