﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mobilieji_irenginiai
{
    class Program
    {
        const string CFd1 = @"../../Mikas.txt";
        const string CFd2 = @"../../Darius.txt";
        const string CFd3 = @"../../Rimas.txt";
        const string CFr = @"../../Rezultatai.txt";
        static void Main(string[] args)
        {
            Studentas A = new Studentas(); // pirmojo studento duomenys
            Studentas B = new Studentas(); // antrojo studento duomenys
            Studentas C = new Studentas();
            A = SkaitytiAtv(CFd1);
            B = SkaitytiAtv(CFd2);

            if (File.Exists(CFr))
                File.Delete(CFr);
            Spausdinti(CFr, A, A.VardasPavarde);
            Spausdinti(CFr, B, B.VardasPavarde);

            using (var failas = new StreamWriter(CFr, true))
            {
                Mobilus max;
                max = A.MaxTrukmė();
                failas.WriteLine("Studentas: {0}, ilgiausiai veikianti" +
                    " baterija \r\n modelis: {1}, tipas: {2}, trukmė: {3}.",
                A.VardasPavarde, max.Modelis, max.Tipas, max.Baterija);
                failas.WriteLine();
                max = B.MaxTrukmė();
                failas.WriteLine("Studentas: {0}, ilgiausiai veikianti" +
                    " baterija \r\n modelis:  {1}, tipas: {2}, trukmė: {3}.",
                B.VardasPavarde, max.Modelis, max.Tipas, max.Baterija);
            }

            // --- Nurodyto tipo įrenginių atrinkimas ---
            Studentas Naujas = new Studentas(); // atrinkti duomenys
            Console.WriteLine("Įveskite norimą įrenginio tipą:");
            string tipas = Console.ReadLine(); // Įvedamas norimas įrenginio tipas
            Atrinkti(A, tipas, Naujas);
            Atrinkti(B, tipas, Naujas);
            // --- Suformuoto sąrašo spausdinimas ir rikiavimas ---
            Naujas.Pradžia();
            if (Naujas.Yra())
            {
                Spausdinti(CFr, Naujas, "Atrinkti nerikiuoti");
                Naujas.Rikiuoti();
                Spausdinti(CFr, Naujas, "Atrinkti surikiuoti");
            }
            else
            {
                using (var failas = new StreamWriter(CFr, true))
                {
                    failas.WriteLine("Naujas sąrašas nesudarytas.");
                }
            }

            //Naujas.Naikinti();
            C = SkaitytiAtv(CFd3);
            Spausdinti(CFr, C, C.VardasPavarde);
            //Trečio studento pridėjimas
            Atrinkti_Į_Rikiuotą(C, tipas, Naujas);
            Naujas.Pradžia();
            if (Naujas.Yra())
            {
                Spausdinti(CFr, Naujas, "Rikiuotas po papildymo");
            }
            else
            {
                using (var failas = new StreamWriter(CFr, true))
                {
                    failas.WriteLine("Naujas sąrašas liko nesudarytas.");
                }
            }
        }

        /// <summary>
        /// Skaitomi duomenys iš failo ir sudedami į sąrašą ATVIRKŠTINE tvarka
        /// </summary>
        /// <param name="fv">duomenų failo vardas</param>
        /// <returns></returns>
        static Studentas SkaitytiAtv(string fv)
        {
            var A = new Studentas();
            using (var failas = new StreamReader(fv))
            {
                string eilute;
                A.VardasPavarde = eilute = failas.ReadLine();
                while ((eilute = failas.ReadLine()) != null)
                {
                    string[] eilDalis = eilute.Split(';');
                    string modelis = eilDalis[0];
                    string tipas = eilDalis[1];
                    int baterija = int.Parse(eilDalis[2]);
                    Mobilus elem = new Mobilus(modelis, tipas, baterija);
                    A.DėtiDuomenis(elem);
                }
            }
            return A;
        }


        /// <summary>
        /// Sąrašo duomenys spausdinami faile
        /// </summary>
        /// <param name="fv"> duomenų failo vardas</param>
        /// <param name="A"> sąrašo objekto nuoroda</param>
        /// <param name="koment">komentaras</param>
        static void Spausdinti(string fv, Studentas A, string koment)
        {
            using (var failas = new StreamWriter(fv, true))
            {
                failas.WriteLine(koment);
                failas.WriteLine("+------------------------------+---------------" +
                "-------+--------------+");
                failas.WriteLine("| Modelis | Tipas " +
                " | Veik. trukmė |");
                failas.WriteLine("+------------------------------+---------------" +
                "-------+--------------+");
                // Sąrašo peržiūra, panaudojant sąsajos metodus
                for (A.Pradžia(); A.Yra(); A.Kitas())
                {
                    failas.WriteLine("{0}", A.ImtiDuomenis().ToString());
                }
                failas.WriteLine("+------------------------------+---------------" +
                "-------+--------------+");
                failas.WriteLine();
            }
        }

        /// <summary>
        /// Iš sąrašo senas kopijuoja objektus į sąrašą naujas
        /// </summary>
        /// <param name="senas">įrenginių sąrašas</param>
        /// <param name="tipas">atrenkamų įrenginių tipas</param>
        /// <param name="naujas">naujo objektų sąrašo adresas</param>
        static void Atrinkti(Studentas senas, string tipas, Studentas naujas)
        {
            for (senas.Pradžia(); senas.Yra(); senas.Kitas())
            {
                Mobilus duom = senas.ImtiDuomenis();
                if (duom.Tipas == tipas)
                    naujas.DėtiDuomenis(duom);
            }
        }

        static void Atrinkti_Į_Rikiuotą(Studentas senas, string tipas, Studentas naujas)
        {
            for (senas.Pradžia(); senas.Yra(); senas.Kitas())
            {
                Mobilus duom = senas.ImtiDuomenis();
                if (duom.Tipas == tipas)
                    naujas.Įterpti(duom);
            }
        }
    }
}