﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace K1pvz
{
    class Program
    {
        const string CDuomenys = @"../../Duomenys.txt";
        const string CRezultatai = @"../../Rezultatai.txt";

        static void Main(string[] args)
        {
            DaugMiestu M = new DaugMiestu();
            DaugMiestu V = new DaugMiestu();
            StreamWriter rasytojas = new StreamWriter(CRezultatai);

            SkaitytiFaila(M, CDuomenys);
            rasytojas.Write("Pradiniai duomenys:");
            rasytojas.Write(M.ToString());
            //V konteinerio formavimas
            Console.Write("Iveskite metus naujam konteineriui suformuoti: ");
            DateTime data = DateTime.ParseExact(Console.ReadLine(), "yyyy",
                CultureInfo.CurrentCulture);
            Formuoti(M, V, data);
            //Valstybe su didžiausiu plotu'
            Console.Write("Iveskite valstybe kurios didžiausią miestą" +
                " norite pamatyti: ");
            string ieskomaValstybe = Console.ReadLine();
            int mietoIndeksas = M.RastiMiesta(ieskomaValstybe);
            rasytojas.Write("\nMiestas su didžiausiu plotu plotu:\n");
            rasytojas.Write(M.Imti(mietoIndeksas).miestas + " " + M.Imti
                (mietoIndeksas).plotas + "\n");
            //Šalinimas
            M.Salinti(mietoIndeksas);
            //Spausdinimas
            rasytojas.Write("\nPrieš rikiavimą:");
            rasytojas.Write(M.ToString());
            //Rikiavimas
            M.Rikiuoti();
            //Spausdinimas
            rasytojas.Write("\nPo rikiavimą:");
            rasytojas.Write(M.ToString());
            //Suformuoto spausdinimas
            rasytojas.Write("\nSuformuotas konteineris:");
            rasytojas.Write(V.ToString());
            rasytojas.Close();
        }

        /// <summary>
        /// Metodas skirtas nuskaityti failą į konteinerį
        /// </summary>
        /// <param name="konteineris">konteineris į kurį failas nuskaitomas
        /// </param>
        /// <param name="failovieta">failo vieta</param>
        static void SkaitytiFaila(DaugMiestu konteineris, string failovieta)
        {
            using (StreamReader skaitytojas = new StreamReader(failovieta,
                Encoding.GetEncoding("utf-8")))
            {
                string eilute;
                while ((eilute = skaitytojas.ReadLine()) != null)
                {
                    string[] dalys = eilute.Split(';');
                    string pavadinimas = dalys[0];
                    string salis = dalys[1];
                    double gyvSkaicius = double.Parse(dalys[2]);
                    double plotas = double.Parse(dalys[3]);
                    DateTime data = DateTime.ParseExact(dalys[4], "yyyy-M-d",
                        CultureInfo.CurrentCulture);
                    Miestas naujas = new Miestas();
                    naujas.Deti(pavadinimas, salis, gyvSkaicius, plotas,
                        data);
                    konteineris.Deti(naujas);
                }
            }
        }

        /// <summary>
        /// Metodas skirtas suformauoti nauja konteineri pagal ivestus
        /// miestus
        /// </summary>
        /// <param name="originalas"></param>
        /// <param name="formuojamas"></param>
        /// <param name="miestai"></param>
        static void Formuoti(DaugMiestu originalas, DaugMiestu formuojamas,
            DateTime metai)
        {
            for (int x = 0; x < originalas.kiekis; x++)
            {
                if (originalas.Imti(x).data.Year == metai.Year)
                    formuojamas.Deti(originalas.Imti(x));
            }
        }


        /// <summary>
        /// Konteinerinė klasė reikalinga mietų informaciją kaupti
        /// </summary>
        class DaugMiestu
        {
            private const int CMax = 20;
            public int kiekis { get; private set; }
            public Miestas[] miestai;

            public DaugMiestu()
            {
                kiekis = 0;
                miestai = new Miestas[CMax];
            }

            /// <summary>
            /// Metodas skirtas įdėti Miesto objektą į konteinerį
            /// </summary>
            /// <param name="naujas">Mieto objektas</param>
            public void Deti(Miestas naujas)
            {
                miestai[kiekis++] = naujas;
            }

            /// <summary>
            /// Metodas skirtas įdėtas 
            /// </summary>
            /// <param name="reikalingas"></param>
            /// <returns></returns>
            public Miestas Imti(int reikalingas)
            {
                return miestai[reikalingas];
            }

            /// <summary>
            /// Metodas linijinis paieškos gražinantis rastą miestą su didžiausiu
            /// plotu pagal nurodytą valstybę
            /// </summary>
            /// <param name="valstybe">ieškoma valstybė</param>
            /// <returns>Rastas miesto indeksas</returns>
            public int RastiMiesta(string valstybe)
            {
                int indeksas = 0;
                double reiksme = double.MinValue;
                for (int x = 0; x < kiekis; x++)
                {
                    if (reiksme < miestai[x].plotas && miestai[x].valstybe ==
                        valstybe)
                    {
                        reiksme = miestai[x].plotas;
                        indeksas = x;
                    }
                }
                return indeksas;
            }

            /// <summary>
            /// Metods skirtas pašalinti pasirinktą konteinerio objektą
            /// </summary>
            /// <param name="salinamas">salinamo objekto indeksas</param>
            public void Salinti(int salinamas)
            {
                miestai[salinamas] = miestai[kiekis - 1];
                kiekis--;
            }

            /// <summary>
            /// Metodas skirtas pažalinti didžiausią plotą turinti miestą 
            /// pagal nurodytą valstybę
            /// </summary>
            /// <param name="valstybe">Valstybė pagal kurią šalinama</param>
            public void SalintiDidziausiaPlota(string valstybe)
            {
                Salinti(RastiMiesta(valstybe));
            }

            /// <summary>
            /// Metodas skirtas surikiuti objektų konteinerį panaudojant
            /// burbuliuko metodą, pagal gyventojų skaičių mažėjimo
            /// tvarka ir miestų pavadinimus didėjimo tvarka
            /// </summary>
            public void Rikiuoti()
            {
                bool keitimas = true;
                while (keitimas)
                {
                    keitimas = false;
                    for (int x = 0; x < kiekis - 1; x++)
                    {
                        if (miestai[x] < miestai[x + 1])
                        {
                            keitimas = true;
                            Miestas laikinas = miestai[x];
                            miestai[x] = miestai[x + 1];
                            miestai[x + 1] = laikinas;
                        }
                    }
                }
            }

            /// <summary>
            /// To string metodo perrašmas, talpinamų objektų parametrų 
            /// spausdinimas lentele
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                string galutinis = "";
                galutinis += "\n|---------------------------" +
                    "----------------------------|\n";
                galutinis += "|     Miestas          Šalis  " +
                    "   G.S     Plotas  Data   |\n";
                galutinis += "|---------------------------" +
                    "----------------------------|\n";
                for (int x = 0; x < kiekis; x++)
                {
                    galutinis += miestai[x].ToString();
                }
                galutinis += "|---------------------------" +
                    "----------------------------|\n";
                return galutinis;
            }
        }

        /// <summary>
        /// Parametrinė klasė skirta kaupti informaciją apie pasirinktą miestą
        /// </summary>
        class Miestas
        {
            public string miestas { get; private set; }
            public string valstybe { get; private set; }
            public double skaicius { get; private set; }
            public double plotas { get; private set; }
            public DateTime data { get; private set; }

            public Miestas()
            {
                miestas = "";
                valstybe = "";
                skaicius = 0;
                plotas = 0;
                data = DateTime.MinValue;
            }

            /// <summary>
            /// Metodas skirtas Mieto objektui priskirti parametrus
            /// </summary>
            /// <param name="miestas"></param>
            /// <param name="valstybe"></param>
            /// <param name="skaicius"></param>
            /// <param name="plotas"></param>
            /// <param name="data"></param>
            public void Deti(string miestas, string valstybe, double
                skaicius,
                double plotas, DateTime data)
            {
                this.miestas = miestas;
                this.valstybe = valstybe;
                this.skaicius = skaicius;
                this.plotas = plotas;
                this.data = data;
            }

            /// <summary>
            /// Metodo ToString perdengimas naudojamas gražiai atvaizduoti
            /// duomenis
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                string galutinis = "";
                galutinis += string.Format("|{0,15}|{1,15}|{2,-7}|{3,-7}|" +
                    "{4,-7}|\n", miestas, valstybe, skaicius, plotas,
                    data.ToString("yyyy"));

                return galutinis;
            }

            /// <summary>
            /// Operatoriaus perdengimas lyginantis gyventojų skaičių 
            /// ir miesto pavadinimus
            /// </summary>
            /// <param name="pirmas"></param>
            /// <param name="antras"></param>
            /// <returns></returns>
            public static bool operator <(Miestas pirmas, Miestas antras)
            {
                bool rezultatas = false;
                int vardu_palyg = string.Compare(pirmas.miestas, antras.miestas);

                if (pirmas.skaicius < antras.skaicius) rezultatas = true;
                else if (pirmas.skaicius == antras.skaicius && vardu_palyg == 1)
                    rezultatas = true;

                return rezultatas;
            }

            /// <summary>
            /// Operatoriaus perdengimas lyginantis gyventojų skaičių 
            /// ir miesto pavadinimus
            /// </summary>
            /// <param name="pirmas"></param>
            /// <param name="antras"></param>
            /// <returns></returns>
            public static bool operator >(Miestas pirmas, Miestas antras)
            {
                bool rezultatas = false;
                int vardu_palyg = string.Compare(pirmas.miestas, antras.miestas);

                if (pirmas.skaicius > antras.skaicius) rezultatas = true;
                else if (pirmas.skaicius == antras.skaicius && vardu_palyg == -1)
                    rezultatas = true;

                return rezultatas;
            }
        }
    }
}
