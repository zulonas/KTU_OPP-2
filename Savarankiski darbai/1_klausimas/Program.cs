using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _1_klausimas
{
    public class Asmuo
    {
        public string pavVard { get; set; } // pavardė, vardas
        public int amžius { get; set; } // amžius
        public TimeSpan laikas { get; set; } // atvykimo laikas
        public Asmuo(string pavVard, int amžius, TimeSpan laikas)
        {
            this.pavVard = pavVard;
            this.amžius = amžius;
            this.laikas = laikas;
        }
        public override string ToString()
        {
            string eilute;
            eilute = string.Format("{0, -17} {1} {2}", pavVard, amžius, laikas);
            return eilute;
        }

        // Užklotas metodas GetHashCode()
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    class Program
    {
        const string CFd1 = "..\\..\\Asmenys.txt"; // asmenų duomenų failo vardas
        static void Main(string[] args)
        {
            // Asmenų sąrašo sudarymas ir spausdinimas
            List<Asmuo> AsmenuList = SkaitytiAsmuoList(CFd1);
            SpausdintiAsmenuList(AsmenuList, "Pradiniai duomenys");
            Queue<Asmuo> Eile = new Queue<Asmuo>();
            TimeSpan atvykimoPradzia = new TimeSpan(7, 0, 0); // tikrinamo laiko intervalo pradžia
            TimeSpan atvykimoPabaiga = new TimeSpan(17, 0, 0); // tikrinamo laiko intervalo pabaiga
            TimeSpan žingsnis = new TimeSpan(0, 5, 0); // laiko intervalo peržiūros žingsnis
            int[] Raktai = { 7, 9, 4, 1, 6, 2, 3 }; // žodyno raktai
                                                    // ATLIKITE: visus nurodytus skaičiavimus

            //-----------------------------------------------------------------
            //Skaitymas
            AsmenuList = SkaitytiAsmuoList(CFd1);
            Atrinkti(AsmenuList, Eile, atvykimoPradzia, atvykimoPabaiga, žingsnis);
            //Atspausdinamas eilės elementų kiekis
            Console.WriteLine($"\r\nSukauptu elementu kiekis: {Eile.Count}");
            //Pirmas eilės elementas
            Asmuo pirmasElementas = null;
            try
            {
                pirmasElementas = Eile.Peek();
                Console.WriteLine($"Pirmas elementas yra: {pirmasElementas}");
            }
            catch (System.InvalidOperationException)
            {
                Console.WriteLine($"Pirmo elemento eilėje nėra");
            }
            if (pirmasElementas != null)
            {
                //Sudaromas rikiuotas žodynas
                SortedDictionary<int, Asmuo> AsmenuZodynas =
                    new SortedDictionary<int, Asmuo>();
                Console.Write("Suformuotas zodynas:\r\n");
                Formuoti(AsmenuList, AsmenuZodynas, pirmasElementas.amžius,
                    Raktai);
                //Spausdinamas zodynas
                Spausdinti(AsmenuZodynas);
                //Randamas vidurkis
                double vidurkis = AsmenuZodynas.Average(x => x.Value.amžius);
                Console.WriteLine($"Bendras amžiaus vidurkis: {vidurkis}");
            }
            else // jeigu pirmo nera
            {
                Console.WriteLine("Negalima formuoti zodyno");
            }

            Console.ReadKey(true);
        }
        // spausdina asmenų duomenų lentelę
        static void SpausdintiAsmenuList(List<Asmuo> AsmuoList, string antraste)
        {
            const string virsus =
            "------------------------------------------------- \r\n"
            + " Nr. Pavardė, vardas Amžius Atvykimo laikas \r\n"
            + "-------------------------------------------------";
            Console.WriteLine("\n " + antraste);
            Console.WriteLine(virsus);
            for (int i = 0; i < AsmuoList.Count; i++)
            {
                Asmuo zmog = AsmuoList[i];
                Console.WriteLine("{0, 3} {1}", i + 1, zmog);
            }
            Console.WriteLine("--------------------------------------------------\n");
        }

        // skaito asmenų duomenų failą
        static List<Asmuo> SkaitytiAsmuoList(string fv)
        {
            // asmenų dinaminis masyvas
            List<Asmuo> AsmuoList = new List<Asmuo>();
            using (StreamReader srautas = new StreamReader(fv,
                Encoding.GetEncoding(1257)))
            {
                string eilute;
                while ((eilute = srautas.ReadLine()) != null)
                {
                    string[] eilDalis = eilute.Split(';');
                    string pav = eilDalis[0];
                    int amžius = int.Parse(eilDalis[1]);
                    TimeSpan laikas = TimeSpan.Parse(eilDalis[2]);
                    Asmuo naujas = new Asmuo(pav, amžius, laikas);
                    AsmuoList.Add(naujas);
                }
            }
            return AsmuoList;
        }

        // spausdina žodyno reikšmes
        // naudoja ENumerator
        public static void Spausdinti(SortedDictionary<int, Asmuo> zodynas)
        {
            var enumerator = zodynas.GetEnumerator();
            while (enumerator.MoveNext())
            {
                object item = enumerator.Current;
                Console.WriteLine(" {0} ", item);
            }
            Console.WriteLine();
        }

        // Formuoja eilę
        static void Atrinkti(List<Asmuo> AsmenuList, Queue<Asmuo> Eile,
        TimeSpan atvykimoPradzia, TimeSpan atvykimoPabaiga,
        TimeSpan žingsnis)
        {
            // ATLIKITE: Dinaminio masyvo asmenys, kurių atvykimo laikas yra duotame
            // intervale [atvykimoPradzia, atvykimoPradzia] ir yra kartotinis duotam
            // žingsniui žingsnis, įrašomi į eilės konteinerį.
            foreach(Asmuo asm in AsmenuList)
            {
                //naudojamas tikrinti ar yra kartinis
                double kartotinisLaikas = asm.laikas.TotalSeconds /
                    žingsnis.TotalSeconds;
                double apvalintas = Math.Floor(kartotinisLaikas);
                if (atvykimoPradzia <= asm.laikas &&  
                    asm.laikas <= atvykimoPabaiga &&
                    kartotinisLaikas == apvalintas)
                {
                    //įrašomas į queue
                    Eile.Enqueue(asm);
                }
            }
        }

        // Formuoja žodyną
        static void Formuoti(List<Asmuo> AsmenuList, SortedDictionary<int, Asmuo> Zodynas,
        int metai, int[] Raktai)
        {
            // ATLIKITE: Dinaminio masyvo asmenys, kurių amžius didesnis už duotą sveiką skaičių
            // metai, surašomi į rikiuotą žodyną. Žodyno raktai – sveiki skaičiai, paeiliui imami
            // iš duoto sveikų skaičių masyvo Raktai, o reikšmės – Asmuo klasės objektai.
            int RaktoNumeris = 0; //naudojamas didinti reiksme
            foreach(Asmuo asm in AsmenuList)
            {
                if(asm.amžius > metai)
                {
                    Zodynas.Add(Raktai[RaktoNumeris++], asm);
                }
            }
        }
    }
}
