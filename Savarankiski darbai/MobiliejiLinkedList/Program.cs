﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MobiliejiLinkedList
{
    class Program
    {
        const string CFd1 = @"../../Mikas.txt";
        const string CFd2 = @"../../Darius.txt";
        const string CFd3 = @"../../Rimas.txt";
        const string CFr = @"../../Rezultatai.txt";
        static void Main(string[] args)
        {
            string[] Vardai = new string[3];
            LinkedList<Mobilus> A = new LinkedList<Mobilus>();
            LinkedList<Mobilus> B = new LinkedList<Mobilus>();
            SkaitytiAtv(CFd1, 0, Vardai, A);
            SkaitytiAtv(CFd2, 1, Vardai, B);
            if (File.Exists(CFr))
                File.Delete(CFr);
            Spausdinti(CFr, A, Vardai[0]);
            Spausdinti(CFr, B, Vardai[1]);
            using (var failas = new StreamWriter(CFr, true))
            {
                Mobilus max;
                max = MaxTrukmė(A);
                failas.WriteLine("Studentas: {0}, ilgiausiai veikianti baterija \r\n" +
                " modelis: {1}, tipas: {2}, trukmė: {3}.",
                Vardai[0], max.modelis, max.tipas, max.baterija);
                failas.WriteLine();
                max = MaxTrukmė(B);
                failas.WriteLine("Studentas: {0}, ilgiausiai veikianti baterija \r\n" +
                " modelis: {1}, tipas: {2}, trukmė: {3}.",
                Vardai[1], max.modelis, max.tipas, max.baterija);
            }

            // --- Nurodyto tipo įrenginių atrinkimas ---
            LinkedList<Mobilus> Naujas = new LinkedList<Mobilus>(); // atrinkti duomenys
            Console.WriteLine("Įveskite norimą įrenginio tipą:");
            string tipas = Console.ReadLine(); // Įvedamas norimas įrenginio tipas
            Atrinkti(A, tipas, Naujas);
            Atrinkti(B, tipas, Naujas);
            // --- Suformuoto sąrašo spausdinimas ir rikiavimas ---
            if (Naujas.Count > 0)
            {
                Spausdinti(CFr, Naujas, "Atrinkti nerikiuoti");
                // -Suformuoto sąrašo rikiavimas
                Naujas = new LinkedList<Mobilus>
                (Naujas.OrderBy(p => p.baterija).ThenBy(p => p.modelis));
                Spausdinti(CFr, Naujas, "Atrinkti surikiuoti");
            }
            else
            {
                using (var failas = new StreamWriter(CFr, true))
                {
                    failas.WriteLine("Naujas sąrašas nesudarytas.");
                }
            }
            LinkedList<Mobilus> C = new LinkedList<Mobilus>(); // trečio studento duomenys
            SkaitytiAtv(CFd3, 2, Vardai, C);
            Spausdinti(CFr, C, Vardai[2]);
            Atrinkti_Į_Rikiuotą(C, tipas, Naujas);
            if (Naujas.Count() > 0)
                Spausdinti(CFr, Naujas, "Rikiuotas po papildymo");
            else
                using (var failas = new StreamWriter(CFr, true))
                {
                    failas.WriteLine("Naujas sąrašas liko nesudarytas.");
                }
        }
        // Skaitomi duomenys iš failo ir sudedami į sąrašą ATVIRKŠČIA tvarka
        // fv – duomenų failo vardas
        // vardo numeris Vardai masyve
        static void SkaitytiAtv(string fv, int indeksas, string[] Vardai,
        LinkedList<Mobilus> A)
        {
            using (var failas = new StreamReader(fv))
            {
                string eilute;
                Vardai[indeksas] = eilute = failas.ReadLine();
                while ((eilute = failas.ReadLine()) != null)
                {
                    string[] eilDalis = eilute.Split(';');
                    string modelis = eilDalis[0];
                    string tipas = eilDalis[1];
                    int baterija = int.Parse(eilDalis[2]);
                    Mobilus elem = new Mobilus(modelis, tipas, baterija);
                    A.AddFirst(elem);
                }
            }
        }
        // Sąrašo duomenys spausdinami faile
        // fv – duomenų failo vardas
        // A - sąrašo objekto nuoroda
        // koment - komentaras
        static void Spausdinti(string fv, LinkedList<Mobilus> A, string koment)
        {
            using (var failas = new StreamWriter(fv, true))
            {
                failas.WriteLine(koment);
                failas.WriteLine("+------------------------------+---------------" +
                "-------+--------------+");
                failas.WriteLine("| Modelis | Tipas " +
                " | Veik. trukmė |");
                failas.WriteLine("+------------------------------+---------------" +
                "-------+--------------+");
                // Sąrašo peržiūra, panaudojant sąsajos metodus
                foreach (Mobilus elem in A)
                {
                    failas.WriteLine("{0}", elem.ToString());
                }
                failas.WriteLine("+------------------------------+---------------" +
                "-------+--------------+");
                failas.WriteLine();
            }
        }
        // Suranda ir grąžina ilgiausiai veikiančio įrenginio duomenis
        static Mobilus MaxTrukmė(LinkedList<Mobilus> A)
        {
            Mobilus max;
            max = A.First();
            foreach (Mobilus elem in A)
                if (elem > max)
                    max = elem;
            return max;
        }
        // Iš sąrašo senas kopijuoja objektus į sąrašą naujas
        // senas įrenginių sąrašas
        // tipas atrenkamų įrenginių tipas
        // naujas naujo objektų sąrašo adresas
        static void Atrinkti(LinkedList<Mobilus> senas, string tipas,
        LinkedList<Mobilus> naujas)
        {
            foreach (Mobilus elem in senas)
            {
                if (elem.tipas == tipas)
                    naujas.AddLast(elem);
            }
        }

         // Iš sąrašo senas kopijuoja objektus į sąrašą naujas
         // senas įrenginių sąrašas
         // tipas atrenkamų įrenginių tipas
         // naujas naujo objektų sąrašo adresas
         static void Atrinkti_Į_Rikiuotą(LinkedList<Mobilus> senas, string tipas,
         LinkedList<Mobilus> naujas)
        {
            foreach (Mobilus elem in senas)
            {
                if (elem.tipas == tipas)
                {
                    Mobilus pagalb = Vieta(naujas, elem);
                    if (pagalb.baterija == -1) naujas.AddFirst(elem);
                    else
                    {
                        LinkedListNode<Mobilus> mazgas = naujas.Find(pagalb);
                        naujas.AddAfter(mazgas, elem);
                    }
                }
            }
        }

        // Ieškoma naujo elemento įterpimo vieta.
        // Vieta objektui elementas ieškoma, naudojantis sukurtu operatoriumi
        // sar – susietas sąrašas
        // elementas – objektas
        static Mobilus Vieta(LinkedList<Mobilus> sar, Mobilus elementas)
        {
            Mobilus rastasElem = new Mobilus();
            rastasElem.baterija = -1;
            foreach (Mobilus elem in sar)
            {
                if (elem <= elementas)
                    rastasElem = elem;
            }
            return rastasElem;
        }
    }
}
