﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobiliejiLinkedList
{
    /// <summary>
    /// Mobiliojo įrenginio duomenų saugojimo klasė
    /// </summary>
    public class Mobilus
    { // mobiliosios elektronikos įrenginys
        public string modelis { get; set; } // modelio pavadinimas
        public string tipas { get; set; } // įrenginio tipas
        public int baterija { get; set; } // baterijos veikimo trukmė


        /// <summary>
        /// konstruktorius
        /// </summary>
        /// <param name="modelis"></param>
        /// <param name="tipas"></param>
        /// <param name="baterija"></param>
        public Mobilus(string modelis = "", string tipas = "", int baterija = 0)
        {
            this.modelis = modelis;
            this.tipas = tipas;
            this.baterija = baterija;
        }

        /// <summary>
        /// Objekto naujos reikšmės
        /// </summary>
        /// <param name="a">modelio pavadinimas</param>
        /// <param name="b">įrenginio tipas</param>
        /// <param name="c">baterijos veikimo trukmė</param>
        void Dėti(string a, string b, int c)
        {
            modelis = a;
            tipas = b;
            baterija = c;
        }

        public override string ToString()
        {
            string eilute;
            eilute = string.Format("|{0, -30}| {1, -20} | {2, 8:f} |",
            modelis, tipas, baterija);
            return eilute;
        }

        public override bool Equals(object objektas)
        {
            Mobilus telef = objektas as Mobilus;
            return telef.tipas == tipas && telef.modelis == modelis && telef.baterija ==
            baterija;
        }

        /// <summary>
        /// Užklotas metodas GetHashCode()
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
 
        /// <summary>
        /// Užklotas operatorius >= (dviejų įrenginių palyginimui pagal baterijos veikimo trukmę ir modelio pavadinimą)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator >=(Mobilus pirmas, Mobilus antras)
        {
            int poz = String.Compare(pirmas.modelis, antras.modelis,
            StringComparison.CurrentCulture);
            return pirmas.baterija > antras.baterija || pirmas.baterija == antras.baterija &&
            poz > 0;
        }

        /// <summary>
        /// Užklotas operatorius <= (dviejų įrenginių palyginimui pagal baterijos veikimo trukmę ir modelio pavadinimą)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator <=(Mobilus pirmas, Mobilus antras)
        {
            int poz = String.Compare(pirmas.modelis, antras.modelis,
            StringComparison.CurrentCulture);
            return pirmas.baterija < antras.baterija || pirmas.baterija == antras.baterija &&
            poz < 0;
        }

        /// <summary>
        /// Užklotas operatorius == (įrenginių tipui palyginti)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator ==(Mobilus pirmas, Mobilus antras)
        {
            return pirmas.tipas == antras.tipas;
        }

        /// <summary>
        /// Užklotas operatorius != (įrenginių tipui palyginti)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator !=(Mobilus pirmas, Mobilus antras)
        {
            return pirmas.tipas != antras.tipas;
        }

        /// <summary>
        /// Užklotas operatorius > (dviejų įrenginių palyginimui pagal baterijos veikimo trukmę)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator >(Mobilus pirmas, Mobilus antras)
        {
            return pirmas.baterija > antras.baterija;
        }

        /// <summary>
        /// Užklotas operatorius < (dviejų įrenginių palyginimui pagal baterijos veikimo trukmę)
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator <(Mobilus pirmas, Mobilus antras)
        {
            return pirmas.baterija < antras.baterija;
        }
    }
}
