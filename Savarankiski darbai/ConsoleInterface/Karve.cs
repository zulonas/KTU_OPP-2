﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleInterface
{
    class Karve : IComparable<Karve>, IEquatable<Karve>
    //Icomparable leidžia palyginti kelis objektus
    //ir palyginus sukiuoti t.y. naudoti Sort metodą 
    // <Karve> išraiška leidžia palyginti (naudoti 
    // CopareTo metodą su Karve) kalsės objektais
    //-----------------------------------------
    //IEquatable leidžia palyginti objektus ir jų reikšmes
    {
        public string Vardas { get; private set; }
        public double Svoris { get; private set; }

        public Karve()
        {

        }

        public Karve(string Vardas, double Svoris)
        {
            this.Vardas = Vardas;
            this.Svoris = Svoris;
        }

        //Būtinas metodas IComparable interface'ui
        public int CompareTo(Karve obj)
        {
            return this.Svoris.CompareTo(obj.Svoris);
        }

        public override string ToString()
        {
            return ($"{Vardas} {Svoris}\n");
        }

        //mūsų sukurtas palyginimo metodas naudojamas
        //IEquatable sąsajai. Palygina objektų vardus.
        public bool Equals(Karve nauja)
        {
            return this.Vardas.Equals(nauja.Vardas);
        }

        //perrašo metodą kurį paveldi iš object klasės 
        //pakeičia mūsų sukurtu Equals(Karve other)
        //metodu. Naudojamas IEquatable sąsajai
        public override bool Equals(object obj)
        {
            Karve temp = (Karve)obj;
            return Equals(temp);
        }

        //metodas reikomenduojamas naudojant IEquatable
        //interface'ą 
        public override int GetHashCode()
        {
            return Vardas.GetHashCode() ^ Svoris.GetHashCode();
        }
    }
}
