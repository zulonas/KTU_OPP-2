﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleInterface
{
    class Karves : IEnumerable
    //IEnumerable leidžia sukti foreach ciklą 
    //tai yra system collections sąsaja (sena)
    //nadinga jeigu apsirašome naudojame ne list'us 
    //bet array'us
    {
        private int CMKiekis = 10;
        public int Kiekis { get; private set; }
        Karve[] karves;


        public Karves()
        {
            karves = new Karve[CMKiekis];
        }

        public void Deti(Karve nauja)
        {
            karves[Kiekis++] = nauja;
        }

        //Būtinas metodas IEnumerable interface'ui
        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < Kiekis; i++)
            {
                yield return karves[i];
            }
            //return karves.GetEnumerator();
        }

        //Masyvo rikiavimas panaudojant IComparable 
        //interface'ą
        public void Rikiuoti()
        {
            Array.Sort(karves);
        }

        //panaudojant IEquatable sąsają tikrina 
        //ar turimame masyve yra tikrinamas 
        //objektas
        public bool ArYra(Karve naujas)
        {
            return karves.Contains(naujas);
        }

        public void Palyginti(Karve nauja)
        {
            foreach(Karve karv in karves)
            {
                Console.Write(karv);
            }
        }
    }
}
