﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            Karves Kkarves = new Karves();

            Kkarves.Deti(new Karve("marge", 500));
            Kkarves.Deti(new Karve("juodmarge", 700));
            Kkarves.Deti(new Karve("sniege", 400));
            Kkarves.Deti(new Karve("lieme", 600));
            Kkarves.Deti(new Karve("dobile", 500));

            //foreach galimas dėl naudojamo IEnumerable
            //interface'o / sąsajos
            foreach (Karve karv in Kkarves)
            {
                Console.Write(karv);
            }
            Console.WriteLine();

            //Rikiavimas vykdomas panaudojant IComparable
            //sąsają kuri leidžia rikiuoti naudojant metodą
            //sort
            //Kkarves.Rikiuoti();

            //foreach galimas dėl naudojamo IEnumerable
            //interface'o / sąsajos
            foreach (Karve karv in Kkarves)
            {
                Console.Write(karv);
            }
            Console.WriteLine();

            //panaudojant IEquatable sąsają tikrina 
            //ar turimame masyve yra tikrinamas 
            //objektas
            Console.Write(Kkarves.ArYra(
                new Karve("marge", 500)
            ));
            Console.WriteLine();

            //Panaudojant IEquatable sąsajos metodą
            //Equals tikrinama ar sutampa reikšmės
            //naujo objekto ir turimo masyvo
            //Kkarves.Palyginti(
            //    new Karve("dobile", 500)
            //);
        }
    }
}
