﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    abstract class Klubas
    {
        public string KluboPavadinimas { get; private set; }

        public Klubas()
        {

        }

        public Klubas(string Pavadinimas)
        {
            this.KluboPavadinimas = Pavadinimas;
        }

        public abstract double KomandosPozicija();

    }
}
