﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    /// <summary>
    /// Zaidėjo klasė, kurioje saugomi žaidėjo duomenys
    /// </summary>
    class Zaidejas : Klubas, IComparable<Zaidejas>,IEquatable<Zaidejas>
    {
        public string Vardas { get; private set; }
        public string Pavarde { get; private set; }
        public DateTime GimimoData { get; private set; }
        public Double Ugis { get; private set; }
        public string Pozicija { get; private set; }
        public bool Kapitonas { get; private set; }

        public Zaidejas():base()
        {
            
        }

        public Zaidejas(string Vardas, string Pavarde, DateTime GimData, 
            double Ugis, string Pozicija, string Klubas, bool ArKapitonas)
            :base(Klubas)
        {
            this.Vardas = Vardas;
            this.Pavarde = Pavarde;
            this.GimimoData = GimData;
            this.Ugis = Ugis;
            this.Pozicija = Pozicija;
            this.Kapitonas = ArKapitonas; 
        }

        public override double KomandosPozicija()
        {
            return 0;
        }

        /// <summary>
        /// Perrašomas to string metodas
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format(" {0, -10} {1, -10} {2, 10} {3, 5}" +
                " {4, -5} {5, -7} {6, -4}", Vardas, Pavarde, 
                GimimoData.ToString("yyyy-MM-dd"), Ugis, Pozicija,
                KluboPavadinimas, (Kapitonas)?"Taip":"Ne");
        }
        /// <summary>
        /// Metodas leidžiantis naudoti Sord metoda įgyvendintas
        /// panaudojant IComparable sąsaja. Rikiuoja nuo jaunausio 
        /// iki seniausio, ir pagal ugį nuo aukščiausio iki žemiausio
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Zaidejas other)
        {
            int poz = DateTime.Compare(this.GimimoData, other.GimimoData);
            if (poz > 0)
                return -1;
            else if (poz < 0)
                return 1;
            else
            {
                if (this.Ugis > other.Ugis) return -1;
                else if (this.Ugis < other.Ugis) return 1;
                else return 0;
            }
        }

        /// <summary>
        /// Panaudojama IEquatable sąsaja vardams palyginti
        /// </summary>
        /// <param name="naujas"></param>
        /// <returns></returns>
        public bool Equals(Zaidejas naujas)
        {
            return this.Vardas.Equals(naujas.Vardas);
        }

    }
}
