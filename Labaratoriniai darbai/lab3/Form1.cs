﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace lab3
{
    public partial class Form1 : Form
    {
        OpenFileDialog FailoPaieska = new OpenFileDialog();
        private List<Zaidejas> Zaidejai = new List<Zaidejas>();
        private List<String> Klubai = new List<string>();
        private List<Zaidejas> SudarytasKlubas =
            new List<Zaidejas>();
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Įvesties failo paieška paspaudus 
        /// pasirinkti failą mygtuką
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {
            if (FailoPaieska.ShowDialog() == DialogResult.OK)
            {
                label1.Text = FailoPaieska.SafeFileName;
                button2.Enabled = true;
                button1.Enabled = false;
            }
        }

        /// <summary>
        /// Metodas skirtas išsaugoti failus į vykdomą programą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (FailoPaieska.FileName != "")
                    SkaitytiFaila(Zaidejai, FailoPaieska.FileName);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                MessageBox.Show("Nepavyko nuskaityti visų failo duomenų",
                    "Klaida");
                return;
            }
            finally
            {
                button2.Enabled = false;
                button1.Enabled = true;
                label1.Text = "failas nepasirinktas";
            }
            toolStripStatusLabel1.Text = "Failas nuskaitytas sėkmingai";
            //Įjungiami mygtukai
            button3.Enabled = true;
            button4.Enabled = true;
            išsaugotiKaipToolStripMenuItem.Enabled = true;
            rastiAukščiausiusToolStripMenuItem.Enabled = true;
            rastiAukščiausiusToolStripMenuItem.Enabled = true;
            klubuSuKandidataisSąrašasToolStripMenuItem.Enabled = true;
            spausdintiDuomenisToolStripMenuItem.Enabled = true;
            FailoPaieska.Reset();
        }

        /// <summary>
        /// Metodas skirtas nuskaityti failą ir sudėti jo reikšmes į 
        /// zaidėjų sąrašą
        /// </summary>
        /// <param name="Zaidejai"></param>
        /// <param name="failoDirektorija"></param>
        private void SkaitytiFaila(List<Zaidejas> Zaidejai,
            string failoDirektorija)
        {
            using (StreamReader srautas = new StreamReader(failoDirektorija,
                Encoding.GetEncoding("utf-8")))
            {
                string eilute;
                while ((eilute = srautas.ReadLine()) != null)
                {
                    Zaidejai.Add(GautiZaideja(eilute));
                }
            }
            AtnaujintiKlubuSarasa();
        }

        /// <summary>
        /// Metodas nuskaitantis eilutę ir paverčiantis ją į objektą
        /// </summary>
        /// <param name="eilute"></param>
        /// <returns></returns>
        private Zaidejas GautiZaideja(string eilute)
        {
            string[] eilDalys = eilute.Split(' ');
            string vardas = eilDalys[0];
            string pavarde = eilDalys[1];
            DateTime gimimoData = DateTime.ParseExact(eilDalys[2], "yyyy-MM-dd",
                System.Globalization.CultureInfo.CurrentCulture);
            double ugis = GetDouble(eilDalys[3]);
            string pozicija = eilDalys[4];
            string klubas = eilDalys[5];
            PridetiKluba(klubas);
            bool arKapitonas = (eilDalys[6] == "true") ? true : false;

            return new Zaidejas(vardas, pavarde, gimimoData, ugis, pozicija,
                klubas, arKapitonas);
        }

        /// <summary>
        /// Metodas skirtas pridėti klubus prie sąrašo
        /// </summary>
        /// <param name="klubopav"></param>
        private void PridetiKluba(string klubopav)
        {
            if (!Klubai.Contains(klubopav))
                Klubai.Add(klubopav);
        }

        /// <summary>
        /// Metodas skirtas paimti double reiksme bet kokiu formatu
        /// </summary>
        /// <param name="value">bandoma parse'inti double reikšmė</param>
        /// <returns>reikšmė kuria kompiuteris normaliai supranta</returns>
        private double GetDouble(string reiksme)
        {
            double rezutatas;

            reiksme = reiksme.Replace(',', '.');
            if (!double.TryParse(reiksme, System.Globalization.NumberStyles.Any,
                System.Globalization.CultureInfo.GetCultureInfo("en-US"),
                out rezutatas))
            {
                rezutatas = -1;
                MessageBox.Show("Nepavyko nuskaityti double reikšmių");
            }

            return rezutatas;
        }

        /// <summary>
        /// Metodas skirtas spausdinti duomenis paspaudus mygtuką 
        /// spausdinti duomenis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpausdintiDuomenisToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            richTextBox1.Text += FormuotiTeksta("Pradiniai duomenys", Zaidejai);
        }

        /// <summary>
        /// Metodas skirtas suformuoti tekstą skirtą spausdinti
        /// </summary>
        /// <param name="pavadinimas"></param>
        /// <param name="Zaidejai"></param>
        /// <returns></returns>
        private string FormuotiTeksta(string pavadinimas,
            List<Zaidejas> Zaidejai)
        {
            string galutinis = "";
            galutinis += $"\n\n\t\t\t\t{pavadinimas}\n";
            galutinis += "Vardas     Pavardė      G.Data    Ūgis  Poz. Klubas" +
                "   Kapit.\n";
            galutinis += "---------------------------------------------------" +
                "---------\n";
            Zaidejai.ForEach(zaidejas =>
                galutinis += (zaidejas.ToString()) + "\n");
            if (Zaidejai.Count == 0)
                galutinis += "\t\t\t\tSąrašas tuščias";
            galutinis += "---------------------------------------------------" +
                "---------\n";

            return galutinis;
        }

        /// <summary>
        /// Metodas skirtas išvalyti visus duomenis sukauptus programoje
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IštrintiVisusDuomenisToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            Zaidejai.Clear();
            SudarytasKlubas.Clear();
            richTextBox1.Text = "";
        }

        /// <summary>
        /// Metodas skirtas uždaryti programą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UždarytiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Metodas surandantis 2 jauniausius pasirinktos pozicijos žaidėjus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button3_Click(object sender, EventArgs e)
        {
            string pasirinkTekst = comboBox1.GetItemText(comboBox1.SelectedItem);
            if (!string.IsNullOrEmpty(pasirinkTekst))
            {
                List<Zaidejas> nauji = Zaidejai
                    .Where(x => x.Pozicija == pasirinkTekst).
                    OrderByDescending(x => x.GimimoData).
                    ToList();

                richTextBox1.Text += $"\n\t\tJaunausi krepsininakai" +
                    $" {pasirinkTekst} pozicijoje yra:\n\t\t\t";
                if (nauji.Count >= 1)
                {
                    richTextBox1.Text += nauji[0].Vardas + " " +
                        nauji[0].Pavarde + " ";

                    if (nauji.Count >= 2)
                        richTextBox1.Text += nauji[1].Vardas + " " +
                            nauji[1].Pavarde;
                }
                else
                    MessageBox.Show("Tokių krepšininkų nerasta");
            }
            else
            {
                MessageBox.Show("\nTokių krepšininkų nerasta\n");
            }
        }

        /// <summary>
        /// Metodas surandandantis ir atspaudinantis kiek yra 
        /// aukščiausių zaidėjų 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RastiAukščiausiusToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            int kiekis = Zaidejai.Count(a => a.Ugis >= 200);
            richTextBox1.Text += ($"\n\n Žaidėjų kurių ūgis yra virš" +
                $" 200 yra: {kiekis}");
        }

        /// <summary>
        /// Metodas skirtas atnaujinti klubų sąrašą
        /// </summary>
        private void AtnaujintiKlubuSarasa()
        {
            comboBox2.DataSource = Klubai;
        }

        /// <summary>
        /// Metodas skirtas sudaryti naują atskirą naujos komandos sąrašą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button4_Click(object sender, EventArgs e)
        {
            string pasirinkTekst = comboBox2.GetItemText
                (comboBox2.SelectedItem);
            if (!string.IsNullOrEmpty(pasirinkTekst))
            {
                List<Zaidejas> kluboZaidejai = Zaidejai.Where(a =>
                a.KluboPavadinimas == pasirinkTekst).ToList();
                foreach (Zaidejas zaid in kluboZaidejai)
                {
                    SudarytasKlubas.Add(GilusKopijavimas(zaid));
                }
                spausdintiSudarytąKomandąToolStripMenuItem.Enabled = true;
                rikiavimasToolStripMenuItem.Enabled = true;
                button4.Enabled = false;
            }
            else
            {
                MessageBox.Show("\nKlubų sąrašo sudaryti" +
                    "nepavyko\n");
            }
        }

        /// <summary>
        /// Metodas skirtas sukurti naują objektą atliekant 
        /// „gilųjį“ kopijavimą
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>naujas sukurtas objektas nepriklausomas
        /// nuo jo sąrašo </returns>
        private Zaidejas GilusKopijavimas(Zaidejas obj)
        {
            return new Zaidejas(obj.Vardas,
                obj.Pavarde, obj.GimimoData,
                obj.Ugis, obj.Pozicija,
                obj.KluboPavadinimas, obj.Kapitonas);
        }

        /// <summary>
        /// Metodas skirtas atspausdinti sudarytos komandos
        /// duomenis ekrane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpausdintiSudarytąKomandąToolStripMenuItem_Click(object
            sender, EventArgs e)
        {
            string komandosPav = ""; //komandos pavadinimas
            if (SudarytasKlubas.Count > 0)
            {
                komandosPav = SudarytasKlubas[0].KluboPavadinimas;
                richTextBox1.Text += FormuotiTeksta(komandosPav,
                SudarytasKlubas);
            }
            else
            {
                MessageBox.Show("Nauja komanda tuščia arba" +
                    "nesudaryta");
            }
        }

        /// <summary>
        /// Metodas skirtas surikiuoti suformuotą sąrašą pagal
        /// amžiu didėjančia tvarka ir pagal aukštį mažėjančia tvarka
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RikiavimasToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            SudarytasKlubas.Sort();
        }

        /// <summary>
        /// Metodas skirtas išvesti visų klubų pavadinimus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KlubuSuKandidataisSąrašasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text += "\n\t\t\tKlubai pretenduojantys" +
                "į rinktinę:\n";
            List<string> naujasSarasas = new List<string>();
            foreach (Zaidejas zaid in Zaidejai)
            {
                if (!naujasSarasas.Contains(zaid.KluboPavadinimas))
                    naujasSarasas.Add(zaid.KluboPavadinimas);
            }
            naujasSarasas.ForEach(a => richTextBox1.Text += a + " ");
            richTextBox1.Text += "\n";
        }

        /// <summary>
        /// Metodas skirtas išsaugoti duomenų failą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IšsaugotiKaipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog rasytojas = new SaveFileDialog();
            rasytojas.Filter = "Teksto failas|*.txt";
            rasytojas.FileName = "rezultatai";
            rasytojas.Title = "Išsaugoti teksto failą";
            if (rasytojas.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string kelias = rasytojas.FileName;
                using (StreamWriter teksRasytojas = new
                    StreamWriter(File.Create(kelias)))
                {
                    teksRasytojas.Write(richTextBox1.Text);
                }
            }
        }

        /// <summary>
        /// Metodas atspausdinantis inforamaciją apie autorių
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void autoriusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text += "\n";
            richTextBox1.Text += SkaitytiFaila(@"../../duomenys/autorius.txt");
            richTextBox1.Text += "\n";
        }

        /// <summary>
        /// Metodas skirtas nuskaitytį pilnai failą 
        /// </summary>
        /// <param name="failoKelias">failo kelias</param>
        /// <returns>failo turimi duomenys</returns>
        private string SkaitytiFaila(string failoKelias)
        {
            string galutinis = "";
            galutinis += File.ReadAllText(failoKelias);
            return galutinis;
        }

        /// <summary>
        /// Metodas atspausdinantis pagalbinę informaciją
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void programaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Text += "\n";
            richTextBox1.Text += SkaitytiFaila(@"../../duomenys/pagalba.txt");
            richTextBox1.Text += "\n";

        }

        private void Link_Clicked(object sender, System.Windows.Forms.LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }
    }
}
