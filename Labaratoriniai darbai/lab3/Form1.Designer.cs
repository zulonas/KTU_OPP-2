﻿namespace lab3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.failasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.išsaugotiKaipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ištrintiVisusDuomenisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uždarytiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spausdinimasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spausdintiDuomenisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spausdintiSudarytąKomandąToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.algoritmaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rastiAukščiausiusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rikiavimasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.klubuSuKandidataisSąrašasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pagalbaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoriusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.klubasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.klubasBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.failasToolStripMenuItem,
            this.spausdinimasToolStripMenuItem,
            this.algoritmaiToolStripMenuItem,
            this.pagalbaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(885, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // failasToolStripMenuItem
            // 
            this.failasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.išsaugotiKaipToolStripMenuItem,
            this.ištrintiVisusDuomenisToolStripMenuItem,
            this.uždarytiToolStripMenuItem});
            this.failasToolStripMenuItem.Name = "failasToolStripMenuItem";
            this.failasToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.failasToolStripMenuItem.Text = "Failas";
            // 
            // išsaugotiKaipToolStripMenuItem
            // 
            this.išsaugotiKaipToolStripMenuItem.Enabled = false;
            this.išsaugotiKaipToolStripMenuItem.Name = "išsaugotiKaipToolStripMenuItem";
            this.išsaugotiKaipToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.išsaugotiKaipToolStripMenuItem.Text = "Išsaugoti kaip";
            this.išsaugotiKaipToolStripMenuItem.Click += new System.EventHandler(this.IšsaugotiKaipToolStripMenuItem_Click);
            // 
            // ištrintiVisusDuomenisToolStripMenuItem
            // 
            this.ištrintiVisusDuomenisToolStripMenuItem.Name = "ištrintiVisusDuomenisToolStripMenuItem";
            this.ištrintiVisusDuomenisToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ištrintiVisusDuomenisToolStripMenuItem.Text = "Ištrinti visus duomenis";
            this.ištrintiVisusDuomenisToolStripMenuItem.Click += new System.EventHandler(this.IštrintiVisusDuomenisToolStripMenuItem_Click);
            // 
            // uždarytiToolStripMenuItem
            // 
            this.uždarytiToolStripMenuItem.Name = "uždarytiToolStripMenuItem";
            this.uždarytiToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.uždarytiToolStripMenuItem.Text = "Uždaryti";
            this.uždarytiToolStripMenuItem.Click += new System.EventHandler(this.UždarytiToolStripMenuItem_Click);
            // 
            // spausdinimasToolStripMenuItem
            // 
            this.spausdinimasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spausdintiDuomenisToolStripMenuItem,
            this.spausdintiSudarytąKomandąToolStripMenuItem});
            this.spausdinimasToolStripMenuItem.Name = "spausdinimasToolStripMenuItem";
            this.spausdinimasToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.spausdinimasToolStripMenuItem.Text = "Spausdinimas";
            // 
            // spausdintiDuomenisToolStripMenuItem
            // 
            this.spausdintiDuomenisToolStripMenuItem.Enabled = false;
            this.spausdintiDuomenisToolStripMenuItem.Name = "spausdintiDuomenisToolStripMenuItem";
            this.spausdintiDuomenisToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.spausdintiDuomenisToolStripMenuItem.Text = "Spausdinti pradinius duomenis";
            this.spausdintiDuomenisToolStripMenuItem.Click += new System.EventHandler(this.SpausdintiDuomenisToolStripMenuItem_Click);
            // 
            // spausdintiSudarytąKomandąToolStripMenuItem
            // 
            this.spausdintiSudarytąKomandąToolStripMenuItem.Enabled = false;
            this.spausdintiSudarytąKomandąToolStripMenuItem.Name = "spausdintiSudarytąKomandąToolStripMenuItem";
            this.spausdintiSudarytąKomandąToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.spausdintiSudarytąKomandąToolStripMenuItem.Text = "Spausdinti sudarytą komandą";
            this.spausdintiSudarytąKomandąToolStripMenuItem.Click += new System.EventHandler(this.SpausdintiSudarytąKomandąToolStripMenuItem_Click);
            // 
            // algoritmaiToolStripMenuItem
            // 
            this.algoritmaiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rastiAukščiausiusToolStripMenuItem,
            this.rikiavimasToolStripMenuItem,
            this.klubuSuKandidataisSąrašasToolStripMenuItem});
            this.algoritmaiToolStripMenuItem.Name = "algoritmaiToolStripMenuItem";
            this.algoritmaiToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.algoritmaiToolStripMenuItem.Text = "Algoritmai";
            // 
            // rastiAukščiausiusToolStripMenuItem
            // 
            this.rastiAukščiausiusToolStripMenuItem.Enabled = false;
            this.rastiAukščiausiusToolStripMenuItem.Name = "rastiAukščiausiusToolStripMenuItem";
            this.rastiAukščiausiusToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.rastiAukščiausiusToolStripMenuItem.Text = "Rasti aukščiausius";
            this.rastiAukščiausiusToolStripMenuItem.Click += new System.EventHandler(this.RastiAukščiausiusToolStripMenuItem_Click);
            // 
            // rikiavimasToolStripMenuItem
            // 
            this.rikiavimasToolStripMenuItem.Enabled = false;
            this.rikiavimasToolStripMenuItem.Name = "rikiavimasToolStripMenuItem";
            this.rikiavimasToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.rikiavimasToolStripMenuItem.Text = "Rikiavimas";
            this.rikiavimasToolStripMenuItem.Click += new System.EventHandler(this.RikiavimasToolStripMenuItem_Click);
            // 
            // klubuSuKandidataisSąrašasToolStripMenuItem
            // 
            this.klubuSuKandidataisSąrašasToolStripMenuItem.Enabled = false;
            this.klubuSuKandidataisSąrašasToolStripMenuItem.Name = "klubuSuKandidataisSąrašasToolStripMenuItem";
            this.klubuSuKandidataisSąrašasToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.klubuSuKandidataisSąrašasToolStripMenuItem.Text = "Klubų su kandidatais sąrašas";
            this.klubuSuKandidataisSąrašasToolStripMenuItem.Click += new System.EventHandler(this.KlubuSuKandidataisSąrašasToolStripMenuItem_Click);
            // 
            // pagalbaToolStripMenuItem
            // 
            this.pagalbaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programaToolStripMenuItem,
            this.autoriusToolStripMenuItem});
            this.pagalbaToolStripMenuItem.Name = "pagalbaToolStripMenuItem";
            this.pagalbaToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.pagalbaToolStripMenuItem.Text = "Pagalba";
            // 
            // programaToolStripMenuItem
            // 
            this.programaToolStripMenuItem.Name = "programaToolStripMenuItem";
            this.programaToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.programaToolStripMenuItem.Text = "Pagalba";
            this.programaToolStripMenuItem.Click += new System.EventHandler(this.programaToolStripMenuItem_Click);
            // 
            // autoriusToolStripMenuItem
            // 
            this.autoriusToolStripMenuItem.Name = "autoriusToolStripMenuItem";
            this.autoriusToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.autoriusToolStripMenuItem.Text = "Autorius";
            this.autoriusToolStripMenuItem.Click += new System.EventHandler(this.autoriusToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(885, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "none";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Courier New", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 27);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(588, 397);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            this.richTextBox1.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.Link_Clicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(606, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(267, 94);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Duomenų įkėlimas";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(16, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "failas nepasirinktas";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(142, 19);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(119, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Įkelti";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Pasirinkti failą";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Location = new System.Drawing.Point(606, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(267, 84);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Jaunausių radimas";
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(186, 37);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Rasti";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "PG",
            "SG",
            "SF",
            "PF",
            "C"});
            this.comboBox1.Location = new System.Drawing.Point(19, 39);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(161, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.comboBox2);
            this.groupBox3.Location = new System.Drawing.Point(606, 219);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(267, 88);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Klubo kandidatų sąrašo sudarymas";
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(187, 41);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 1;
            this.button4.Text = "Sudaryti";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(19, 41);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(161, 21);
            this.comboBox2.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 450);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.klubasBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolStripMenuItem failasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem išsaugotiKaipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ištrintiVisusDuomenisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uždarytiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spausdinimasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem algoritmaiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rastiAukščiausiusToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem rikiavimasToolStripMenuItem;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ToolStripMenuItem klubuSuKandidataisSąrašasToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem spausdintiDuomenisToolStripMenuItem;
        private System.Windows.Forms.BindingSource klubasBindingSource;
        private System.Windows.Forms.ToolStripMenuItem spausdintiSudarytąKomandąToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pagalbaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem programaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoriusToolStripMenuItem;
    }
}

