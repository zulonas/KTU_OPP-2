﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace lab5
{
    public partial class Form1 : Form
    {

        private Sarasas<Komanda> Komandos;
        private Sarasas<Krepsininkas> Krepsininkai;
        OpenFileDialog failoPaieska = new OpenFileDialog();

        public Form1()
        {
            InitializeComponent();
            toolStripStatusLabel1.Text = "";
            įvestiŽaidėjusToolStripMenuItem.Enabled = false;
            spausdintiKomandasToolStripMenuItem.Enabled = false;
            spausdintiKrepšininkusToolStripMenuItem.Enabled = false;
            spausdintiGeriausiasPozicijasToolStripMenuItem.Enabled = false;
            rastiDaugiaiPataikiusiąToolStripMenuItem.Enabled = false;
        }

        private void UždarytiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Metodas aktyvuojamas paspaudus mygtuką skirtas nuskaityti
        /// ir įrašydi duomenis į programą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IvestiKomandasToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            if (failoPaieska.ShowDialog() == DialogResult.OK)
            {
                Komandos = FormuotiKomandas(failoPaieska.FileName);
                toolStripStatusLabel1.Text = "Komandų failas nuskaitytas" +
                    " sėkmingai";
                ivestiKomandasToolStripMenuItem.Enabled = false;
                įvestiŽaidėjusToolStripMenuItem.Enabled = true;
                spausdintiKomandasToolStripMenuItem.Enabled = true;
            }
            else
            {
                toolStripStatusLabel1.Text = "Nepavyko nuskaityti failo";
            }
        }

        /// <summary>
        /// Skaitoma failas ir formuojama komandų sąrašas
        /// </summary>
        /// <param name="failoKelias"></param>
        /// <returns></returns>
        private Sarasas<Komanda> FormuotiKomandas(string failoKelias)
        {
            Sarasas<Komanda> formuojamas = new Sarasas<Komanda>();
            using(StreamReader skaitytojas = new StreamReader(failoKelias))
            {
                string eilute;
                while((eilute = skaitytojas.ReadLine()) != null)
                {
                    Komanda naujas = FormuotiKomanda(eilute);
                    formuojamas.PridetiPabaigoje(naujas);
                }
            }
            return formuojamas;
        }

        /// <summary>
        /// Suformuojama komanda pagal eilutėje turimus komandos duomenis
        /// </summary>
        /// <param name="eilute"></param>
        /// <returns></returns>
        private Komanda FormuotiKomanda(string eilute)
        {
            string[] dalys = eilute.Split(';');
            string pavadinimas = dalys[0];
            int viso = int.Parse(dalys[1]);
            int laimeta = int.Parse(dalys[2]);
            return new Komanda(pavadinimas, viso, laimeta);
        }

        /// <summary>
        /// Metodas skirtas suformuoti sąrašą krepšininkų
        /// </summary>
        /// <param name="failoKelias">krepsininkų duomenų failas</param>
        /// <returns></returns>
        private Sarasas<Krepsininkas> FormuotiKrepsininkus(string failoKelias)
        {
            Sarasas <Krepsininkas> naujas = new Sarasas<Krepsininkas>();

            //Pridedami Krepsininkai
            using(StreamReader skaitytojas = new StreamReader(failoKelias))
            {
                string eilute;
                while((eilute = skaitytojas.ReadLine()) != null)
                {
                    naujas.PridetiPabaigoje(
                        FormuotiKrepsininka(eilute)
                    );
                }
            }
            return naujas;
        }

        /// <summary>
        /// Metodas skirtas suformuoti krepsininko objektą turint jo duomenis
        /// </summary>
        /// <param name="dalys">string[] masyvas</param>
        /// <returns>suformuotas krepsininkas</returns>
        private Krepsininkas FormuotiKrepsininka(string eilute)
        {
            string[] dalys = eilute.Split(';');
            string komanda = dalys[0];
            string vardas = dalys[1];
            string pavarde = dalys[2];
            DateTime gimimoData = DateTime.Parse(dalys[3]);
            double ugis = GetDouble(dalys[4]);
            Pozicija pozicija = (Pozicija)Enum.Parse(typeof(Pozicija),
                       dalys[5].Trim());
            int pataikytiTaskai = int.Parse(dalys[6]);
            int nePataikytiTaskai = int.Parse(dalys[7]);

            return new Krepsininkas(komanda, vardas, pavarde, gimimoData, ugis,
                pozicija, pataikytiTaskai, nePataikytiTaskai);
        }


        /// <summary>
        /// Metodas skirtas paimti double reiksme bet kokiu formatu
        /// </summary>
        /// <param name="value">bandoma parse'inti double reikšmė</param>
        /// <returns>reikšmė kuria kompiuteris normaliai supranta</returns>
        private double GetDouble(string reiksme)
        {
            double rezutatas;

            reiksme = reiksme.Replace(',', '.');
            if (!double.TryParse(reiksme, System.Globalization.NumberStyles.Any,
                System.Globalization.CultureInfo.GetCultureInfo("en-US"),
                out rezutatas))
            {
                rezutatas = -1;
                MessageBox.Show("Nepavyko nuskaityti double reikšmių");
            }

            return rezutatas;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            IssaugotiRezultatus();
        }

        /// <summary>
        /// Metodas išsaugantis programos atliekamus rezultatus faile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IssaugotiRezultatus()
        {
            SaveFileDialog rasytojas = new SaveFileDialog();
            rasytojas.Filter = "Teksto failas|*.txt";
            rasytojas.FileName = "rezultatai";
            rasytojas.Title = "Išsaugoti teksto failą";
            if (rasytojas.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string kelias = rasytojas.FileName;
                using (StreamWriter teksRasytojas = new
                    StreamWriter(File.Create(kelias)))
                {
                    teksRasytojas.Write(richTextBox1.Text);
                }
            }
        }

        /// <summary>
        /// Metodas skirtas išsaugoti ekrane rodomus duomenis iš meniu 
        /// punkto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IšsaugotiDuomenisToolStripMenuItem1_Click(object sender,
            EventArgs e)
        {
            IssaugotiRezultatus();
        }

        /// <summary>
        /// Metodas skirtas nusakaitytyti iš failo paspaudus mygtuką
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ĮvestiŽaidėjusToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            if (failoPaieska.ShowDialog() == DialogResult.OK)
            {
                Krepsininkai = FormuotiKrepsininkus(failoPaieska.FileName);
                toolStripStatusLabel1.Text = "Krepšininkų failas nuskaitytas" +
                    " sėkmingai";
                įvestiŽaidėjusToolStripMenuItem.Enabled = false;
                spausdintiKrepšininkusToolStripMenuItem.Enabled = true;
                spausdintiGeriausiasPozicijasToolStripMenuItem.Enabled = true;
                rastiDaugiaiPataikiusiąToolStripMenuItem.Enabled = true;
            }
            else
            {
                toolStripStatusLabel1.Text = "Nepavyko nuskaityti failo";
            }
        }

        /// <summary>
        /// Metodas spausdinantis visų komandų/sąrašus duomenis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpausdintiKomandasToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            richTextBox1.Text += KomanduSarasas("Visos Komandos",Komandos);
            toolStripStatusLabel1.Text = "";
        }

        /// <summary>
        /// Metodas skirtas suformuoti komandų sąrašą/lentelę 
        /// </summary>
        /// <param name="pavadinimas">lentelės pavadinimas</param>
        /// <param name="Komandos">komandų sąrašas</param>
        /// <returns></returns>
        private string KomanduSarasas(string pavadinimas, Sarasas<Komanda> 
            Komandos)
        {
            string galutinis = "";
            galutinis += string.Format("                                 " +
                " {0, -25} \n",
                pavadinimas);
            galutinis += "|--------------------------" +
                "--------------------------------" +
                "---------------------|\n";
            galutinis += "| Pavadinimas             Zaista " +
                "                 Laimėta            Reitingas  |\n";
            galutinis += "|------------------------" +
                "-----------------------------------" +
                "--------------------|\n";
            for (Komandos.DPradzia(); Komandos.DYra(); Komandos.DKitas())
            {
                Komanda dabar = Komandos.ImtiDuomenis();

                galutinis += string.Format("| {0, -12}             {1, 2}" +
                    "                        {2, 2}                 {3, 5:f2}" +
                    "   |\n", dabar.Pavadinimas, dabar.Zaista, dabar.Laimeta,
                    dabar.GautiPergaliuSantyki());
            }
            if (Komandos.ArTuscias()) galutinis += "|                      " +
                    "           sąrašas tuščias                             " +
                    "  |\n";

            galutinis += "|-----------------------------------------" +
                "-----------------" +
                "---------------------|\n\n";

            return galutinis;
        }

        /// <summary>
        /// Metodas skirtas suformuoti zaidėjų sąrašą/lentelę
        /// </summary>
        /// <param name="pavadinimas">lentelės pavadinimai</param>
        /// <param name="Krepsininkai">krepsininkų sąrašas</param>
        /// <returns></returns>
        private string ZaidejuSarasas(string pavadinimas, Sarasas<Krepsininkas>
            Krepsininkai)
        {
            string galutinis = "";
            galutinis += string.Format("                         " +
                "     {0, -25}  \n",
                  pavadinimas);
            galutinis += "|------------------------------" +
                "-------------------------------------------------|\n";
            galutinis += "| Komanda   Vardas       Pavardė     " +
                "    Gimimo    Ūgis  Pozicija Taškai Klaidos|\n";
            galutinis += "|                                  " +
                "       data                                  |\n";
            galutinis += "|----------------------" +
                "---------------------------------------------------------|\n";
            for (Krepsininkai.DPradzia(); Krepsininkai.DYra();
                Krepsininkai.DKitas())
            {
                Krepsininkas dabar = Krepsininkai.ImtiDuomenis();

                galutinis += string.Format("| {0, -9} {1, -12} {2, -13}" +
                    " {3,-10}   {4,3}      {5,1}      {6, 2}    {7,2}    |\n",
                    dabar.Komanda,
                    dabar.Vardas,
                    dabar.Pavarde,
                    dabar.GimimoData.ToString("yyyy-MM-dd"),
                    dabar.Ugis,
                    dabar.Pozicija.ToString(),
                    dabar.PataikytiTaskai,
                    dabar.KlaiduKiekis
                );
            }
            if(Krepsininkai.ArTuscias()) galutinis+= "|                " +
                    "                 sąrašas tuščias          " +
                    "                     |\n";
            galutinis += "|-----------------------" +
                "--------------------------------------" +
                "------------------|\n\n";

            return galutinis;
        }

        /// <summary>
        /// Metodas atspausdina visus Zaidėjų sukauptus duomenis programoje
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpausdintiKrepšininkusToolStripMenuItem_Click(object 
            sender, EventArgs e)
        {
            richTextBox1.Text += ZaidejuSarasas("Visi Krepsininkai",
                Krepsininkai);
            toolStripStatusLabel1.Text += "";
        }
        
        /// <summary>
        /// Metodas surandandis geriausius krepsininkus pagal pozicija
        /// </summary>
        /// <param name="poz"></param>
        /// <returns></returns>
        private Sarasas<Krepsininkas> RastiGeriausiusKrepsininkus(Pozicija poz)
        {

            Sarasas<Krepsininkas> naujas = new Sarasas<Krepsininkas>();
            int taskai = int.MinValue;
            for (Krepsininkai.DPradzia(); Krepsininkai.DYra(); 
                Krepsininkai.DKitas())
            {
                if (Krepsininkai.ImtiDuomenis().Reitingas() > taskai &&
                    Krepsininkai.ImtiDuomenis().Pozicija == poz)
                    taskai = Krepsininkai.ImtiDuomenis().Reitingas();
            }

            for (Krepsininkai.DPradzia(); Krepsininkai.DYra();
                Krepsininkai.DKitas())
            {
                if (Krepsininkai.ImtiDuomenis().Reitingas() == taskai &&
                    Krepsininkai.ImtiDuomenis().Pozicija == poz)
                    naujas.PridetiPabaigoje(Krepsininkai.ImtiDuomenis());
            }
            return naujas;
        }

        /// <summary>
        /// Metodas randantis visus geriausius sportininkus visose pozocijose
        /// ir išveda juos į ekraną
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpausdintiGeriausiasPozicijasToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            richTextBox1.Text += ZaidejuSarasas("Centro geriausi", 
                RastiGeriausiusKrepsininkus(Pozicija.C));
            richTextBox1.Text += ZaidejuSarasas("Geriausi genėjai", 
                RastiGeriausiusKrepsininkus(Pozicija.G));
            richTextBox1.Text += ZaidejuSarasas("Geriausi puolėjai", 
                RastiGeriausiusKrepsininkus(Pozicija.P));
            toolStripStatusLabel1.Text = "Geriausios pozicijos atspausdintos";
        }

        /// <summary>
        /// Metodas suranda daugiausiai pataikiusią komandą 
        /// ir gražina jos objektą
        /// </summary>
        /// <returns></returns>
        private Sarasas<Komanda> RastiDaugiausiaiPataikiusias()
        {
            Dictionary<string, int> komand = new Dictionary<string, int>();
            for(Komandos.DPradzia(); Komandos.DYra(); Komandos.DKitas())
            {
                komand.Add(Komandos.ImtiDuomenis().Pavadinimas, 0);
            }

            for (Krepsininkai.DPradzia(); Krepsininkai.DYra();
                Krepsininkai.DKitas())
            {
                komand[Krepsininkai.ImtiDuomenis().Komanda] +=
                    Krepsininkai.ImtiDuomenis().Reitingas();
            }

            int maxReiksme = komand.Values.Max();
            Sarasas<Komanda> naujas = new Sarasas<Komanda>();
            foreach (KeyValuePair<string, int> reiksme in komand)
            {
                if (reiksme.Value == maxReiksme)
                    naujas.PridetiPabaigoje(RastiKomanda(reiksme.Key));
            }

            return naujas;
        }

        /// <summary>
        /// Metodas skirtas surasti komandos objektą saraše
        /// </summary>
        /// <param name="pavadinimas">komandos pavadinimas</param>
        /// <returns>Rasta komanda</returns>
        private Komanda RastiKomanda(string pavadinimas)
        {
            for (Komandos.DPradzia(); Komandos.DYra(); Komandos.DKitas())
            {
                if (Komandos.ImtiDuomenis().Pavadinimas == pavadinimas)
                {
                    return Komandos.ImtiDuomenis();
                }
            }
            return null;
        }

        /// <summary>
        /// Metodas Komandų saraše parodo kokią poziciją užėmė komanda
        /// </summary>
        /// <param name="komandos"></param>
        /// <param name="komanda"></param>
        /// <returns></returns>
        private int RastiKomandosVieta(Sarasas<Komanda> komandos,
            Komanda komanda)
        {
            komandos.RikiuotiBurbulu();
            int vieta = 1;
            for(komandos.DPradzia();  komandos.DYra(); komandos.DKitas())
            {
                if (komandos.ImtiDuomenis() == komanda)
                    return vieta;
                vieta++;
            }
            return -1;
        }

        /// <summary>
        /// Metodas surandantis ir į ekraną išvedanis komandą kuri daugiasiai
        /// pataikė, ir kurią ji vietą užimė
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RastiDaugiaiPataikiusiąToolStripMenuItem_Click(
            object sender, EventArgs e)
        {
            Sarasas<Komanda> daugaiausPataik = RastiDaugiausiaiPataikiusias();

            if (daugaiausPataik.ArTuscias())
            {
                richTextBox1.Text += "Tokia komanda nerasta\n";
                toolStripStatusLabel1.Text = "Komandos surasti nepavyko";
            }
            else
            {
                richTextBox1.Text += $"Daugiausiai pataikė \n";
                for (daugaiausPataik.DPradzia(); daugaiausPataik.DYra();
                    daugaiausPataik.DKitas())
                {
            richTextBox1.Text += $"komanda " +
            $"{daugaiausPataik.ImtiDuomenis()} ir užėmė " +
            $"{RastiKomandosVieta(Komandos, daugaiausPataik.ImtiDuomenis())}"+
            $" vietą\n";
                }
                    
                toolStripStatusLabel1.Text = "Komandos surasta sėkimingai";
            }
        }

        /// <summary>
        /// Metodas išvalantis ekrane sukauptą informaciją
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValytiEkranąToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            richTextBox1.Text = "";
        }
    }
}
