﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5
{
    /// <summary>
    /// Dvipusio Mazgo (node'o) klasė
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class Mazgas<T>
    {
        public T Duomenys { get; set; }
        public Mazgas<T> Desine { get; set; }
        public Mazgas<T> Kaire { get; set; }
        public Mazgas()
        {
        }

        public Mazgas(T duomenys, Mazgas<T> desine, Mazgas<T> kaire)
        {
            this.Duomenys = duomenys;
            this.Desine = desine;
            this.Kaire = kaire;
        }
    }
}
