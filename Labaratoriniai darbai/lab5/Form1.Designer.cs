﻿namespace lab5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.failasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ivestiKomandasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.įvestiŽaidėjusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.išsaugotiDuomenisToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.uždarytiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spausdinimasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spausdintiKomandasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spausdintiKrepšininkusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spausdintiGeriausiasPozicijasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rastiDaugiaiPataikiusiąToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.valytiEkranąToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.failasToolStripMenuItem,
            this.spausdinimasToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(703, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // failasToolStripMenuItem
            // 
            this.failasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ivestiKomandasToolStripMenuItem,
            this.įvestiŽaidėjusToolStripMenuItem,
            this.išsaugotiDuomenisToolStripMenuItem1,
            this.toolStripSeparator1,
            this.uždarytiToolStripMenuItem});
            this.failasToolStripMenuItem.Name = "failasToolStripMenuItem";
            this.failasToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.failasToolStripMenuItem.Text = "Failas";
            // 
            // ivestiKomandasToolStripMenuItem
            // 
            this.ivestiKomandasToolStripMenuItem.Name = "ivestiKomandasToolStripMenuItem";
            this.ivestiKomandasToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ivestiKomandasToolStripMenuItem.Text = "Įvesti komandas";
            this.ivestiKomandasToolStripMenuItem.Click += new System.EventHandler(this.IvestiKomandasToolStripMenuItem_Click);
            // 
            // įvestiŽaidėjusToolStripMenuItem
            // 
            this.įvestiŽaidėjusToolStripMenuItem.Name = "įvestiŽaidėjusToolStripMenuItem";
            this.įvestiŽaidėjusToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.įvestiŽaidėjusToolStripMenuItem.Text = "Įvesti žaidėjus";
            this.įvestiŽaidėjusToolStripMenuItem.Click += new System.EventHandler(this.ĮvestiŽaidėjusToolStripMenuItem_Click);
            // 
            // išsaugotiDuomenisToolStripMenuItem1
            // 
            this.išsaugotiDuomenisToolStripMenuItem1.Name = "išsaugotiDuomenisToolStripMenuItem1";
            this.išsaugotiDuomenisToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.išsaugotiDuomenisToolStripMenuItem1.Text = "Išsaugoti duomenis";
            this.išsaugotiDuomenisToolStripMenuItem1.Click += new System.EventHandler(this.IšsaugotiDuomenisToolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // uždarytiToolStripMenuItem
            // 
            this.uždarytiToolStripMenuItem.Name = "uždarytiToolStripMenuItem";
            this.uždarytiToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.uždarytiToolStripMenuItem.Text = "Uždaryti";
            this.uždarytiToolStripMenuItem.Click += new System.EventHandler(this.UždarytiToolStripMenuItem_Click);
            // 
            // spausdinimasToolStripMenuItem
            // 
            this.spausdinimasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spausdintiKomandasToolStripMenuItem,
            this.spausdintiKrepšininkusToolStripMenuItem,
            this.spausdintiGeriausiasPozicijasToolStripMenuItem,
            this.rastiDaugiaiPataikiusiąToolStripMenuItem,
            this.toolStripSeparator2,
            this.valytiEkranąToolStripMenuItem});
            this.spausdinimasToolStripMenuItem.Name = "spausdinimasToolStripMenuItem";
            this.spausdinimasToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.spausdinimasToolStripMenuItem.Text = "Spausdinimas";
            // 
            // spausdintiKomandasToolStripMenuItem
            // 
            this.spausdintiKomandasToolStripMenuItem.Name = "spausdintiKomandasToolStripMenuItem";
            this.spausdintiKomandasToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.spausdintiKomandasToolStripMenuItem.Text = "Spausdinti Komandas";
            this.spausdintiKomandasToolStripMenuItem.Click += new System.EventHandler(this.SpausdintiKomandasToolStripMenuItem_Click);
            // 
            // spausdintiKrepšininkusToolStripMenuItem
            // 
            this.spausdintiKrepšininkusToolStripMenuItem.Name = "spausdintiKrepšininkusToolStripMenuItem";
            this.spausdintiKrepšininkusToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.spausdintiKrepšininkusToolStripMenuItem.Text = "Spausdinti Krepšininkus";
            this.spausdintiKrepšininkusToolStripMenuItem.Click += new System.EventHandler(this.SpausdintiKrepšininkusToolStripMenuItem_Click);
            // 
            // spausdintiGeriausiasPozicijasToolStripMenuItem
            // 
            this.spausdintiGeriausiasPozicijasToolStripMenuItem.Name = "spausdintiGeriausiasPozicijasToolStripMenuItem";
            this.spausdintiGeriausiasPozicijasToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.spausdintiGeriausiasPozicijasToolStripMenuItem.Text = "Spausdinti geriausias pozicijas";
            this.spausdintiGeriausiasPozicijasToolStripMenuItem.Click += new System.EventHandler(this.SpausdintiGeriausiasPozicijasToolStripMenuItem_Click);
            // 
            // rastiDaugiaiPataikiusiąToolStripMenuItem
            // 
            this.rastiDaugiaiPataikiusiąToolStripMenuItem.Name = "rastiDaugiaiPataikiusiąToolStripMenuItem";
            this.rastiDaugiaiPataikiusiąToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.rastiDaugiaiPataikiusiąToolStripMenuItem.Text = "Rasti daugiai pataikiusią komandą";
            this.rastiDaugiaiPataikiusiąToolStripMenuItem.Click += new System.EventHandler(this.RastiDaugiaiPataikiusiąToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(250, 6);
            // 
            // valytiEkranąToolStripMenuItem
            // 
            this.valytiEkranąToolStripMenuItem.Name = "valytiEkranąToolStripMenuItem";
            this.valytiEkranąToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.valytiEkranąToolStripMenuItem.Text = "Valyti ekraną";
            this.valytiEkranąToolStripMenuItem.Click += new System.EventHandler(this.ValytiEkranąToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(703, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 27);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(679, 369);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(564, 402);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Išsaugoti kaip .txt";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem failasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uždarytiToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem įvestiŽaidėjusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ivestiKomandasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem išsaugotiDuomenisToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem spausdinimasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spausdintiKomandasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spausdintiKrepšininkusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spausdintiGeriausiasPozicijasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rastiDaugiaiPataikiusiąToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem valytiEkranąToolStripMenuItem;
    }
}

