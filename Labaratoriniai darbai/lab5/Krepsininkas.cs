﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5
{
    enum Pozicija
    {
        C, //centas 
        G, //ginejas 
        P //puolėjas
    }

    /// <summary>
    /// Klasė skirta saugoti krepšininko duomenis
    /// </summary>
    class Krepsininkas : IComparable<Krepsininkas>
    {
        public string Vardas { get; private set; }
        public string Pavarde { get; private set; }
        public DateTime GimimoData { get; private set; }
        public double Ugis { get; private set; }
        public Pozicija Pozicija { get; private set; }
        public int PataikytiTaskai { get; private set; }
        public int KlaiduKiekis { get; private set; }
        public string Komanda { get; private set; }

        public Krepsininkas()
        {

        }

        /// <summary>
        /// Konstruktoriaus perdengimas skirtas duomenų įvedimui
        /// </summary>
        /// <param name="vardas"></param>
        /// <param name="pavarde"></param>
        /// <param name="gimimoData"></param>
        /// <param name="ugis"></param>
        /// <param name="pozicija"></param>
        /// <param name="taskai"></param>
        /// <param name="klaidos"></param>
        public Krepsininkas(string komanda, string vardas, string pavarde,
            DateTime gimimoData, double ugis, Pozicija pozicija,
            int taskai, int klaidos)
        {
            this.Vardas = vardas;
            this.Pavarde = pavarde;
            this.GimimoData = gimimoData;
            this.Ugis = ugis;
            this.Pozicija = pozicija;
            this.PataikytiTaskai = taskai;
            this.KlaiduKiekis = klaidos;
            this.Komanda = komanda;
        }


        /// <summary>
        /// Metodas skirtas palyginti ir surikiuoti nuo didžiausio
        /// iki mažiausio naudojant Sort()
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Krepsininkas other)
        {
            if (this.PataikytiTaskai < other.PataikytiTaskai)
                return 1;
            else if (this.PataikytiTaskai > other.PataikytiTaskai)
                return -1;
            else
                return 0;
        }

        /// <summary>
        /// To string metodo perdengimas
        /// </summary>
        /// <returns>Krepšininko vardas ir pavardė</returns>
        public override string ToString()
        {
            return Vardas + " " + Pavarde;
        }

        /// <summary>
        /// Metodas skirtas sukrasti sportininko taiklumą procentais
        /// </summary>
        /// <returns>procentinis taiklumas</returns>
        public int Reitingas()
        {
            return PataikytiTaskai - KlaiduKiekis;
        }
    }
}
