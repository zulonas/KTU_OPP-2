﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5
{
    class Komanda : IComparable<Komanda>
    {
        public string Pavadinimas { get; private set; }
        public int Zaista { get; private set; }
        public int Laimeta { get; private set; }

        public Komanda()
        {

        }

        public Komanda(string pavadinimas, int zaista, int laimeta)
        {
            this.Pavadinimas = pavadinimas;
            this.Zaista = zaista;
            this.Laimeta = laimeta;
        }

        public override string ToString()
        {
            return Pavadinimas;
        }

        /// <summary>
        /// Metodas skirtas gauti Laimėtų ir pralaimėtų varžybų santikį
        /// </summary>
        /// <returns></returns>
        public double GautiPergaliuSantyki()
        {
            return (double)Laimeta / Zaista;
        }

        public int CompareTo(Komanda other)
        {
            if (this.GautiPergaliuSantyki() > other.GautiPergaliuSantyki())
                return -1;
            else if (this.GautiPergaliuSantyki() < other.GautiPergaliuSantyki())
                return 1;
            else
                return 0;
        }
    }
}
