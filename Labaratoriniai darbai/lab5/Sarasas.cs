﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5
{
    /// <summary>
    /// Bendrinė Sąrašo klasė
    /// </summary>
    /// <typeparam name="T"></typeparam>
    sealed class Sarasas<T> where T : IComparable<T>
    {
        private Mazgas<T> Pradzia;
        private Mazgas<T> Pabaiga;
        private Mazgas<T> Darbinis;

        public Sarasas()
        {
            Pradzia = null;
            Pabaiga = null;
            Darbinis = null;
        }

        /// <summary>
        /// Metodas skirtas prideti mazgą su pasirinktias duomenimis
        /// sąrašo pradžioje
        /// </summary>
        /// <param name="duomenys">norimi pridėti duomenys</param>
        public void PridetiPradzioje(T duomenys)
        {
            Mazgas<T> naujas = new Mazgas<T>(duomenys, null, null);
            if (Pradzia != null)
            {
                Pradzia.Kaire = naujas;
                naujas.Desine = Pradzia;
            }
            else
                Pabaiga = naujas;
            Pradzia = naujas;
        }

        /// <summary>
        /// Metodas skirtas prideti mazgą su pasirinktias duomenimis
        /// sąrašo pabaigoje
        /// </summary>
        /// <param name="duomenys">norimi pridėti duomenys</param>
        public void PridetiPabaigoje(T duomenys)
        {
            Mazgas<T> naujas = new Mazgas<T>(duomenys, null, null);
            if (Pabaiga != null)
            {
                Pabaiga.Desine = naujas;
                naujas.Kaire = Pabaiga; 
            }
            else
                Pradzia = naujas;
            Pabaiga = naujas;
        }

        /// <summary>
        /// Gražina darbinio mazgo duomenis
        /// </summary>
        /// <returns></returns>
        public T ImtiDuomenis()
        {
            return Darbinis.Duomenys;
        }

        /// <summary>
        /// Darbinis mazgas nukreipiamas į sąrašo pradžią
        /// </summary>
        public void DPradzia()
        {
            Darbinis = this.Pradzia;
        }

        /// <summary>
        /// Darbinis mazgas nukreipiamas į sąrašo pabaigą
        /// </summary>
        public void DPabaiga()
        {
            Darbinis = this.Pradzia;
        }

        /// <summary>
        /// Metodas tikrinantis ar dabartinis darbinis elementas 
        /// yra nelygus nuliui
        /// </summary>
        /// <returns>true - jeigu nelygus</returns>
        public bool DYra()
        {
            return Darbinis != null;
        }

        /// <summary>
        /// Darbiniam elementui
        /// priskiriamas sąrašo elementas, esantis dešiniau
        /// </summary>
        public void DKitas()
        {
            Darbinis = Darbinis.Desine;
        }

        /// <summary>
        ///  Darbiniam elementui
        /// priskiriamas sąrašo elementas, esantis kairiau
        /// </summary>
        public void Kainen()
        {
            Darbinis = Darbinis.Kaire;
        }

        /// <summary>
        /// Metodas skirtas sunaikinti sąrašą ir visas jo nuorodas
        /// nepanaudojamai
        /// </summary>
        public void NaikintiSarasa()
        {
            while(Pradzia != null)
            {
                Darbinis = Pradzia;
                Pradzia = Pradzia.Desine;
                Darbinis.Desine = null;
                Darbinis.Kaire = null;
            }
            Pabaiga = Darbinis = Pradzia = null;
        }

        /// <summary>
        /// Metodas skirtas išrikiuoti sąrašo elementus naudojant
        /// MinMax metodas. Objekto klasėse BŪTINAI turi būti naudojama
        /// IComparable sasaja.
        /// </summary>
        public void RikiuotiMinMax()
        {
            for(Mazgas<T> d1=Pradzia; d1 != null; d1 = d1.Desine)
            {
                Mazgas<T> maksimalus = d1; // mazgas su max reiksme
                for(Mazgas<T> d2= d1; d2 != null; d2 = d2.Desine)
                {
                    if(d2.Duomenys.CompareTo(maksimalus.Duomenys) < 0)
                    //reikalingas sąsajos metodas
                    {
                        maksimalus = d2;
                    }
                    //Sukeitimas
                    T duomenysTemp = d1.Duomenys;
                    d1.Duomenys = maksimalus.Duomenys;
                    maksimalus.Duomenys = duomenysTemp;
                }
            }
        }

        /// <summary>
        /// Metodas išrikiuojantis duomenis Burbuliuko metodu. Objekto klasėse
        /// BŪTINAI turi būti naudojama
        /// IComparable sasaja.
        /// </summary>
        public void RikiuotiBurbulu()
        {
            bool keitimas = true;
            Mazgas<T> d1, d2;
            while (keitimas)
            {
                keitimas = false;
                d1 = Pradzia;
                d2 = Pradzia; 
                while(d2 != null)
                {
                    if(d2.Duomenys.CompareTo(d1.Duomenys) < 0)
                    {
                        T duomenysTemp = d1.Duomenys;
                        d1.Duomenys = d2.Duomenys;
                        d2.Duomenys = duomenysTemp;
                        keitimas = true;
                    }
                    d1 = d2;
                    d2 = d2.Desine;
                }
            }
        }


        /// <summary>
        /// Tikrina ar sąrašas yra tuščias
        /// </summary>
        /// <returns></returns>
        public bool ArTuscias()
        {
            return Pradzia == null;
        }
    }
}
