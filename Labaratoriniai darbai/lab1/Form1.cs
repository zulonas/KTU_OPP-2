﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Globalization;

namespace lab1
{
    public partial class Forma : Form
    {
        public Forma()
        {
            InitializeComponent();
            mygtIvesti.Enabled = false;
        }

        OpenFileDialog failoPaieska = new OpenFileDialog();
        Kolekcionieriai KolekcionKont = new Kolekcionieriai();

        /// <summary>
        /// Kai mygtukas paisirinkti paspaudžiamas atlkiekama kailo paieškos
        /// sistema
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mygtPasirinkti_Click(object sender, EventArgs e)
        {
            if(failoPaieska.ShowDialog() == DialogResult.OK)
            {
                tekstFailoPav.Text = failoPaieska.SafeFileName;
                mygtIvesti.Enabled = true;
            }
        }

        /// <summary>
        /// Kai mygtukas mygtIvesti paspaudžia aktyvuojamas duomenų
        /// įvedimas 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mygtIvesti_Click(object sender, EventArgs e)
        {
            try
            {
                SkaitytiKolekcFaila(KolekcionKont, failoPaieska.FileName);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                MessageBox.Show("Nepavyko nuskaityti visų failo duomenų",
                    "Klaida");
                return;
            }
            finally
            {
                mygtIvesti.Enabled = false;
                tekstFailoPav.Text = "failas nepasirinktas";
            }
            statusoEtikete1.Text = "Failas nuskaitytas sėkmingai";
            failoPaieska.Reset();            
        }

        /// <summary>
        /// Nuskaito duomenų failą su duotu failo keliu, ir duomenis įveda į
        /// Kolekcionieriai objektą
        /// </summary>
        /// <param name="KolekcionKont"></param>
        /// <param name="failoKelias"></param>
        private void SkaitytiKolekcFaila(Kolekcionieriai KolekcionKont,
            string failoKelias)
        {
            using (StreamReader srautas = new StreamReader(failoKelias,
                Encoding.GetEncoding("utf-8")))
            {
                string eilute;
                while ((eilute = srautas.ReadLine()) != null)
                {
                    string[] eilDalys = eilute.Split(' ');
                    Kolekcionierius kolekcionierius = new Kolekcionierius();
                    kolekcionierius.Deti(eilDalys[0], eilDalys[1]);

                    KAtvirukai kAtvirukai = new KAtvirukai();
                    //Tikrinimas ar yra eilutė tobuliuota jeigu taip tada
                    //skaitymas tesiamas
                    while (!string.IsNullOrEmpty(
                        (eilute = srautas.ReadLine())))
                    {
                        eilDalys = eilute.Split(' ');
                        string pavadinimas = eilDalys[0].Replace("\t", "");
                        string salis = eilDalys[1];
                        DateTime metai;
                        if (!DateTime.TryParseExact(eilDalys[2], "yyyy",
                            CultureInfo.CurrentCulture, 
                            DateTimeStyles.None , out metai))
                        {
                            metai = DateTime.MinValue;
                        }
                        bool spalvota = (eilDalys[3] == "S") ? true : false;
                        double aukstis = double.Parse(eilDalys[4]);
                        double plotis = double.Parse(eilDalys[5]);
                        int kiekis = int.Parse(eilDalys[6]);
                        Atvirukas atvirukas = new Atvirukas(pavadinimas,
                            salis, metai, spalvota, aukstis,
                            plotis, kiekis);
                        kAtvirukai.DetiAtviruka(atvirukas);
                    }
                    KolekcionKont.DetiKolekcionieriu(kAtvirukai, 
                        kolekcionierius);
                }
            }
        }

        /// <summary>
        /// Pagrindinio lango matodas atliekamas kai užkraunamas pagrindinis
        /// langas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            statusoEtikete1.Text = "";
        }

        /// <summary>
        /// Metodas atliekamas kai mygtukas Uždaryti programą paspaudžiamas
        /// uždarantis programą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barmenuUzdaryti_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Medotas aktyvuojamas kai mygtukas mygtSpalvosRasti paspaudžiamas
        /// aktyvuojamas paieškos funkciją
        /// </summary>
        /// <param name="sendet"></param>
        /// <param name="e"></param>
        private void mygtSpalvosRasti_Click(object sendet, EventArgs e)
        {
            string galutinis = KolekcionKont.GautiSpalvotuTuretoja
                (pasirinkValstybes.Text);
            if(galutinis == "")
            {
                MessageBox.Show("Nepavyko surasti tokio kolekcininko",
                    "Klaida");
                return;
            }
            statusoEtikete1.Text = "Kolekcionieriai turintys daugiausiai "
                + pasirinkValstybes.Text + " atviručių nuskatyti sėkmingai";
            platausTesktoDeze.Text += "\n\tKolekcionieriai turintys" +
                " daugiausiai spalvotų ";
            platausTesktoDeze.Text += pasirinkValstybes.Text;
            platausTesktoDeze.Text += " atviruku yra: \n\t\t\t\t";
            platausTesktoDeze.Text += galutinis;
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas mygtSpausdinti paspaudžiamas
        /// aktyvuojantis spaudinimo ir atnaujinimo metodus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mygtSpausdinti_Click(object sender, EventArgs e)
        {
            platausTesktoDeze.Text += KolekcionKont.tekstasFormatuotas();
            platausTesktoDeze.Text += 
                KolekcionKont.tekstasFormatuotasDaugiau();
            AtnaujiniSalis();
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas mygtIsvalyti paspaudžiamas
        /// kuris išvalo spausdinimo langą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mygtIsvalytiLanga_Click(object sender, EventArgs e)
        {
            platausTesktoDeze.Text = "";
            statusoEtikete1.Text = "Rašymo langas išvalytas";
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas mygtRikiavimas aktyvuojamas
        /// paleidžiantis rikiaviko funkciką kuri surikiuoja atvirukus
        /// pagal metus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mygtRikiavimas_Click(object sender, EventArgs e)
        {
            for(int x=0; x<KolekcionKont.kolekcKiekis; x++)
            {
                KolekcionKont.ImtiKolekcionieriuDaugiau(x).
                    AtvirukuRikiavimas();
            }
            if (KolekcionKont.kolekcKiekis == 0)
                statusoEtikete1.Text = "Nėra duomenų";
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas mygtSalinti paspaudžiamas
        /// leidžiantis pašalinti pašalinti Kolekcionierių atvirukus
        /// kurių metai yra nežinomi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mygtSalinti_Click(object sender, EventArgs e)
        {
            for (int x = 0; x < KolekcionKont.kolekcKiekis; x++)
            {
                KolekcionKont.ImtiKolekcionieriuDaugiau(x).SalintiNulinius();
            }
            if (KolekcionKont.kolekcKiekis == 0)
                statusoEtikete1.Text = "Nėra duomenų";
        }

        /// <summary>
        /// Metodas skirtas Atnaujinti šalių sąrašą kuris pateikiamas 
        /// pasirinkValstybes combobox'e 
        /// </summary>
        private void AtnaujiniSalis()
        {
            for (int x = 0; x < KolekcionKont.saliuKiekis; x++)
            {
                if (!pasirinkValstybes.Items.Contains(
                    KolekcionKont.GautiSaliuSarasa()[x]))
                {
                    pasirinkValstybes.Items.Insert(x,
                        KolekcionKont.GautiSaliuSarasa()[x]);
                }

            }
            statusoEtikete1.Text = "Atnaujintas saliu sąrašas";
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas meniubuttonišsaugotiDuomenis
        /// paspaudžiamas kuris leidžia įrašyti kolekcionieirių 
        /// klasės su apdorotais duomenimis iškart panaudojant rušiavimo ir
        /// šalinimo metodus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void meniubuttonišsaugotiDuomenis_Click(object sender,
            EventArgs e)
        {
            SaveFileDialog rasytojas = new SaveFileDialog();
            rasytojas.Filter = "Teksto failas|*.txt";
            rasytojas.FileName = "duomenys";
            rasytojas.Title = "Išsaugoti teksto failą";
            if (rasytojas.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string kelias = rasytojas.FileName;
                using (StreamWriter teksRasytojas = new
                    StreamWriter(File.Create(kelias)))
                {
                    teksRasytojas.Write("                          PRADINIAI"+
                        " DUOMENYS\n");
                    teksRasytojas.Write(KolekcionKont.tekstasFormatuotas());
                    teksRasytojas.Write(KolekcionKont.
                        tekstasFormatuotasDaugiau());
                    mygtRikiavimas_Click(sender, e);
                    teksRasytojas.Write("\n                       SURIKIUOTI" +
                        " DUOMENYS\n");
                    teksRasytojas.Write(KolekcionKont.tekstasFormatuotas());
                    teksRasytojas.Write(KolekcionKont.
                        tekstasFormatuotasDaugiau());
                    mygtSalinti_Click(sender, e);
                    teksRasytojas.Write("\n                       PAŠALINTI " +
                        "NEŽINOMI METAI\n");
                    teksRasytojas.Write(KolekcionKont.tekstasFormatuotas());
                    teksRasytojas.Write(KolekcionKont.
                        tekstasFormatuotasDaugiau());
                    statusoEtikete1.Text = "Duomenis išsaugoti";
                }
            }
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas meniuButtonAtvertiDuomenis
        /// paspaudžiamas
        /// kuris leidžia nuskaityti duomenis iš failo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void meniuButtonAtvertiDuomenis_Click(object sender,
            EventArgs e)
        {
            if (failoPaieska.ShowDialog() == DialogResult.OK)
            {
                tekstFailoPav.Text = failoPaieska.SafeFileName;
                mygtIvesti.Enabled = true;
            }
        }
    }
}
