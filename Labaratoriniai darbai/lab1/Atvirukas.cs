﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace lab1
{
    /// <summary>
    /// Klasė aprašanti Atviruko parametrus
    /// </summary>
    class Atvirukas
    {
        public string pavadinimas { get; private set; }
        public string valstybe { get; private set; }
        public DateTime metai { get; private set; }
        public bool spalvotas { get; private set; }
        public double aukstis { get; private set; }
        public double plotis { get; private set; }
        public int kiekis { get; private set; }

        public Atvirukas()
        {
            pavadinimas = "";
            spalvotas = false;
            aukstis = 0;
            plotis = 0;
            kiekis = 0;
            valstybe = "NE";
            metai = DateTime.MinValue;
        }

        /// <summary>
        /// Metodas skirtas prisikirti atviruko reiksmes
        /// </summary>
        /// <param name="pavadinimas"></param>
        /// <param name="salis"></param>
        /// <param name="metai"></param>
        /// <param name="spalvotas"></param>
        /// <param name="aukstis"></param>
        /// <param name="plotis"></param>
        /// <param name="kiekis"></param>
        public Atvirukas(string pavadinimas, string salis, DateTime metai, 
            bool spalvotas, double aukstis, double plotis, int kiekis)
        {
            this.pavadinimas = pavadinimas;
            this.valstybe = salis;
            this.metai = metai;
            this.spalvotas = spalvotas;
            this.aukstis = aukstis;
            this.plotis = plotis;
            this.kiekis = kiekis;
        }

        /// <summary>
        /// Metodas skirtas gražinti gražiai formatuotą tekstą naudojamą 
        /// gražiam teksto atavaizdavimui
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string galutinis = "";
            string metai = (DateTime.Compare(this.metai, DateTime.MinValue)
                != 0) ? this.metai.ToString("yyyy") : "0";
            string spalvotas = (this.spalvotas == true) ? "Taip" : "Ne";
            galutinis += string.Format(" {0, -13}   {1,-4}    {2, 6}    " +
                "  {3,-7}    {4, 5}     {5, 5}   {6, 4}     ", pavadinimas,
                valstybe, metai, spalvotas, aukstis, plotis, kiekis);
            return galutinis;
        }

        /// <summary>
        /// Perklotas > operatorius
        /// </summary>
        /// <param name="atvir1"></param>
        /// <param name="atvir2"></param>
        /// <returns></returns>
        public static bool operator >(Atvirukas atvir1, Atvirukas atvir2)
        {
            int rezultatas = DateTime.Compare(atvir1.metai, atvir2.metai);
            if (rezultatas > 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Perklotas < operatorius
        /// </summary>
        /// <param name="atvir1"></param>
        /// <param name="atvir2"></param>
        /// <returns></returns>
        public static bool operator <(Atvirukas atvir1, Atvirukas atvir2)
        {
            int rezultatas = DateTime.Compare(atvir1.metai, atvir2.metai);

            if (rezultatas < 0)
                return true;
            else
                return false;
        }
    }
}
