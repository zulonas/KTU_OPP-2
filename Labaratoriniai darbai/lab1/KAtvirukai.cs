﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    /// <summary>
    /// Konteinerinė kolekcionierių klasė savyje talpinanti atvirukus
    /// </summary>
    class KAtvirukai
    {
        const int Cn = 50;
        private Atvirukas[] Atvirukai;
        public int AtvirukuKiek { get; private set; } //talpinamas
        //atviruku kiekis

        public KAtvirukai()
        {
            AtvirukuKiek = 0;
            Atvirukai = new Atvirukas[Cn];
        }

        /// <summary>
        /// Metodas gražinantas paririnktą atviruką pagal indeksą
        /// </summary>
        /// <param name="i">norimas atviruko indeksas</param>
        /// <returns></returns>
        public Atvirukas ImtiAtviruka(int i) { return Atvirukai[i]; }

        /// <summary>
        /// Metodas skirtas pridėti papildomą atviruką prie kolekcionieriaus
        /// </summary>
        /// <param name="naujas"></param>
        public void DetiAtviruka(Atvirukas naujas) {
            Atvirukai[AtvirukuKiek++] = naujas;
        }

        /// <summary>
        /// Metodas surandantis kiek pasirinktos valstybės spalvotų atviruku
        /// turi kolekcionierius
        /// </summary>
        /// <param name="valstybe">valstybes pavadinimas</param>
        /// <returns></returns>
        public int GautiKiekiSpalvotu(string valstybe)
        {
            int kiekis = 0;
            for(int x=0; x<AtvirukuKiek; x++)
            {
                if (ImtiAtviruka(x).valstybe == valstybe &&
                    ImtiAtviruka(x).spalvotas == true)
                    kiekis++;
            }
            return kiekis;
        }

        /// <summary>
        /// Metodas skirtas gražinti gražiai formatuotą tekstą naudojamą 
        /// gražiam teksto atavaizdavimui
        /// </summary>
        /// <returns>formatuotas tekstas</returns>
        public override string ToString()
        {
            string galutinis = "";
            galutinis += "--------------------------------------------" +
                "-------------------------------\n";
            galutinis += "| Pavadinimas | Šalis | Išleidimo" +
                " | Spalvotas | Plotis | Aukštis | Kiekis |\n";
            galutinis += "|             |       |    " +
                "data   |           |        |         |        |\n";
            galutinis += "-------------------------------------" +
                "--------------------------------------\n";
            for (int x = 0; x < AtvirukuKiek; x++)
            {
                galutinis += ImtiAtviruka(x).ToString() + "\n";
            }
            galutinis += "|--------------------------------------------" +
                "-----------------------------|\n";
            return galutinis;
        }

        /// <summary>
        /// Metodas skitas surikiuoti "Kolekcionierius" klasės
        /// objektų masyvą Atvirtukai
        /// </summary>
        public void AtvirukuRikiavimas()
        {
            int i = 0;
            bool keitimas = true;
            while (keitimas)
            {
                keitimas = false;
                for (int j = AtvirukuKiek - 1; j > i; j--)
                    if (Atvirukai[j] < Atvirukai[j - 1])
                    {
                        keitimas = true;
                        Atvirukas laikinas = Atvirukai[j];
                        Atvirukai[j] = Atvirukai[j - 1];
                        Atvirukai[j - 1] = laikinas;
                    }
                i++;
            }
        }

        /// <summary>
        /// Metodas pašalina nulines reiksmes iš Atviruku objekto masyvo
        /// </summary>
        public void SalintiNulinius()
        {
            for (int x = 0; x < AtvirukuKiek; x++)
            {
                if(ImtiAtviruka(x).metai == DateTime.MinValue)
                {
                    Atvirukas laikinas = Atvirukai[AtvirukuKiek-1];
                    Atvirukai[x] = laikinas;
                    AtvirukuKiek--;
                    x--; //nes reikia patiktinti ar paskutinis
                    //elementas yra tinkamas
                }
            }
        }
    }
}
