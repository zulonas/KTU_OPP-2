﻿namespace lab1
{
    partial class Forma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusoJuosta = new System.Windows.Forms.StatusStrip();
            this.StatusoEtikete = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusoEtikete1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.mygtPasirinkti = new System.Windows.Forms.Button();
            this.mygtIvesti = new System.Windows.Forms.Button();
            this.tekstFailoPav = new System.Windows.Forms.Label();
            this.meniuJuosta = new System.Windows.Forms.MenuStrip();
            this.meniuKategorijaFailas = new System.Windows.Forms.ToolStripMenuItem();
            this.kategAtvertiDuomenis = new System.Windows.Forms.ToolStripMenuItem();
            this.kategIssaugotiDuomenis = new System.Windows.Forms.ToolStripMenuItem();
            this.kategUzdaryti = new System.Windows.Forms.ToolStripMenuItem();
            this.grupesDeze1 = new System.Windows.Forms.GroupBox();
            this.grupesDeze3 = new System.Windows.Forms.GroupBox();
            this.pasirinkValstybes = new System.Windows.Forms.ComboBox();
            this.mygtSpalvosRasti = new System.Windows.Forms.Button();
            this.grupesDeze4 = new System.Windows.Forms.GroupBox();
            this.mygtRikiavimas = new System.Windows.Forms.Button();
            this.grupesDeze5 = new System.Windows.Forms.GroupBox();
            this.mygtSalinti = new System.Windows.Forms.Button();
            this.grupesDeze2 = new System.Windows.Forms.GroupBox();
            this.mygtIsvalytiLanga = new System.Windows.Forms.Button();
            this.mygtSpausdinti = new System.Windows.Forms.Button();
            this.platausTesktoDeze = new System.Windows.Forms.RichTextBox();
            this.statusoJuosta.SuspendLayout();
            this.meniuJuosta.SuspendLayout();
            this.grupesDeze1.SuspendLayout();
            this.grupesDeze3.SuspendLayout();
            this.grupesDeze4.SuspendLayout();
            this.grupesDeze5.SuspendLayout();
            this.grupesDeze2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusoJuosta
            // 
            this.statusoJuosta.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusoEtikete,
            this.statusoEtikete1});
            this.statusoJuosta.Location = new System.Drawing.Point(0, 497);
            this.statusoJuosta.Name = "statusoJuosta";
            this.statusoJuosta.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusoJuosta.Size = new System.Drawing.Size(970, 22);
            this.statusoJuosta.TabIndex = 0;
            this.statusoJuosta.Text = "statusStrip1";
            // 
            // StatusoEtikete
            // 
            this.StatusoEtikete.Name = "StatusoEtikete";
            this.StatusoEtikete.Size = new System.Drawing.Size(0, 17);
            // 
            // statusoEtikete1
            // 
            this.statusoEtikete1.Name = "statusoEtikete1";
            this.statusoEtikete1.Size = new System.Drawing.Size(34, 17);
            this.statusoEtikete1.Text = "none";
            // 
            // mygtPasirinkti
            // 
            this.mygtPasirinkti.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F);
            this.mygtPasirinkti.Location = new System.Drawing.Point(24, 41);
            this.mygtPasirinkti.Margin = new System.Windows.Forms.Padding(4);
            this.mygtPasirinkti.Name = "mygtPasirinkti";
            this.mygtPasirinkti.Size = new System.Drawing.Size(96, 26);
            this.mygtPasirinkti.TabIndex = 2;
            this.mygtPasirinkti.Text = "Pasirinkti failą";
            this.mygtPasirinkti.UseVisualStyleBackColor = true;
            this.mygtPasirinkti.Click += new System.EventHandler(this.mygtPasirinkti_Click);
            // 
            // mygtIvesti
            // 
            this.mygtIvesti.Enabled = false;
            this.mygtIvesti.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F);
            this.mygtIvesti.Location = new System.Drawing.Point(169, 41);
            this.mygtIvesti.Margin = new System.Windows.Forms.Padding(4);
            this.mygtIvesti.Name = "mygtIvesti";
            this.mygtIvesti.Size = new System.Drawing.Size(108, 26);
            this.mygtIvesti.TabIndex = 4;
            this.mygtIvesti.Text = "Įkelti duomenis";
            this.mygtIvesti.UseVisualStyleBackColor = true;
            this.mygtIvesti.Click += new System.EventHandler(this.mygtIvesti_Click);
            // 
            // tekstFailoPav
            // 
            this.tekstFailoPav.AutoSize = true;
            this.tekstFailoPav.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.tekstFailoPav.ForeColor = System.Drawing.SystemColors.GrayText;
            this.tekstFailoPav.Location = new System.Drawing.Point(32, 94);
            this.tekstFailoPav.Margin = new System.Windows.Forms.Padding(3);
            this.tekstFailoPav.Name = "tekstFailoPav";
            this.tekstFailoPav.Size = new System.Drawing.Size(96, 13);
            this.tekstFailoPav.TabIndex = 6;
            this.tekstFailoPav.Text = "failas nepasirinktas";
            // 
            // meniuJuosta
            // 
            this.meniuJuosta.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.meniuKategorijaFailas});
            this.meniuJuosta.Location = new System.Drawing.Point(0, 0);
            this.meniuJuosta.Name = "meniuJuosta";
            this.meniuJuosta.Size = new System.Drawing.Size(970, 24);
            this.meniuJuosta.TabIndex = 10;
            this.meniuJuosta.Text = "menuStrip1";
            // 
            // meniuKategorijaFailas
            // 
            this.meniuKategorijaFailas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kategAtvertiDuomenis,
            this.kategIssaugotiDuomenis,
            this.kategUzdaryti});
            this.meniuKategorijaFailas.Name = "meniuKategorijaFailas";
            this.meniuKategorijaFailas.Size = new System.Drawing.Size(48, 20);
            this.meniuKategorijaFailas.Text = "Failas";
            // 
            // kategAtvertiDuomenis
            // 
            this.kategAtvertiDuomenis.Name = "kategAtvertiDuomenis";
            this.kategAtvertiDuomenis.Size = new System.Drawing.Size(180, 22);
            this.kategAtvertiDuomenis.Text = "Atverti duomenis";
            this.kategAtvertiDuomenis.Click += new System.EventHandler(this.meniuButtonAtvertiDuomenis_Click);
            // 
            // kategIssaugotiDuomenis
            // 
            this.kategIssaugotiDuomenis.Name = "kategIssaugotiDuomenis";
            this.kategIssaugotiDuomenis.Size = new System.Drawing.Size(180, 22);
            this.kategIssaugotiDuomenis.Text = "Įrašyti kaip";
            this.kategIssaugotiDuomenis.Click += new System.EventHandler(this.meniubuttonišsaugotiDuomenis_Click);
            // 
            // kategUzdaryti
            // 
            this.kategUzdaryti.Name = "kategUzdaryti";
            this.kategUzdaryti.Size = new System.Drawing.Size(180, 22);
            this.kategUzdaryti.Text = "Uždaryti programą";
            this.kategUzdaryti.Click += new System.EventHandler(this.barmenuUzdaryti_Click);
            // 
            // grupesDeze1
            // 
            this.grupesDeze1.Controls.Add(this.mygtIvesti);
            this.grupesDeze1.Controls.Add(this.mygtPasirinkti);
            this.grupesDeze1.Controls.Add(this.tekstFailoPav);
            this.grupesDeze1.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupesDeze1.Location = new System.Drawing.Point(660, 27);
            this.grupesDeze1.Name = "grupesDeze1";
            this.grupesDeze1.Size = new System.Drawing.Size(298, 127);
            this.grupesDeze1.TabIndex = 12;
            this.grupesDeze1.TabStop = false;
            this.grupesDeze1.Text = "Duomenų įkėlimas";
            // 
            // grupesDeze3
            // 
            this.grupesDeze3.Controls.Add(this.pasirinkValstybes);
            this.grupesDeze3.Controls.Add(this.mygtSpalvosRasti);
            this.grupesDeze3.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F);
            this.grupesDeze3.Location = new System.Drawing.Point(660, 282);
            this.grupesDeze3.Name = "grupesDeze3";
            this.grupesDeze3.Size = new System.Drawing.Size(298, 105);
            this.grupesDeze3.TabIndex = 13;
            this.grupesDeze3.TabStop = false;
            this.grupesDeze3.Text = "Spalvotų turėtojas";
            // 
            // pasirinkValstybes
            // 
            this.pasirinkValstybes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pasirinkValstybes.FormattingEnabled = true;
            this.pasirinkValstybes.Location = new System.Drawing.Point(24, 48);
            this.pasirinkValstybes.Name = "pasirinkValstybes";
            this.pasirinkValstybes.Size = new System.Drawing.Size(167, 24);
            this.pasirinkValstybes.TabIndex = 2;
            // 
            // mygtSpalvosRasti
            // 
            this.mygtSpalvosRasti.Location = new System.Drawing.Point(207, 48);
            this.mygtSpalvosRasti.Name = "mygtSpalvosRasti";
            this.mygtSpalvosRasti.Size = new System.Drawing.Size(85, 23);
            this.mygtSpalvosRasti.TabIndex = 1;
            this.mygtSpalvosRasti.Text = "Rasti";
            this.mygtSpalvosRasti.UseVisualStyleBackColor = true;
            this.mygtSpalvosRasti.Click += new System.EventHandler(this.mygtSpalvosRasti_Click);
            // 
            // grupesDeze4
            // 
            this.grupesDeze4.Controls.Add(this.mygtRikiavimas);
            this.grupesDeze4.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F);
            this.grupesDeze4.Location = new System.Drawing.Point(660, 393);
            this.grupesDeze4.Name = "grupesDeze4";
            this.grupesDeze4.Size = new System.Drawing.Size(142, 102);
            this.grupesDeze4.TabIndex = 14;
            this.grupesDeze4.TabStop = false;
            this.grupesDeze4.Text = "Rikiavimas";
            // 
            // mygtRikiavimas
            // 
            this.mygtRikiavimas.Location = new System.Drawing.Point(24, 43);
            this.mygtRikiavimas.Name = "mygtRikiavimas";
            this.mygtRikiavimas.Size = new System.Drawing.Size(96, 30);
            this.mygtRikiavimas.TabIndex = 0;
            this.mygtRikiavimas.Text = "pagal metus";
            this.mygtRikiavimas.UseVisualStyleBackColor = true;
            this.mygtRikiavimas.Click += new System.EventHandler(this.mygtRikiavimas_Click);
            // 
            // grupesDeze5
            // 
            this.grupesDeze5.Controls.Add(this.mygtSalinti);
            this.grupesDeze5.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F);
            this.grupesDeze5.Location = new System.Drawing.Point(808, 393);
            this.grupesDeze5.Name = "grupesDeze5";
            this.grupesDeze5.Size = new System.Drawing.Size(150, 102);
            this.grupesDeze5.TabIndex = 15;
            this.grupesDeze5.TabStop = false;
            this.grupesDeze5.Text = "Šalinimas";
            // 
            // mygtSalinti
            // 
            this.mygtSalinti.Location = new System.Drawing.Point(19, 43);
            this.mygtSalinti.Name = "mygtSalinti";
            this.mygtSalinti.Size = new System.Drawing.Size(108, 30);
            this.mygtSalinti.TabIndex = 0;
            this.mygtSalinti.Text = "be datos";
            this.mygtSalinti.UseVisualStyleBackColor = true;
            this.mygtSalinti.Click += new System.EventHandler(this.mygtSalinti_Click);
            // 
            // grupesDeze2
            // 
            this.grupesDeze2.Controls.Add(this.mygtIsvalytiLanga);
            this.grupesDeze2.Controls.Add(this.mygtSpausdinti);
            this.grupesDeze2.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F);
            this.grupesDeze2.Location = new System.Drawing.Point(660, 161);
            this.grupesDeze2.Name = "grupesDeze2";
            this.grupesDeze2.Size = new System.Drawing.Size(298, 115);
            this.grupesDeze2.TabIndex = 17;
            this.grupesDeze2.TabStop = false;
            this.grupesDeze2.Text = "Spausdinimas";
            // 
            // mygtIsvalytiLanga
            // 
            this.mygtIsvalytiLanga.Location = new System.Drawing.Point(169, 33);
            this.mygtIsvalytiLanga.Name = "mygtIsvalytiLanga";
            this.mygtIsvalytiLanga.Size = new System.Drawing.Size(108, 58);
            this.mygtIsvalytiLanga.TabIndex = 1;
            this.mygtIsvalytiLanga.Text = "Išvalyti spausdinimo langa";
            this.mygtIsvalytiLanga.UseVisualStyleBackColor = true;
            this.mygtIsvalytiLanga.Click += new System.EventHandler(this.mygtIsvalytiLanga_Click);
            // 
            // mygtSpausdinti
            // 
            this.mygtSpausdinti.Location = new System.Drawing.Point(24, 33);
            this.mygtSpausdinti.Name = "mygtSpausdinti";
            this.mygtSpausdinti.Size = new System.Drawing.Size(96, 58);
            this.mygtSpausdinti.TabIndex = 0;
            this.mygtSpausdinti.Text = "Spausdinti duomenis";
            this.mygtSpausdinti.UseVisualStyleBackColor = true;
            this.mygtSpausdinti.Click += new System.EventHandler(this.mygtSpausdinti_Click);
            // 
            // platausTesktoDeze
            // 
            this.platausTesktoDeze.DataBindings.Add(new System.Windows.Forms.Binding("ReadOnly", global::lab1.Properties.Settings.Default, "ReadOnly", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.platausTesktoDeze.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.platausTesktoDeze.Location = new System.Drawing.Point(13, 27);
            this.platausTesktoDeze.Name = "platausTesktoDeze";
            this.platausTesktoDeze.ReadOnly = global::lab1.Properties.Settings.Default.ReadOnly;
            this.platausTesktoDeze.Size = new System.Drawing.Size(641, 468);
            this.platausTesktoDeze.TabIndex = 16;
            this.platausTesktoDeze.Text = "";
            // 
            // Forma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 519);
            this.Controls.Add(this.grupesDeze2);
            this.Controls.Add(this.platausTesktoDeze);
            this.Controls.Add(this.grupesDeze5);
            this.Controls.Add(this.grupesDeze4);
            this.Controls.Add(this.grupesDeze3);
            this.Controls.Add(this.grupesDeze1);
            this.Controls.Add(this.statusoJuosta);
            this.Controls.Add(this.meniuJuosta);
            this.Font = new System.Drawing.Font("Lucida Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.meniuJuosta;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Forma";
            this.Text = "Atvirukų skaičiuoklė";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusoJuosta.ResumeLayout(false);
            this.statusoJuosta.PerformLayout();
            this.meniuJuosta.ResumeLayout(false);
            this.meniuJuosta.PerformLayout();
            this.grupesDeze1.ResumeLayout(false);
            this.grupesDeze1.PerformLayout();
            this.grupesDeze3.ResumeLayout(false);
            this.grupesDeze4.ResumeLayout(false);
            this.grupesDeze5.ResumeLayout(false);
            this.grupesDeze2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusoJuosta;
        private System.Windows.Forms.Button mygtPasirinkti;
        private System.Windows.Forms.Button mygtIvesti;
        private System.Windows.Forms.Label tekstFailoPav;
        private System.Windows.Forms.MenuStrip meniuJuosta;
        private System.Windows.Forms.ToolStripMenuItem meniuKategorijaFailas;
        private System.Windows.Forms.ToolStripMenuItem kategUzdaryti;
        private System.Windows.Forms.GroupBox grupesDeze1;
        private System.Windows.Forms.ToolStripStatusLabel StatusoEtikete;
        private System.Windows.Forms.ToolStripStatusLabel statusoEtikete1;
        private System.Windows.Forms.GroupBox grupesDeze3;
        private System.Windows.Forms.Button mygtSpalvosRasti;
        private System.Windows.Forms.GroupBox grupesDeze4;
        private System.Windows.Forms.GroupBox grupesDeze5;
        private System.Windows.Forms.Button mygtRikiavimas;
        private System.Windows.Forms.Button mygtSalinti;
        private System.Windows.Forms.RichTextBox platausTesktoDeze;
        private System.Windows.Forms.GroupBox grupesDeze2;
        private System.Windows.Forms.Button mygtIsvalytiLanga;
        private System.Windows.Forms.Button mygtSpausdinti;
        private System.Windows.Forms.ComboBox pasirinkValstybes;
        private System.Windows.Forms.ToolStripMenuItem kategIssaugotiDuomenis;
        private System.Windows.Forms.ToolStripMenuItem kategAtvertiDuomenis;
    }
}

