﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    /// <summary>
    /// Mazgo klasė
    /// </summary>
    public sealed class Mazgas
    {
        public Atvirukas Duomenys { get; set; }
        public Mazgas Kitas { get; set; }

        public Mazgas()
        {

        }

        public Mazgas(Atvirukas duomenys, Mazgas adresas)
        {
            this.Duomenys = duomenys;
            this.Kitas = adresas;
        }
    }
}
