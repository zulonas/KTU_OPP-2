﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    /// <summary>
    /// Atvirkštinis sąrašas skirtas pradiniams duomenims talpinti
    /// </summary>
    public sealed class KAtvirukaiA
    {
        private Mazgas Pradzia;
        private Mazgas Galas;
        private Mazgas Darbinis;

        public KAtvirukaiA()
        {
            Galas = new Mazgas(new Atvirukas(), null);
            Pradzia = new Mazgas(new Atvirukas(), Galas);
            Darbinis = null;
        }

        /// <summary>
        /// Darbinis mazgas priskiriamas sekančiam elementui po pradinio
        /// </summary>
        public void Pradžia()
        {
            Darbinis = Pradzia.Kitas;
        }

        /// <summary>
        /// Sąsajai priskiriamas sąrašo sekantis elementas
        /// </summary>
        public void Kitas()
        {
            Darbinis = Darbinis.Kitas;
        }

        /// <summary>
        /// Grąžina true, jeigu sąsaja netuščia; false - priešingu atveju
        /// </summary>
        /// <returns></returns>
        public bool Yra()
        {
            return Darbinis.Kitas != null;
        }

        /// <summary>
        /// Grąžina pagalbinės rodyklės rodomo elemento reikšmę
        /// </summary>
        /// <returns></returns>
        public Atvirukas ImtiDuomenis() { return Darbinis.Duomenys; }

        /// <summary>
        /// Sukuriamas sąrašo elementas ir prijungiamas prie sąrašo PRADŽIOS
        /// </summary>
        /// <param name="inf"> naujo elemento reikšmė (duomenys)</param>
        public void DėtiDuomenis(Atvirukas inf)
        {
            Pradzia.Kitas = new Mazgas(inf, Pradzia.Kitas);
        }

        /// <summary>
        /// Randa spalvotų atvirukų kiekį kurie priklauso pasirinktai 
        /// saliai ir yra spalvoti
        /// </summary>
        /// <param name="salis"></param>
        /// <returns>ratų kiekis</returns>
        public int KiekisSpalvotu(string salis)
        {
            int galutinis = 0;
            for(Mazgas d1 = Pradzia.Kitas; d1.Kitas !=null; d1 = d1.Kitas)
            {
                if (d1.Duomenys.Valstybe == salis &&
                    d1.Duomenys.Spalvotas == true)
                    galutinis++;
            }
            return galutinis;
        }

        /// <summary>
        /// Metodas surandantis kiek yra spalvotų
        /// atvirukų iš pasirinktos šalies
        /// </summary>
        /// <param name="salis"></param>
        /// <returns></returns>
        public int GautiSpalvotuKieki(string salis)
        {
            int kiekis = 0;
            for (Mazgas d1 = Pradzia.Kitas; d1.Kitas != null; d1 = d1.Kitas)
            {
                if (d1.Duomenys.Valstybe == salis && d1.Duomenys.Spalvotas)
                    kiekis += d1.Duomenys.Kiekis;
            }
            if (kiekis == 0) return -1;
            return kiekis;
        }

        /// <summary>
        /// Motodas skirtas išvesti gražiai saraše sukauptas reikšmes
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string galutinis = "";
            galutinis += "|-------------+-------+-------+---------+---------" +
                "+--------+--------|\n";
            galutinis += "| Pavadinimas | Šalis | Metai |Spalvotas| Aukstis " +
                "| Plotis | Kiekis |\n";
            galutinis += "|-------------+-------+-------+---------+---------" +
                "+--------+--------|\n";
            for (Mazgas d1 = Pradzia.Kitas; d1.Kitas != null; d1 = d1.Kitas)
            {
                galutinis += d1.Duomenys;
            }
            if (Pradzia.Kitas == Galas) galutinis += "|                     " +
                    "      Sąrašas tuščias                         |\n";
            galutinis += "|-------------+-------+-------+---------+---------" +
                "+--------+--------|\n";
            return galutinis;
        }
    }
}
