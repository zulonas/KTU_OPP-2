﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace lab4
{
    /// <summary>
    /// Klasė aprašanti Atviruko parametrus
    /// </summary>
    public class Atvirukas
    {
        public string Pavadinimas { get; private set; }
        public string Valstybe { get; private set; }
        public DateTime Metai { get; private set; }
        public bool Spalvotas { get; private set; }
        public double Aukstis { get; private set; }
        public double Plotis { get; private set; }
        public int Kiekis { get; private set; }

        public Atvirukas()
        {
            Pavadinimas = "";
            Spalvotas = false;
            Aukstis = 0;
            Plotis = 0;
            Kiekis = 0;
            Valstybe = "NE";
            Metai = DateTime.MinValue;
        }

        /// <summary>
        /// Metodas skirtas prisikirti atviruko reiksmes
        /// </summary>
        /// <param name="pavadinimas"></param>
        /// <param name="salis"></param>
        /// <param name="metai"></param>
        /// <param name="spalvotas"></param>
        /// <param name="aukstis"></param>
        /// <param name="plotis"></param>
        /// <param name="kiekis"></param>
        public Atvirukas(string pavadinimas, string salis, DateTime metai, 
            bool spalvotas, double aukstis, double plotis, int kiekis)
        {
            this.Pavadinimas = pavadinimas;
            this.Valstybe = salis;
            this.Metai = metai;
            this.Spalvotas = spalvotas;
            this.Aukstis = aukstis;
            this.Plotis = plotis;
            this.Kiekis = kiekis;
        }

        /// <summary>
        /// Metodas skirtas gražinti gražiai formatuotą tekstą naudojamą 
        /// gražiam teksto atavaizdavimui
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string galutinis = "";
            string metai = (DateTime.Compare(this.Metai, DateTime.MinValue)
                != 0) ? this.Metai.ToString("yyyy") : "0";
            string spalvotas = (this.Spalvotas == true) ? "Taip" : "Ne";
            galutinis += string.Format("|{0, -13}| {1,-5} | {2, 5} |" +
                " {3,-7} | {4, 7} | {5, 6} | {6, 6} |\n", Pavadinimas,
                Valstybe, metai, spalvotas, Aukstis, Plotis, Kiekis);
            return galutinis;
        }

        /// <summary>
        /// Perklotas > operatorius
        /// </summary>
        /// <param name="atvir1"></param>
        /// <param name="atvir2"></param>
        /// <returns></returns>
        public static bool operator >(Atvirukas atvir1, Atvirukas atvir2)
        {
            int rezultatas = DateTime.Compare(atvir1.Metai, atvir2.Metai);
            return rezultatas > 0 || rezultatas == 0 && atvir1.Kiekis > atvir2.Kiekis;
        }

        /// <summary>
        /// Perklotas < operatorius
        /// </summary>
        /// <param name="atvir1"></param>
        /// <param name="atvir2"></param>
        /// <returns></returns>
        public static bool operator <(Atvirukas atvir1, Atvirukas atvir2)
        {
            int rezultatas = DateTime.Compare(atvir1.Metai, atvir2.Metai);
            return rezultatas < 0 || rezultatas == 0 && atvir1.Kiekis < atvir2.Kiekis;
        }
    }
}
