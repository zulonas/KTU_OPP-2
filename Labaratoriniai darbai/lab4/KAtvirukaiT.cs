﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    /// <summary>
    /// Tiesioginis sąrašas skirtas apdorotiems duomenims talpinti
    /// </summary>
    public sealed class KAtvirukaiT
    {

        private Mazgas Pradzia;
        private Mazgas Galas;
        private Mazgas FGalas;
        private Mazgas Darbinis;

        public KAtvirukaiT()
        {
            Galas = new Mazgas(new Atvirukas(), null);
            Pradzia = new Mazgas(new Atvirukas(), Galas);
            FGalas = Pradzia;
            Darbinis = null;
        }

        /// <summary>
        /// Darbinis mazgas priskiriamas sekančiam elementui po pradinio
        /// </summary>
        public void Pradžia()
        {
            Darbinis = Pradzia.Kitas;
        }

        /// <summary>
        /// Sąsajai priskiriamas sąrašo sekantis elementas
        /// </summary>
        public void Kitas()
        {
            Darbinis = Darbinis.Kitas;
        }

        /// <summary>
        /// Grąžina true, jeigu sąsaja netuščia; false - priešingu atveju
        /// </summary>
        /// <returns></returns>
        public bool Yra()
        {
            return Darbinis.Kitas != null;
        }

        /// <summary>
        /// Grąžina pagalbinės rodyklės rodomo elemento reikšmę
        /// </summary>
        /// <returns></returns>
        public Atvirukas ImtiDuomenis() { return Darbinis.Duomenys; }

        /// <summary>
        /// Sukuriamas sąrašo elementas ir prijungiamas prie sąrašo PRADŽIOS
        /// </summary>
        /// <param name="inf"> naujo elemento reikšmė (duomenys)</param>
        public void DėtiDuomenis(Atvirukas inf)
        {
            FGalas.Kitas = new Mazgas(inf, Galas);
            FGalas = FGalas.Kitas;
        }

        /// <summary>
        /// Rikiavimas burbuliuku
        /// </summary>
        public void Rikiuoti()
        {
            if (Pradzia.Kitas.Kitas == null) return; 
            bool keista = true;
            while (keista)
            {
                keista = false;
                Mazgas laik = Pradzia.Kitas;
                while (laik.Kitas.Kitas != null)
                {
                    if (laik.Duomenys > laik.Kitas.Duomenys)
                    {
                        Atvirukas keiciamas = laik.Kitas.Duomenys;
                        laik.Kitas.Duomenys = laik.Duomenys;
                        laik.Duomenys = keiciamas;
                        keista = true;
                    }
                    laik = laik.Kitas;
                }
            }
        }

        /// <summary>
        /// Metodas skirtas pašalinti mezgus/atvirukus
        /// kur metai yra nepriskirti
        /// </summary>
        public void SalintiNulinius()
        {
            Mazgas d1 = Pradzia;
            Mazgas galas = FGalas; // priskiriamai į 
            //prieš galą esantį fiktivų jeigu sąrašas 
            //būtų tuščias niekas nepasikeistų
            while (d1.Kitas != Galas)
            {
                
                if (ArPriskiti(d1.Kitas))
                {
                    d1 = d1.Kitas;
                    galas = d1;
                }
                else
                {
                    d1.Kitas = d1.Kitas.Kitas;
                }
            }
            FGalas = galas;
            FGalas.Kitas = Galas;
        }


        /// <summary>
        /// Metodas skirtas patikrinti ar pasirinkto 
        /// mazgo/atviruko metai yra priskirti
        /// </summary>
        /// <param name="mazgas"></param>
        /// <returns></returns>
        private bool ArPriskiti(Mazgas mazgas)
        {
            if (mazgas.Duomenys.Metai == DateTime.MinValue)
                return false;
            return true;
        }

        /// <summary>
        /// Išduoda sąrašo duomenis lentelėmis
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string galutinis = "";
            galutinis += "|-------------+-------+-------+---------+---------" +
                "+--------+--------|\n";
            galutinis += "| Pavadinimas | Šalis | Metai |Spalvotas| Aukstis " +
                "| Plotis | Kiekis |\n";
            galutinis += "|-------------+-------+-------+---------+---------" +
                "+--------+--------|\n";
            for (Mazgas d1 = Pradzia.Kitas; d1.Kitas != null; d1 = d1.Kitas)
            {
                galutinis += d1.Duomenys;
            }
            if (Pradzia.Kitas == Galas) galutinis += "|                     " +
                    "      Sąrašas tuščias                         |\n";
            galutinis += "|-------------+-------+-------+---------+---------" +
                "+--------+--------|\n";
            return galutinis;
        }
    }
}
