﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.IO;

namespace lab4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            toolStripStatusLabel1.Text = "";
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button5.Enabled = false;
            algoritmaiToolStripMenuItem.Enabled = false;
            spausdinimasToolStripMenuItem.Enabled = false;
            išsaugotiRezultatusToolStripMenuItem.Enabled = false;
        }

        OpenFileDialog failoPaieska = new OpenFileDialog();
        Kolekcionierius kolek1 = new Kolekcionierius();
        Kolekcionierius kolek2 = new Kolekcionierius();
        KAtvirukaiA AtvirukaiA1 = new KAtvirukaiA();
        KAtvirukaiA AtvirukaiA2 = new KAtvirukaiA();
        KAtvirukaiT AtvirukaidaugiauT1 = new KAtvirukaiT();
        KAtvirukaiT AtvirukaidaugiauT2 = new KAtvirukaiT();

        /// <summary>
        /// Metodas uždarantis programą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IšeitiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Metodas skirtas parodyti failą paiešką ir pasirinkti
        /// ivesties failą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {
            if (failoPaieska.ShowDialog() == DialogResult.OK)
            {
                button3.Enabled = true;
            }
        }

        /// <summary>
        /// Metodas skirtas nuskaityti pasirinkto failo duomenis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button3_Click(object sender, EventArgs e)
        {
            AtvirukaiA1 = SkaitytiDuomenis(failoPaieska.FileName, ref kolek1);
            toolStripStatusLabel1.Text = "Pirmo kelekcionieriaus failas" +
                " buvo nuskaitytas sėkmingai.";
            button1.Enabled = false;
            button3.Enabled = false;
            button2.Enabled = true;
        }

        /// <summary>
        /// Nuskaito failą ir gražina nuskaitytus duomenis
        /// </summary>
        /// <param name="failoKelias"></param>
        /// <param name="kolek"></param>
        /// <returns></returns>
        private KAtvirukaiA SkaitytiDuomenis(string failoKelias, 
            ref Kolekcionierius kolek)
        {
            KAtvirukaiA naujasSarasas = new KAtvirukaiA();
            using (StreamReader skaitytojas = new StreamReader(failoKelias))
            {
                string eilute = skaitytojas.ReadLine();
                //skaitomas headeris
                string[] dalys = eilute.Split(';');
                kolek.Deti(dalys[0], dalys[1]);
                while((eilute = skaitytojas.ReadLine()) != null)
                {
                    Atvirukas naujas = GautiAtviruka(eilute);
                    naujasSarasas.DėtiDuomenis(naujas);

                }
            }
            return naujasSarasas;
        }


        /// <summary>
        /// Metodas skirtas nuskaityti/paversti Atviruka/atviruku iš duotos
        /// duomenų eilutės
        /// </summary>
        /// <param name="eilute"></param>
        /// <returns></returns>
        private Atvirukas GautiAtviruka(string eilute)
        {
            string[] eilDalys = eilute.Split(';');
            string pavadinimas = eilDalys[0];
            string salis = eilDalys[1];
            if (!comboBox1.Items.Contains(salis) && salis!= "NE")
                comboBox1.Items.Add(salis);
            DateTime metai;
            if (!DateTime.TryParseExact(eilDalys[2], "yyyy",
                CultureInfo.CurrentCulture,
                DateTimeStyles.None, out metai))
            {
                metai = DateTime.MinValue;
            }
            bool spalvota = (eilDalys[3] == "S") ? true : false;
            double aukstis = GetDouble(eilDalys[4]);
            double plotis = GetDouble(eilDalys[5]);
            int kiekis = int.Parse(eilDalys[6]);
            return new Atvirukas(pavadinimas, salis, metai, spalvota, aukstis,
                plotis, kiekis);
        }

        /// <summary>
        /// Metodas skirtas paimti double reiksme bet kokiu formatu
        /// </summary>
        /// <param name="value">bandoma parse'inti double reikšmė</param>
        /// <returns>reikšmė kuria kompiuteris normaliai supranta</returns>
        private static double GetDouble(string reiksme)
        {
            double rezutatas;

            reiksme = reiksme.Replace(',', '.');
            if (!double.TryParse(reiksme, System.Globalization.
                NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"),
                out rezutatas))
            {
                rezutatas = -1;
                MessageBox.Show("Nepavyko nuskaityti double reikšmių");
            }

            return rezutatas;
        }

        /// <summary>
        /// Metodas skirtas pasirinkti įvesties failą iš kurio bus skaitomi
        /// duomenys
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, EventArgs e)
        {
            if (failoPaieska.ShowDialog() == DialogResult.OK)
            {
                button4.Enabled = true;
            }
        }

        /// <summary>
        ///  Metodas skirtas nuskaityti pasirinkto failo duomenis
        ///  po skaitymo įjungti programos veikimui reikalingus mygtukus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button4_Click(object sender, EventArgs e)
        {
            AtvirukaiA2 = SkaitytiDuomenis(failoPaieska.FileName, ref kolek2);
            toolStripStatusLabel1.Text = "Antro kelekcionieriaus failas" +
                 " buvo nuskaitytas sėkmingai.";
            button2.Enabled = false;
            button4.Enabled = false;
            algoritmaiToolStripMenuItem.Enabled = true;
            spausdinimasToolStripMenuItem.Enabled = true;
            button5.Enabled = true;
            išsaugotiRezultatusToolStripMenuItem.Enabled = true;
        }

        /// <summary>
        /// Metodas skirtas formos ekrane pateikti programoje sukauptus 
        /// duomenis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpausdintiEkraneToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            richTextBox1.Text += "\t\t\t\t"+ kolek1;
            richTextBox1.Text += "\nPradiniai duomenys\n";
            richTextBox1.Text += AtvirukaiA1;
            richTextBox1.Text += "\nDaugiau negu vienas\n";
            richTextBox1.Text += AtvirukaidaugiauT1;

            richTextBox1.Text += "\n\n\t\t\t\t" + kolek2;
            richTextBox1.Text += "\nPradiniai duomenys\n";
            richTextBox1.Text += AtvirukaiA2;
            richTextBox1.Text += "\nDaugiau negu vienas\n";
            richTextBox1.Text += AtvirukaidaugiauT2;
            richTextBox1.Text += "\n";
            toolStripStatusLabel1.Text = "";
        }
        
        /// <summary>
        /// Metodas skirtas suformuoti atvirukų kurių kolekcionieriai
        /// turi daugiau negu po vieną
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormuotiDaugiauNegu1ToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            AtvirukaidaugiauT1 = FormuotiAtvirkstini(AtvirukaiA1);
            AtvirukaidaugiauT2 = FormuotiAtvirkstini(AtvirukaiA2);
            toolStripStatusLabel1.Text = "Suformuoti sąrašai";
        }


        /// <summary>
        /// Metodas formuojantis sąrašą atvirukų kurių yra daugiau negu vienas
        /// </summary>
        /// <param name="pirminis"></param>
        /// <returns></returns>
        private KAtvirukaiT FormuotiAtvirkstini(KAtvirukaiA pirminis)
        {
            KAtvirukaiT naujas = new KAtvirukaiT();
            for (pirminis.Pradžia(); pirminis.Yra(); pirminis.Kitas())
            {
                if (pirminis.ImtiDuomenis().Kiekis > 1)
                {
                    naujas.DėtiDuomenis(pirminis.ImtiDuomenis());
                }
            }
            return naujas;
        }

        /// <summary>
        /// Metodas skirta išvalyti programos veikimo langą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ValytiEkranąToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            richTextBox1.Text = "";
        }

        /// <summary>
        /// Metodas skirtas atrastį kolekcionierių kuris turi daugiausiai
        /// pasitinktos valstybės atviruku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button5_Click(object sender, EventArgs e)
        {
            string salis = comboBox1.GetItemText(comboBox1.SelectedItem);
            int pirmas = AtvirukaiA1.GautiSpalvotuKieki(salis);
            int antras = AtvirukaiA2.GautiSpalvotuKieki(salis);

            if (pirmas > antras)
                MessageBox.Show($"Daugiausia spalvotu turi {kolek1} " +
                    $"- {pirmas}");
            else if (pirmas < antras)
                MessageBox.Show($"Daugiausia spalvotu turi {kolek2} " +
                    $"- {antras}");
            else if (pirmas == -1 && antras == -1)
                MessageBox.Show($"Tokio kolekcionieriaus nėra");
            if (pirmas == antras)
                MessageBox.Show($"Visi turi po vienodą kiekį - {pirmas}");
        }

        /// <summary>
        /// Metodas surikiuojantis kolekcionierių atvirukus kurių yra
        /// daugiau negu vienas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RIkiuotiToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            AtvirukaidaugiauT1.Rikiuoti();
            AtvirukaidaugiauT2.Rikiuoti();
        }

        /// <summary>
        /// Metodas pašalinantis nulines reikšmes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ŠalintiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AtvirukaidaugiauT1.SalintiNulinius();
            AtvirukaidaugiauT2.SalintiNulinius();
        }

        /// <summary>
        /// Metodas išsaugantis programos atliekamus rezultatus faile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IšsaugotiRezultatusToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            SaveFileDialog rasytojas = new SaveFileDialog();
            rasytojas.Filter = "Teksto failas|*.txt";
            rasytojas.FileName = "rezultatai";
            rasytojas.Title = "Išsaugoti teksto failą";
            if (rasytojas.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string kelias = rasytojas.FileName;
                using (StreamWriter teksRasytojas = new
                    StreamWriter(File.Create(kelias)))
                {
                    teksRasytojas.Write(richTextBox1.Text);
                }
            }
        }
    }
}
