﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    /// <summary>
    /// Kolekcionieriaus asmeninė informacija
    /// </summary>
    class Kolekcionierius
    {
        public string Vardas { get; private set; }
        public string Pavarde { get; private set; }

        public Kolekcionierius()
        {
            Vardas = "";
            Pavarde = "";
        }

        /// <summary>
        /// Metodas skirtas priskiti vardui ir pavardei
        /// reikšmes
        /// </summary>
        /// <param name="vardas"></param>
        /// <param name="pavarde"></param>
        public void Deti(string vardas, string pavarde)
        {
            this.Vardas = vardas;
            this.Pavarde = pavarde;
        }

        /// <summary>
        /// Metodas skirtas gražinti gražiai formatuotą tekstą naudojamą 
        /// gražiam teksto atavaizdavimui
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string galutinis = "";
            galutinis += Vardas + " " + Pavarde;
            return galutinis;
        }
    }
}
