﻿namespace lab4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.failasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.išeitiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.algoritmaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formuotiDaugiauNegu1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rIkiuotiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.šalintiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spausdinimasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spausdintiEkraneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.valytiEkranąToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.išsaugotiRezultatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.failasToolStripMenuItem,
            this.algoritmaiToolStripMenuItem,
            this.spausdinimasToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(874, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // failasToolStripMenuItem
            // 
            this.failasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.išsaugotiRezultatusToolStripMenuItem,
            this.išeitiToolStripMenuItem});
            this.failasToolStripMenuItem.Name = "failasToolStripMenuItem";
            this.failasToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.failasToolStripMenuItem.Text = "Failas";
            // 
            // išeitiToolStripMenuItem
            // 
            this.išeitiToolStripMenuItem.Name = "išeitiToolStripMenuItem";
            this.išeitiToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.išeitiToolStripMenuItem.Text = "Išeiti";
            this.išeitiToolStripMenuItem.Click += new System.EventHandler(this.IšeitiToolStripMenuItem_Click);
            // 
            // algoritmaiToolStripMenuItem
            // 
            this.algoritmaiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formuotiDaugiauNegu1ToolStripMenuItem,
            this.rIkiuotiToolStripMenuItem,
            this.šalintiToolStripMenuItem});
            this.algoritmaiToolStripMenuItem.Name = "algoritmaiToolStripMenuItem";
            this.algoritmaiToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.algoritmaiToolStripMenuItem.Text = "Algoritmai";
            // 
            // formuotiDaugiauNegu1ToolStripMenuItem
            // 
            this.formuotiDaugiauNegu1ToolStripMenuItem.Name = "formuotiDaugiauNegu1ToolStripMenuItem";
            this.formuotiDaugiauNegu1ToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.formuotiDaugiauNegu1ToolStripMenuItem.Text = "Formuoti daugiau negu 1";
            this.formuotiDaugiauNegu1ToolStripMenuItem.Click += new System.EventHandler(this.FormuotiDaugiauNegu1ToolStripMenuItem_Click);
            // 
            // rIkiuotiToolStripMenuItem
            // 
            this.rIkiuotiToolStripMenuItem.Name = "rIkiuotiToolStripMenuItem";
            this.rIkiuotiToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.rIkiuotiToolStripMenuItem.Text = "Rikiuoti";
            this.rIkiuotiToolStripMenuItem.Click += new System.EventHandler(this.RIkiuotiToolStripMenuItem_Click);
            // 
            // šalintiToolStripMenuItem
            // 
            this.šalintiToolStripMenuItem.Name = "šalintiToolStripMenuItem";
            this.šalintiToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.šalintiToolStripMenuItem.Text = "Šalinti nežinomus metus";
            this.šalintiToolStripMenuItem.Click += new System.EventHandler(this.ŠalintiToolStripMenuItem_Click);
            // 
            // spausdinimasToolStripMenuItem
            // 
            this.spausdinimasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spausdintiEkraneToolStripMenuItem,
            this.valytiEkranąToolStripMenuItem});
            this.spausdinimasToolStripMenuItem.Name = "spausdinimasToolStripMenuItem";
            this.spausdinimasToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.spausdinimasToolStripMenuItem.Text = "Spausdinimas";
            // 
            // spausdintiEkraneToolStripMenuItem
            // 
            this.spausdintiEkraneToolStripMenuItem.Name = "spausdintiEkraneToolStripMenuItem";
            this.spausdintiEkraneToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.spausdintiEkraneToolStripMenuItem.Text = "Spausdinti ekrane";
            this.spausdintiEkraneToolStripMenuItem.Click += new System.EventHandler(this.SpausdintiEkraneToolStripMenuItem_Click);
            // 
            // valytiEkranąToolStripMenuItem
            // 
            this.valytiEkranąToolStripMenuItem.Name = "valytiEkranąToolStripMenuItem";
            this.valytiEkranąToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.valytiEkranąToolStripMenuItem.Text = "Valyti ekraną";
            this.valytiEkranąToolStripMenuItem.Click += new System.EventHandler(this.ValytiEkranąToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(874, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.richTextBox1.Location = new System.Drawing.Point(12, 28);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(578, 397);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(596, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(270, 124);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Skaityti duomenis";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(154, 90);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(110, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "Įrašyti";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 90);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(114, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Įrašyti";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(154, 55);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Pasirinkti";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Pasirinkti";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(163, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Kolekcionierius2";
            //this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kolekcionierius1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Location = new System.Drawing.Point(596, 157);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(270, 72);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rasti turinti daugiausiai spalvotų";
            //this.groupBox2.Enter += new System.EventHandler(this.GroupBox2_Enter);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(166, 27);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(98, 23);
            this.button5.TabIndex = 1;
            this.button5.Text = "Ieškoti";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(6, 29);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(140, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // išsaugotiRezultatusToolStripMenuItem
            // 
            this.išsaugotiRezultatusToolStripMenuItem.Name = "išsaugotiRezultatusToolStripMenuItem";
            this.išsaugotiRezultatusToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.išsaugotiRezultatusToolStripMenuItem.Text = "Išsaugoti rezultatus";
            this.išsaugotiRezultatusToolStripMenuItem.Click += new System.EventHandler(this.IšsaugotiRezultatusToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 450);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolStripMenuItem failasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem išeitiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem algoritmaiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formuotiDaugiauNegu1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rIkiuotiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem šalintiToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem spausdinimasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spausdintiEkraneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem valytiEkranąToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem išsaugotiRezultatusToolStripMenuItem;
    }
}

