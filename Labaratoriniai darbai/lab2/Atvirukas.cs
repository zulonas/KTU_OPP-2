﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace lab2
{
    /// <summary>
    /// Klasė aprašanti Atviruko parametrus
    /// </summary>
    class Atvirukas
    {
        public string Pavadinimas { get; private set; }
        public string Valstybe { get; private set; }
        public DateTime Metai { get; private set; }
        public bool Spalvotas { get; private set; }
        public double Aukstis { get; private set; }
        public double Plotis { get; private set; }
        public int Kiekis { get; private set; }

        public Atvirukas()
        {
            Pavadinimas = "";
            Spalvotas = false;
            Aukstis = 0;
            Plotis = 0;
            Kiekis = 0;
            Valstybe = "NE";
            Metai = DateTime.MinValue;
        }

        /// <summary>
        /// Metodas skirtas prisikirti atviruko reiksmes
        /// </summary>
        /// <param name="pavadinimas"></param>
        /// <param name="salis"></param>
        /// <param name="metai"></param>
        /// <param name="spalvotas"></param>
        /// <param name="aukstis"></param>
        /// <param name="plotis"></param>
        /// <param name="kiekis"></param>
        public void Deti(string pavadinimas, string salis, DateTime metai,
            bool spalvotas, double aukstis, double plotis, int kiekis)
        {
            this.Pavadinimas = pavadinimas;
            this.Valstybe = salis;
            this.Metai = metai;
            this.Spalvotas = spalvotas;
            this.Aukstis = aukstis;
            this.Plotis = plotis;
            this.Kiekis = kiekis;
        }

        /// <summary>
        /// Metodas skirtas gražinti gražiai formatuotą tekstą naudojamą 
        /// gražiam teksto atavaizdavimui
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string galutinis = "";
            string metai = (DateTime.Compare(this.Metai, DateTime.MinValue)
                != 0) ? this.Metai.ToString("yyyy") : "0";
            string spalvotas = (this.Spalvotas == true) ? "Taip" : "Ne";
            galutinis += string.Format(" {0, -13}   {1,-4}    {2, 6}    " +
                "  {3,-7}    {4, 5}     {5, 5}   {6, 4}     ", Pavadinimas,
                Valstybe, metai, spalvotas, Aukstis, Plotis, Kiekis);
            return galutinis;
        }

        /// <summary>
        /// Metodas skirtas papildyti Atviručių kiekį
        /// </summary>
        /// <param name="kiekis"></param>
        public void PapildytiKieki(int kiekis)
        {
            this.Kiekis += kiekis;
        }

        /// <summary>
        /// Perdengimas skirtas palygintu 2 atvuku objektus
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator ==(Atvirukas pirmas, Atvirukas antras)
        {
            return (pirmas.Pavadinimas == antras.Pavadinimas &&
                pirmas.Aukstis == antras.Aukstis &&
                pirmas.Valstybe == antras.Valstybe &&
                pirmas.Metai == antras.Metai &&
                pirmas.Plotis == antras.Plotis &&
                pirmas.Spalvotas == antras.Spalvotas) ? true : false;
        }

        /// <summary>
        /// Perdengimas skirtas palygintu 2 atvuku objektus
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator !=(Atvirukas pirmas, Atvirukas antras)
        {
            return (pirmas.Pavadinimas != antras.Pavadinimas ||
                pirmas.Aukstis != antras.Aukstis ||
                pirmas.Valstybe != antras.Valstybe ||
                pirmas.Metai != antras.Metai ||
                pirmas.Plotis != antras.Plotis ||
                pirmas.Spalvotas != antras.Spalvotas) ? true : false;
        }

        /// <summary>
        /// Perdendiamas operatorius > lyginantis kiekis
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator >(Atvirukas pirmas, Atvirukas antras)
        {
            return (pirmas.Kiekis > antras.Kiekis) ? true : false; 
        }

        /// <summary>
        /// Perdendiamas operatorius < lyginantis kiekis
        /// </summary>
        /// <param name="pirmas"></param>
        /// <param name="antras"></param>
        /// <returns></returns>
        public static bool operator <(Atvirukas pirmas, Atvirukas antras)
        {
            return (pirmas.Kiekis < antras.Kiekis) ? true : false;
        }

    }
}
