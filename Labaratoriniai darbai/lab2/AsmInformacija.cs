﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Kolekcionieriaus asmeninė informacija
    /// </summary>
    class AsmInformacija
    {
        public string vardas { get; private set; }
        public string pavarde { get; private set; }

        public AsmInformacija()
        {
            vardas = string.Empty;
            pavarde = string.Empty;
        }

        /// <summary>
        /// Metodas skirtas priskiti vardui ir pavardei
        /// reikšmes
        /// </summary>
        /// <param name="vardas"></param>
        /// <param name="pavarde"></param>
        public void Deti(string vardas, string pavarde)
        {
            this.vardas = vardas;
            this.pavarde = pavarde;
        }

        /// <summary>
        /// Metodas skirtas gražinti gražiai formatuotą tekstą naudojamą 
        /// gražiam teksto atavaizdavimui
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string galutinis = "";
            galutinis += string.Format("               {0, -19}" +
                "                {1,-18}       \n", vardas, pavarde);
            return galutinis;
        }
    }
}
