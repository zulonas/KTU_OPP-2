﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Klasė aprašanti kolekcionieriaus informciją
    /// </summary>
    class Kolekcionierius
    {
        public List<Atvirukas> Atvirukai { get; private set; }
        public List<Atvirukas> AtvirukaiVirsVieno { get; private set; }
        public AsmInformacija Informacija { get; private set; }
        
        public Kolekcionierius()
        {
            Informacija = new AsmInformacija();
            Atvirukai = new List<Atvirukas>();
            AtvirukaiVirsVieno = new List<Atvirukas>();
        }

        /// <summary>
        /// Metodas skirtas gauti atviruką
        /// </summary>
        /// <param name="indeksas"></param>
        /// <returns></returns>
        public Atvirukas GautiAtviruka(int indeksas)
        {
            return Atvirukai[indeksas];
        }

        /// <summary>
        /// Metodas skirtas gauti atviruką kurių yra 
        /// virš vieno
        /// </summary>
        /// <param name="indeksas"></param>
        /// <returns></returns>
        public Atvirukas GautiAtvirukaVirsVieno(int indeksas)
        {
            return AtvirukaiVirsVieno[indeksas];
        }

        /// <summary>
        /// Metodas skirtas pridėti naują atviruko objektą į atvirukų sąrašą
        /// </summary>
        /// <param name="naujas"></param>
        public void DetiAtviruka(Atvirukas naujas)
        {
            if(naujas.Kiekis >0)
                Atvirukai.Add(naujas);
        }

        /// <summary>
        /// Metodas skirtas pridėti naują atviruko objektą į atviruką sąrašą
        /// kur jų yra daugiau negu vienas
        /// </summary>
        /// <param name="naujas"></param>
        public void DetiAtvirukaDaugVieno(Atvirukas naujas)
        {
            Atvirukai.Add(naujas);
        }

        /// <summary>
        /// Metododas skirtas priskirti naujus asmens duomenis
        /// </summary>
        /// <param name="nauja"></param>
        public void DetiAsmDuomenis(AsmInformacija nauja)
        {
            Informacija = nauja;
        }

        /// <summary>
        /// Metodas skirtas iškiruoti AtvirukaiVirsVieno pagal metus
        /// didėjančia tvarka
        /// </summary>
        public void RikiuotiAtvirukus()
        {
            Atvirukai =
                Atvirukai.OrderBy(x => x.Metai).ToList();
            AtvirukaiVirsVieno = 
                AtvirukaiVirsVieno.OrderBy(x => x.Metai).ToList();
        }


        /// <summary>
        ///  Metodas skirtas gražinanti suformatuotą tekstą 
        /// </summary>
        /// <param name="daugiauneguvienas"> numatytoji nuostata spausdina
        /// visus atvirukus, jeigu ji pakeičiama į True, tada spausdinami tik
        /// Atvirukai kuriu yra daugiau negu 1</param>
        /// <returns>suformatuotas tekstas</returns>
        public string ToString(bool daugiauneguvienas
            = false)
        {
            string galutinis = "";
            galutinis += "\n\n";
            galutinis += Informacija.ToString();
            galutinis += "--------------------------------------------" +
                "-------------------------------\n";
            galutinis += "| Pavadinimas | Šalis | Išleidimo" +
                " | Spalvotas | Plotis | Aukštis | Kiekis |\n";
            galutinis += "|             |       |    " +
                "data   |           |        |         |        |\n";
            galutinis += "-------------------------------------" +
                "--------------------------------------\n";
            if (daugiauneguvienas) {
                foreach(Atvirukas atvirurukas in AtvirukaiVirsVieno)
                {
                    galutinis += atvirurukas.ToString() + "\n";
                }
                galutinis+=((AtvirukaiVirsVieno.Count == 0) ? "             " +
                    "                 Duomenu nera\n" : "");
            }
            else
            {
                for (int x = 0; x < Atvirukai.Count; x++)
                {
                    galutinis += Atvirukai[x].ToString() + "\n";
                }
                galutinis+=((Atvirukai.Count == 0) ? "             " +
                    "                 Duomenu nera\n" : "");
            }
            galutinis += "|--------------------------------------------" +
                "-----------------------------|";
            return galutinis;
        }

        /// <summary>
        /// Tikrina kiek kolekcionierius turi spalvoju atviručių
        /// </summary>
        /// <param name="valstybe">valstybė pagal kurią ieškoma
        /// atviručių</param>
        /// <returns>spalvotų atviričių kiekis</returns>
        public int KiekisSpalvotu(string valstybe) 
        {
            return (int)Atvirukai.FindAll(x => (x.Valstybe == valstybe) 
            && (x.Spalvotas)).Count;
        } 

        /// <summary>
        /// Pašalina visus metų neturinčius atvirukus
        /// </summary>
        public void SalintiNuliniusVirsVieno()
        {
            Atvirukai.RemoveAll(e => e.Metai == DateTime.MinValue);
            AtvirukaiVirsVieno.RemoveAll(e => e.Metai == DateTime.MinValue);
        } 

        //tikrina ar sarašas surikiuotas
        public bool ArSurikiuota()
        {
            var tikimasi1 = Atvirukai.OrderBy(x => x.Metai).ToList();
            var tikimasi2 = AtvirukaiVirsVieno.OrderBy(x => x.Metai).ToList();
            return Atvirukai.SequenceEqual(tikimasi1) &&
                AtvirukaiVirsVieno.SequenceEqual(tikimasi2);
        }
    }
}
