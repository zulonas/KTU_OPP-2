﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace lab2
{
    /// <summary>
    /// Klasė aprašanti Šalis
    /// </summary>
    class Salis
    {
        public string pavadinimas {get; private set;}

        public Salis()
        {
            pavadinimas = string.Empty;
        }

        /// <summary>
        /// Metodas skirtas priskirti Šalie paramentrų reikšmes
        /// </summary>
        /// <param name="naujaSalis">Naujos šalies pavadinimas</param>
        public void Deti(string naujaSalis)
        {
            pavadinimas = naujaSalis;
        }
    }
}
