using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    /// <summary>
    /// Klasė aprašanti kolekcionierių informciją
    /// </summary>
    class Kolekcionieriai
    {
        const int CKiekis = 10;
        const int CSaliuKiekis = 100;
        private KAtvirukai[] kAtvirukai; // visi atvirukai
        private KAtvirukai[] kAtvirukaiDaugiugiau; //daugiau kaip vienas
        //egzemplempiorius
        private Kolekcionierius[] kolekcionierius; //Kolekcionierių asmeniniai
        //duomenys
        public int kolekcKiekis { get; private set; }
        private string[] saliuSarasas = new string[CSaliuKiekis];
        public int saliuKiekis = 0;


        public Kolekcionieriai()
        {
            kolekcKiekis = 0;
            kAtvirukai = new KAtvirukai[CKiekis];
            kAtvirukaiDaugiugiau = new KAtvirukai[CKiekis];
            kolekcionierius = new Kolekcionierius[CKiekis];
        }

        /// <summary>
        /// Metodas pagal indeksą gražinantis pasirinktą kolekcionierių
        /// konteinerį
        /// </summary>
        /// <param name="n">norimo kolekcionieriaus indeksas</param>
        /// <returns>kolekcionieriaus objektas</returns>
        public KAtvirukai ImtiKolekcionieriu(int n)
        {
            return kAtvirukai[n];
        }

        /// <summary>
        /// Metodas pagal indeksą gražinantis pasirinktą kolekcionierių
        /// konteinerį
        /// </summary>
        /// <param name="n">norimo kolekcionieriaus indeksas</param>
        /// <returns>kolekcionieriaus objektas</returns>
        public KAtvirukai ImtiKolekcionieriuDaugiau(int n)
        {
            return kAtvirukaiDaugiugiau[n];
        }

        /// <summary>
        /// Metodas pagal indeksą gražinantis pasirinkto kolekcionieriaus
        /// asmeninę informaciją 
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public Kolekcionierius ImtiKolekcionieriausDuom(int n)
        {
            return kolekcionierius[n];
        }

        /// <summary>
        /// Metodas pridedantis naujus objektus prie esamų konteinerių
        /// </summary>
        /// <param name="atviruk">Atviruku informacija</param>
        /// <param name="asmDuom">Kolekkcionieriaus informacija</param>
        public void DetiKolekcionieriu(KAtvirukai atviruk,
            Kolekcionierius asmDuom)
        {
            skaitytiSalis(atviruk);
            kolekcionierius[kolekcKiekis] = asmDuom;
            kAtvirukai[kolekcKiekis] = atviruk;
            //pridedama į skirtingą konteinerį jeigu daugiau negu 1
            KAtvirukai atvirukai = new KAtvirukai();
            for (int x = 0; x < atviruk.AtvirukuKiek; x++)
            {
                if (atviruk.ImtiAtviruka(x).kiekis > 1)
                {
                    atvirukai.DetiAtviruka(atviruk.ImtiAtviruka(x));
                }
            }
            kAtvirukaiDaugiugiau[kolekcKiekis] = atvirukai;
            kolekcKiekis++;
        }

        /// <summary>
        /// Randa zmogu kuris turi daugiausiai pasirinktos valstybes 
        /// spalvotų atvirukų.
        /// </summary>
        /// <param name="valstybe">Pasirinkta valstybė</param>
        /// <returns>Rasto žmogaus vardas ir pavardė</returns>
        public string GautiSpalvotuTuretoja(string valstybe)
        {
            ///pataisyti su rikiavimu
            int daugiausiaReikme = 0;
            for(int x=0; x<kolekcKiekis; x++)
            {
                if (ImtiKolekcionieriu(x).GautiKiekiSpalvotu(valstybe)
                    > daugiausiaReikme)
                {
                    daugiausiaReikme = 
                        ImtiKolekcionieriu(x).GautiKiekiSpalvotu(valstybe);
                }
            }

            if (daugiausiaReikme != 0)
            {
                string galutinis = "";
                for (int x = 0; x < kolekcKiekis; x++) {
                    if (ImtiKolekcionieriu(x).GautiKiekiSpalvotu(valstybe)
                        == daugiausiaReikme)
                    {
                        galutinis += ImtiKolekcionieriausDuom(x).vardas + " " +
                            ImtiKolekcionieriausDuom(x).pavarde;
                    }
                    galutinis += " ";
                }
                return galutinis;
            }
            return "";
        }

        /// <summary>
        /// Metodas nuskaitantis turimas salis į masyą
        /// </summary>
        private void skaitytiSalis(KAtvirukai naujas)
        {
            for (int x = 0; x < naujas.AtvirukuKiek; x++)
            {
                if (!saliuSarasas.Contains(naujas.ImtiAtviruka(x).valstybe)
                    && naujas.ImtiAtviruka(x).valstybe != "NE")
                {
                    saliuSarasas[saliuKiekis++] =
                        naujas.ImtiAtviruka(x).valstybe;
                }
            }
        }

        /// <summary>
        /// Metodas gražinantas turimu šaliu sąrašą.
        /// </summary>
        /// <returns></returns>
        public string[] GautiSaliuSarasa()
        {
            return saliuSarasas;
        }

        /// <summary>
        /// Metodas skirtas gražinanti gražiai suformatuotą tekstą iš 
        /// visų atvirukų duomenų
        /// </summary>
        /// <returns></returns>
        public string tekstasFormatuotas()
        {
            string galutinis = "";
            galutinis += "\n                   INFORMACIJA APIE VISUS" +
                " ATVIRUKUS                         ";
            galutinis += "\n\n";
            
            for (int x = 0; x < kolekcKiekis; x++)
            {
                galutinis += ImtiKolekcionieriausDuom(x).ToString();
                galutinis += ImtiKolekcionieriu(x).ToString() + "\n";
            }
            return galutinis;
        }

        /// <summary>
        /// Metodas skirtas gražinanti gražiai suformatuotą tekstą iš 
        /// atvirukų kurių kiekis daugiau nei vienas
        /// </summary>
        /// <returns></returns>
        public string tekstasFormatuotasDaugiau()
        {
            string galutinis = "";
            galutinis += "\n                ATVIRUKAI KURIŲ DAUGIAU NEGU " +
                "VIENAS                         ";
            galutinis += "\n\n";
            for (int x = 0; x < kolekcKiekis; x++)
            {
                galutinis += ImtiKolekcionieriausDuom(x).ToString();
                galutinis += ImtiKolekcionieriuDaugiau(x).ToString() + "\n";
            }
            return galutinis;
        }
    }
}
