﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Globalization;

namespace lab2
{
    public partial class Forma : Form
    {
        OpenFileDialog failoPaieska = new OpenFileDialog();
        private List<Kolekcionierius> Kolekcionieriai = 
            new List<Kolekcionierius>();
        private  List<Salis> SaliuSarasas = new List<Salis>();

        public Forma()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Kai mygtukas mygtPasirinkti paspaudžiamas atlkiekama failo paieškos
        /// sistema
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mygtPasirinkti_Click(object sender, EventArgs e)
        {
            if(failoPaieska.ShowDialog() == DialogResult.OK)
            {
                tekstFailoPav.Text = failoPaieska.SafeFileName;
                mygtIvesti.Enabled = true;
                iterptiIRikiuota.Enabled = true;
            }
        }

        /// <summary>
        /// Kai mygtukas mygtIvesti paspaudžia aktyvuojamas duomenų
        /// įvedimas 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mygtIvesti_Click(object sender, EventArgs e)
        {
            try
            {
                if(failoPaieska.FileName!="")
                    SkaitytiKolekcFaila(Kolekcionieriai, failoPaieska.FileName);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                MessageBox.Show("Nepavyko nuskaityti visų failo duomenų",
                    "Klaida");
                return;
            }
            finally
            {
                iterptiIRikiuota.Enabled = false;
                mygtIvesti.Enabled = false;
                tekstFailoPav.Text = "failas nepasirinktas";
            }
            statusoEtikete1.Text = "Failas nuskaitytas sėkmingai";
            //Įjungiami mygtukai
            spausdintiDuomenisToolStripMenuItem.Enabled = true;
            isvalytiSpausdinimoLangaToolStripMenuItem.Enabled = true;
            pasirinkValstybes.Enabled = true;
            mygtSpalvosRasti.Enabled = true;
            rikiavimasToolStripMenuItem.Enabled = true;
            salinimasBeDatosToolStripMenuItem.Enabled = true;
            kategIssaugotiDuomenis.Enabled = true;
            atnaujintiSalisToolStripMenuItem.Enabled = true;
            įrašytiKaipCSVToolStripMenuItem.Enabled = true;
            tikrintiArRikiuotasToolStripMenuItem.Enabled = true;
            failoPaieska.Reset(); 
        }

        /// <summary>
        /// Nuskaito duomenų failą su duotu failo keliu, ir duomenis įveda į
        /// Kolekcionieriai objektą
        /// </summary>
        /// <param name="KolekcionKont"></param>
        /// <param name="failoKelias"></param>
        private void SkaitytiKolekcFaila(List<Kolekcionierius> Kolekcionieriai,
            string failoKelias, bool rikiuotas=false)
        {
            using (StreamReader srautas = new StreamReader(failoKelias,
                Encoding.GetEncoding("utf-8")))
            {
                string eilute;
                while ((eilute = srautas.ReadLine()) != null)
                {
                    string[] eilDalys = eilute.Split(' ');
                    Kolekcionierius kolekcionierius = new Kolekcionierius();

                    //Asmeninė informacija
                    AsmInformacija informacija = new AsmInformacija();
                    informacija.Deti(eilDalys[0], eilDalys[1]);
                    kolekcionierius.DetiAsmDuomenis(informacija);

                    //Tikrinimas ar yra eilutė ne tuščia, jeigu taip tada
                    //skaitymas tesiamas
                    while (!string.IsNullOrEmpty(eilute = srautas.ReadLine()))
                    {
                        //sujungiami vineodi
                        Atvirukas naujas = GautiAtviruka(eilute);
                        bool prideta = false;
                        foreach (Atvirukas atvir in kolekcionierius.Atvirukai)
                        {
                            if (naujas == atvir)
                            {
                                atvir.PapildytiKieki(naujas.Kiekis);
                                prideta = true;
                                break;
                            }
                        }
                        if(prideta == false )
                        kolekcionierius.DetiAtviruka(naujas);
                    }

                    //tikrinimas ar yra toks pat kolekcionierius
                    int vienodasIndeksas = Kolekcionieriai.FindIndex(
                        a => (a.Informacija.vardas == eilDalys[0]) &&
                        (a.Informacija.pavarde == eilDalys[1])
                    );

                    if (vienodasIndeksas != -1) //vykdymas sujungimas 
                    {
                        if (rikiuotas == true && ArSurikiuotaVirsVieno())
                        //surastas vienodas sąrašas rikiuotas
                        {
                            RikiutuSarasuSujungimas(Kolekcionieriai
                                [vienodasIndeksas].Atvirukai,
                                kolekcionierius.Atvirukai);
                        }
                        else //jeigu surastas vienodas sarašas nerikiuotas
                        {
                            SarasuSujungimas(Kolekcionieriai[vienodasIndeksas].
                                Atvirukai, kolekcionierius.Atvirukai);
                        }
                        Kolekcionieriai[vienodasIndeksas].
                             AtvirukaiVirsVieno.Clear();
                        Kolekcionieriai[vienodasIndeksas].
                            AtvirukaiVirsVieno.AddRange(
                                Kolekcionieriai[vienodasIndeksas].Atvirukai.
                                    Where(a => a.Kiekis > 1).ToList());
                    }
                    else //pridedamas naujas
                    {
                        if (rikiuotas == true) //kai norime rikiuoti nors
                            //panašaus nėra
                        {
                            kolekcionierius.RikiuotiAtvirukus();
                        }
                        kolekcionierius.AtvirukaiVirsVieno.AddRange
                            (kolekcionierius.Atvirukai.
                            Where(a => a.Kiekis > 1).
                            OrderBy(a => a.Metai).ToList());
                        // jeigu tokio kolekcionieriaus nėra pridedamas naujas
                        Kolekcionieriai.Add(kolekcionierius);
                    }
                }
            }
            AtnaujintiSalis();
        }

        
        /// <summary>
        /// Metodas nuskaitantis eilute ir paverčiatis į Atviruko
        /// objektą
        /// </summary>
        /// <param name="eilute">eilute su Atviruko duomenimis</param>
        /// <returns>Atviruko objektas</returns>
        private Atvirukas GautiAtviruka(string eilute)
        {
            string[] eilDalys = eilute.Split(' ');
            string pavadinimas = eilDalys[0].Replace("\t", "").Trim();
            string salis = eilDalys[1].Trim();
            //pridedama šalis prie šalių sąrašo
            if (!SaliuSarasas.Exists(x => x.pavadinimas == salis))
            {
                Salis naujaSalis = new Salis();
                naujaSalis.Deti(salis);
                SaliuSarasas.Add(naujaSalis);
            }
            DateTime metai;
            //pridedami metai jeigu užrašyti
            if (!DateTime.TryParseExact(eilDalys[2], "yyyy",
                CultureInfo.CurrentCulture,
                DateTimeStyles.None, out metai))
            {
                metai = DateTime.MinValue;
            }
            bool spalvota = (eilDalys[3] == "S") ? true : false;
            double aukstis = GetDouble(eilDalys[4]);
            double plotis = GetDouble(eilDalys[5]);
            int kiekis = int.Parse(eilDalys[6]);
            Atvirukas atvirukas = new Atvirukas();
            atvirukas.Deti(pavadinimas,salis, metai, spalvota,
                aukstis, plotis, kiekis);
            return atvirukas;
        }

        /// <summary>
        /// Pagrindinio lango matodas atliekamas kai užkraunamas pagrindinis
        /// langas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            statusoEtikete1.Text = "";
        }

        /// <summary>
        /// Metodas atliekamas kai mygtukas Uždaryti programą paspaudžiamas
        /// uždarantis programą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barmenuUzdaryti_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Medotas aktyvuojamas kai mygtukas mygtSpalvosRasti paspaudžiamas
        /// aktyvuojamas paieškos funkciją
        /// </summary>
        /// <param name="sendet"></param>
        /// <param name="e"></param>
        private void mygtSpalvosRasti_Click(object sendet, EventArgs e)
        {
            string galutinis = GautiSpalvotuTuretoja(pasirinkValstybes.Text);
            if(galutinis == "")
            {
                MessageBox.Show("Nepavyko surasti tokio kolekcininko",
                    "Klaida");
                return;
            }
            statusoEtikete1.Text = "Kolekcionieriai turintys daugiausiai "
                + pasirinkValstybes.Text + " atviručių nuskatyti sėkmingai";
            platausTesktoDeze.Text += "\n\tKolekcionieriai turintys" +
                " daugiausiai spalvotų ";
            platausTesktoDeze.Text += pasirinkValstybes.Text;
            platausTesktoDeze.Text += " atviruku yra: \n\t\t\t\t";
            platausTesktoDeze.Text += galutinis;
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas meniubuttonišsaugotiDuomenis
        /// paspaudžiamas kuris leidžia įrašyti kolekcionieirių 
        /// klasės su apdorotais duomenimis iškart panaudojant rušiavimo ir
        /// šalinimo metodus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void meniubuttonišsaugotiDuomenis_Click(object sender,
            EventArgs e)
        {
            SaveFileDialog rasytojas = new SaveFileDialog();
            rasytojas.Filter = "Teksto failas|*.txt";
            rasytojas.FileName = "duomenys";
            rasytojas.Title = "Išsaugoti teksto failą";
            if (rasytojas.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string kelias = rasytojas.FileName;
                using (StreamWriter teksRasytojas = new
                    StreamWriter(File.Create(kelias)))
                {
                    teksRasytojas.Write("                          PRADINIAI"+
                        " DUOMENYS\n");
                    foreach (Kolekcionierius kolek in Kolekcionieriai)
                        teksRasytojas.Write(kolek.ToString(false));
                    teksRasytojas.Write("\n                          DAUGIAU " +
                        "NEGU VIENAS\n");
                    foreach (Kolekcionierius kolek in Kolekcionieriai)
                        teksRasytojas.Write(kolek.ToString(true));
                    rikiavimasToolStripMenuItem_Click(sender, e);
                    teksRasytojas.Write("\n                          SURIKIUO" +
                        "TI DUOMENYS\n");
                    teksRasytojas.Write("                          VISI " +
                        "DUOMENYS\n");
                    foreach (Kolekcionierius kolek in Kolekcionieriai)
                        teksRasytojas.Write(kolek.ToString(false));
                    teksRasytojas.Write("\n                           DAUGIAU "+
                        "NEGU VIENAS\n");
                    foreach (Kolekcionierius kolek in Kolekcionieriai)
                        teksRasytojas.Write(kolek.ToString(true));
                    salinimasBeDatosToolStripMenuItem_Click(sender, e);
                    teksRasytojas.Write("\n                       PAŠALINTI " +
                        "NEŽINOMI METAI\n");
                    teksRasytojas.Write("                          VISI " +
                        "DUOMENYS\n");
                    foreach (Kolekcionierius kolek in Kolekcionieriai)
                        teksRasytojas.Write(kolek.ToString(false));
                    teksRasytojas.Write("\n                           DAUGIAU "+
                        "NEGU VIENAS\n");
                    foreach (Kolekcionierius kolek in Kolekcionieriai)
                        teksRasytojas.Write(kolek.ToString(true));
                    statusoEtikete1.Text = "Duomenis išsaugoti";
                }
            }
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas meniuButtonAtvertiDuomenis
        /// paspaudžiamas
        /// kuris leidžia nuskaityti duomenis iš failo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void meniuButtonAtvertiDuomenis_Click(object sender,
            EventArgs e)
        {
            if (failoPaieska.ShowDialog() == DialogResult.OK)
            {
                tekstFailoPav.Text = failoPaieska.SafeFileName;
                mygtIvesti.Enabled = true;
            }
            mygtIvesti_Click(sender, e);
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas mygtRikiavimas aktyvuojamas
        /// paleidžiantis rikiaviko funkciką kuri surikiuoja atvirukus
        /// pagal metus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rikiavimasToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            foreach (Kolekcionierius kolek in Kolekcionieriai)
            {
                kolek.RikiuotiAtvirukus();
            }
            if (Kolekcionieriai.Count == 0)
                statusoEtikete1.Text = "Nėra duomenų";
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas mygtSalinti paspaudžiamas
        /// leidžiantis pašalinti pašalinti Kolekcionierių atvirukus
        /// kurių metai yra nežinomi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void salinimasBeDatosToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            foreach(Kolekcionierius kolek in Kolekcionieriai)
            {
                kolek.SalintiNuliniusVirsVieno();
            }
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas mygtSpausdinti paspaudžiamas
        /// aktyvuojantis spaudinimo ir atnaujinimo metodus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void spausdintiDuomenisToolStripMenuItem_Click(object sender,
            EventArgs e)
        { 
            platausTesktoDeze.Text += "\n                " +
                "      INFORMACIJA APIE VISUS ATVIRUKUS";
            foreach (Kolekcionierius kolek in Kolekcionieriai)
                platausTesktoDeze.Text += kolek.ToString(false);
            platausTesktoDeze.Text += ((Kolekcionieriai.Count == 0) ?
                "\n                              Duomenu nera" : "");
            platausTesktoDeze.Text += "\n\n                      " +
            "ATVIRUKAI KURIŲ DAUGIAU NEGU VIENAS"; 
            foreach (Kolekcionierius kolek in Kolekcionieriai)
                platausTesktoDeze.Text += kolek.ToString(true);
            platausTesktoDeze.Text += ((Kolekcionieriai.Count == 0) ?
                "\n                              Duomenu nera" : "");
        }

        /// <summary>
        /// Metodas aktyvuojamas kai mygtukas mygtIsvalyti paspaudžiamas
        /// kuris išvalo spausdinimo langą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void isvalytiSpausdinimoLangaToolStripMenuItem_Click(object
            sender, EventArgs e)
        {
            platausTesktoDeze.Text = "";
            statusoEtikete1.Text = "Rašymo langas išvalytas";
        }

        /// <summary>
        /// Randa zmogu kuris turi daugiausiai pasirinktos valstybes 
        /// spalvotų atvirukų.
        /// </summary>
        /// <param name="valstybe">Pasirinkta valstybė</param>
        /// <returns>Rasto žmogaus vardas ir pavardė</returns>
        private string GautiSpalvotuTuretoja(string valstybe)
        {
            string galutinis = string.Empty;
            if (Kolekcionieriai.Count == 0) // jeigu tuščias
                return galutinis;
            int maksimalus = Kolekcionieriai.Max(a =>
                a.KiekisSpalvotu(valstybe));
            if (maksimalus == 0)
            {
                return galutinis;
            }
            else
            {
                var rasti = Kolekcionieriai.Where(a => 
                a.KiekisSpalvotu(valstybe)== maksimalus).ToList();

                foreach (AsmInformacija informacija in
                    rasti.Select(a => a.Informacija))
                {
                    galutinis += informacija.vardas + " " + informacija.pavarde
                        + " ";
                }
                return galutinis;
            }
        }

        /// <summary>
        /// Atnaujina Šalių sąrašą combobox'e
        /// </summary>
        private void AtnaujintiSalis()
        {
            pasirinkValstybes.DataSource = SaliuSarasas.
                Where(a => a.pavadinimas != "NE").ToList();
            pasirinkValstybes.DisplayMember = "pavadinimas";
        }

        /// <summary>
        /// Atnaujina šalių sąrašą paspaudus mygtuką
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void atnaujintiSalisToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            AtnaujintiSalis();
        }

        /// <summary>
        /// Metodas skirtas paimti double reiksme bet kokiu formatu
        /// </summary>
        /// <param name="value">bandoma parse'inti double reikšmė</param>
        /// <returns>reikšmė kuria kompiuteris normaliai supranta</returns>
        private static double GetDouble(string reiksme)
        {
            double rezutatas;

            reiksme = reiksme.Replace(',', '.');
            if (!double.TryParse(reiksme, System.Globalization.NumberStyles.Any,
                CultureInfo.GetCultureInfo("en-US"), out rezutatas))
            {
                rezutatas = -1;
                MessageBox.Show("Nepavyko nuskaityti double reikšmių");
            }

            return rezutatas;
        }

        /// <summary>
        /// Metodas skirtas sujungti sąrašus
        /// </summary>
        private void SarasuSujungimas(List<Atvirukas> senas, 
            List<Atvirukas> naujas)
        {
            for (int x=0; x<naujas.Count; x++)
            {
                for(int z=0; z<senas.Count; z++)
                {
                    if (senas[z] == naujas[x])
                    {
                        senas[z].PapildytiKieki(naujas[x].Kiekis);
                        break;
                    }
                    //jeigu tokio nėra sąraše - pridedamas 
                    if (z == senas.Count - 1)
                    {
                        senas.Add(naujas[x]);
                        z++; //nes pridedamas naujas
                    }
                }
            }
            return;
        }

        /// <summary>
        /// Įterpia rikiuotą sąrašą įterpia paildomas reikšmes iš nerikiuoto
        /// sąrašo
        /// </summary>
        /// <param name="senas">Rikiuotas sąrašas, į kurį įterpsime</param>
        /// <param name="naujas">Sąrašas, iš kurio terpsime</param>
        static void RikiutuSarasuSujungimas(List<Atvirukas> senas,
            List<Atvirukas> naujas)
        {
            int vieta = 0;
            for (int i = 0; i < naujas.Count; i++)
            {
                for (int z = 0; z < senas.Count; z++)
                {
                    if (naujas[i] == senas[z])//Jei operatorius grąžina tiesą
                    {
                        senas[z].PapildytiKieki(naujas[i].Kiekis);
                        break;
                    }
                    if (senas[z] < naujas[i])
                    {
                        vieta = i;
                        senas.Insert(vieta, naujas[i]);
                        z++;
                        break;
                    }
                    else if (z == senas.Count - 1)
                    {
                        senas.Add(naujas[i]);
                        z++;
                    }
                }
            }
            //neaišku kodėl bet reikalingas
            SutrauktiVienodus(senas); 
        }

        /// <summary>
        /// Metodas skirtas sutraukti atvrukus, bet su skirtingais kiekiais
        /// </summary>
        /// <param name="senas"></param>
        static void SutrauktiVienodus(List<Atvirukas> senas)
        {
            if (senas.Count == 0)
                return;
            int kiekis = senas[0].Kiekis; 
            int dabartinis = 0;
            while (senas.Sum(a => a.Kiekis) != kiekis)
            {
                kiekis += senas[dabartinis + 1].Kiekis;
                if (senas[dabartinis] == senas[dabartinis + 1])
                {
                    senas[dabartinis].PapildytiKieki(
                        senas[dabartinis + 1].Kiekis
                    );
                    senas.RemoveAt(dabartinis + 1);
                }
                dabartinis++;
            }
        }

        /// <summary>
        /// Metodas skirtas išvalyti visus sąrašus apie kolekcionierius
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void išvalytiVisusDuomenisToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            Kolekcionieriai.Clear();
            platausTesktoDeze.Text = "";
        }

        /// <summary>
        /// Metodas spausdinantis duomenis csv formatu kai paspaudžiamas
        /// mygtukas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void įrašytiKaipCSVToolStripMenuItem_Click(object sender, 
            EventArgs e)
        {
            SaveFileDialog rasytojas = new SaveFileDialog();
            rasytojas.Filter = "grafiniaiDuomenys|*.csv";
            rasytojas.FileName = "CSVrezultatai";
            rasytojas.Title = "Išsaugoti teksto failą";
            if (rasytojas.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string kelias = rasytojas.FileName;
                using (StreamWriter teksRasytojas = new
                    StreamWriter(File.Create(kelias)))
                {
                    teksRasytojas.Write("Pavadinimas, aukstis," +
                        " plotis, kiekis\n");
                    foreach (Atvirukas atvir in Kolekcionieriai[0].Atvirukai)
                        teksRasytojas.Write(atvir.Pavadinimas + "," + 
                            atvir.Aukstis + "," + atvir.Plotis + "," +
                            atvir.Kiekis + "\n");
                }
            }
        }

        /// <summary>
        /// Mygtukas skirtas patikrinti ar visu kolekcionių sąrašai yra 
        /// surikiuoti. Rezultatą išveda į ekraną.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tikrintiArRikiuotasToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            if (ArSurikiuotaVirsVieno())
                MessageBox.Show("Sąrašas surikiuotas");
            else
                MessageBox.Show("Sąrašas nesurikiuotas");
        }

        /// <summary>
        /// Tikrina ar sąrašo kolekcionieriai sąrašas ArvirukaiViršVieno
        /// surikiuoti pagal metus
        /// </summary>
        /// <returns></returns>
        private bool ArSurikiuotaVirsVieno()
        {
            foreach (Kolekcionierius kolek in Kolekcionieriai)
            {
                if (kolek.ArSurikiuota() == false)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Vykdomas įterpimas į rikiuotą sąrašą
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iterptiIRikiuota_Click(object sender, EventArgs e)
        {
            if (ArSurikiuotaVirsVieno())
            {
                try {
                    if (failoPaieska.FileName != "")
                    {
                        SkaitytiKolekcFaila(Kolekcionieriai, 
                            failoPaieska.FileName, true);
                    }
                }
                catch (System.IndexOutOfRangeException ex)
                {
                    MessageBox.Show("Nepavyko nuskaityti visų failo duomenų",
                        "Klaida");
                    return;
                }
                finally
                {
                    mygtIvesti.Enabled = false;
                    iterptiIRikiuota.Enabled = false;
                    tekstFailoPav.Text = "failas nepasirinktas";
                }
                statusoEtikete1.Text = "Failas nuskaitytas sėkmingai";
                //Įjungiami mygtukai
                spausdintiDuomenisToolStripMenuItem.Enabled = true;
                isvalytiSpausdinimoLangaToolStripMenuItem.Enabled = true;
                pasirinkValstybes.Enabled = true;
                mygtSpalvosRasti.Enabled = true;
                rikiavimasToolStripMenuItem.Enabled = true;
                salinimasBeDatosToolStripMenuItem.Enabled = true;
                kategIssaugotiDuomenis.Enabled = true;
                atnaujintiSalisToolStripMenuItem.Enabled = true;
                įrašytiKaipCSVToolStripMenuItem.Enabled = true;
                tikrintiArRikiuotasToolStripMenuItem.Enabled = true;
                failoPaieska.Reset();
            }
            else
            {
                MessageBox.Show("Sąrašas nesurikiuotas. Įterpti negalime");
            }
        }
    }
}
